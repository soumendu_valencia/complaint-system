import axios from 'axios';

export default axios.create({
  baseURL: 'https://vtc-complaint-system.herokuapp.com/api',
  headers: {
    'Content-type': 'application/json',
  },
});
