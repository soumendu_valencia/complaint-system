import { configureStore } from '@reduxjs/toolkit';
import loginReducer from '../components/Login/LoginSlice';
import investigationReducer from '../components/InvestigationsPanel/investigationSlice';
import createAccountReducer from '../components/CreateAccount/CreateAccountSlice';
import intakeFormReducer from '../components/IntakeForm/intakeFormSlice';
import firstReviewReducer from '../components/FirstReview/firstReviewSlice';
import ViewComplaintReducer from '../components/ViewComplaint/ViewComplaintSlice';
import existingComplaintsReducer from '../components/ExistingComplaints/existingComplaintsSlice';
import changesReducer from './changesSlice';
import mdrReducer from '../components/MDR/mdrSlice';

export const store = configureStore({
  reducer: {
    intakeForm: intakeFormReducer,
    firstReview: firstReviewReducer,
    existingComplaints: existingComplaintsReducer,
    login: loginReducer,
    createAccount: createAccountReducer,
    viewComplaint: ViewComplaintReducer,
    investigation: investigationReducer,
    changes: changesReducer,
    mdr: mdrReducer,
  },
});

export default store;
