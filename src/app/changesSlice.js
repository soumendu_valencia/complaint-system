/* eslint-disable no-unused-vars */
/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../services/test.service';

export const writeChange = createAsyncThunk(
  'testComplaints/writeChange',
  async (changes) => {
    const response = await TestDataService.writeChange(changes);
    return response.data.result;
  },
);

export const changePhase = createAsyncThunk(
  'testComplaints/changePhase',
  async (info) => {
    const response = await TestDataService.changePhase(info);
    return response.data.result;
  },
);

export const uploadFiles = createAsyncThunk(
  'api/uploadFiles',
  async (files) => {
    const response = await TestDataService.uploadFiles(files);
    return response.data;
  },
);

export const writeFile = createAsyncThunk(
  'api/writeFile',
  async (file) => {
    const response = await TestDataService.writeFile(file);
    return response.data;
  },
);

export const fetchAllFiles = createAsyncThunk(
  'api/fetchAllFiles',
  async (issueID) => {
    const response = await TestDataService.fetchAllFiles(issueID);
    return response.data.result;
  },
);

export const getFile = createAsyncThunk(
  'api/getFile',
  async (props) => {
    const response = await TestDataService.getFile(props);
    return response.data;
  },
);

export const changesSlice = createSlice({
  name: 'changes',
  initialState: {
    writeChangeSuccessful: null,
    phaseChangeSuccessful: null,
    uploadFilesSuccessful: null,
    writeFilesSuccessful: null,
    fetchFilesComplete: null,
    fileList: [],
  },
  reducers: {
    resetPhaseChangeSuccessful: (state) => {
      state.phaseChangeSuccessful = null;
    },
    resetWriteChangeSuccessful: (state) => {
      state.writeChangeSuccessful = null;
    },
    resetFilesSuccessful: (state) => {
      state.uploadFilesSuccessful = null;
      state.writeFileSuccessful = null;
    },
    resetFetchFiles: (state) => {
      state.fetchFilesComplete = null;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(writeChange.fulfilled, (state, action) => {
      state.writeChangeSuccessful = action.payload.toString();
    });
    builder.addCase(changePhase.fulfilled, (state, action) => {
      state.phaseChangeSuccessful = action.payload.toString();
    });
    builder.addCase(uploadFiles.fulfilled, (state, action) => {
      state.uploadFilesSuccessful = action.payload;
    });
    builder.addCase(writeFile.fulfilled, (state, action) => {
      state.writeFilesSuccessful = action.payload;
    });
    builder.addCase(fetchAllFiles.fulfilled, (state, action) => {
      state.fileList = action.payload;
      window.sessionStorage.setItem('complaintFiles', JSON.stringify(action.payload));
      state.fetchFilesComplete = true;
    });
    // builder.addCase(getFile.fulfilled, (state, action) => {

    // });
  },
});

export const {
  resetPhaseChangeSuccessful,
  resetWriteChangeSuccessful,
  resetFilesSuccessful,
} = changesSlice.actions;

export default changesSlice.reducer;
