// eslint-disable-next-line no-unused-vars
class Complaint {
  constructor(props) {
    const {
      complaintID,
      customerID,
      patientID,
      dateAware,
      dateReported,
      dateEvent,
      immediateSafetyIssue,
      isComplaint,
      mdrNumber,
      complaintStatus,
      description,
      asReportedCode,
      conclusion,
      dateClosed,
    } = props;

    this.complaintID = complaintID;
    this.customerID = customerID;
    this.patientID = patientID;
    this.dateAware = dateAware;
    this.dateReported = dateReported;
    this.dateEvent = dateEvent;
    this.immediateSafetyIssue = immediateSafetyIssue;
    this.isComplaint = isComplaint;
    this.mdrNumber = mdrNumber;
    this.complaintStatus = complaintStatus;
    this.description = description;
    this.asReportedCode = asReportedCode;
    this.conclusion = conclusion;
    this.dateClosed = dateClosed;
  }
}
