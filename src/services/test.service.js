/* eslint-disable class-methods-use-this */
import http from '../http-common';

class TestDataService {
  getCustAndPat() {
    return http.get('/testComplaints');
  }

  getProducts() {
    return http.get('/getProducts');
  }

  writeIntake(formData) {
    return http.post('/writeIntakeToDb', JSON.stringify(formData));
  }

  updateIntake(formData) {
    return http.post('/updateIntakeToDb', JSON.stringify(formData));
  }

  getAllUsers() {
    return http.post('/fetchAllUsers');
  }

  getAllComplaints() {
    return http.get('/getAllComplaints');
  }

  getSingleComplaint(issueID) {
    return http.post('/getSingleComplaint', JSON.stringify({ ComplaintID: issueID }));
  }

  getAsReportedCodes() {
    return http.get('/getAsReportedCodes');
  }

  writeFirstRevToDb(formData) {
    return http.put('/writeFirstRevToDb', JSON.stringify(formData));
  }

  writeInvestigationToDb(formData) {
    return http.put('/writeInvestigationToDb', JSON.stringify(formData));
  }

  fetchInvestigation(issueID) {
    return http.post('/fetchInvestigation', JSON.stringify({ issueID }));
  }

  fetchAllInvestigations() {
    return http.post('/fetchAllInvestigations');
  }

  fetchLoginData() {
    return http.get('/getLoginData');
  }

  writeLoginInfo(loginInfo) {
    return http.post('/writeLoginInfo', JSON.stringify(loginInfo));
  }

  createAccount(accountInfo) {
    return http.post('/createAccount', JSON.stringify(accountInfo));
  }

  writeChange(changesInfo) {
    return http.post('/writeChange', JSON.stringify(changesInfo));
  }

  writeAssignToDb(formData) {
    return http.post('/writeAssignToDb', JSON.stringify(formData));
  }

  writeMDRToDb(mdrData) {
    return http.post('/writeMDRToDb', JSON.stringify(mdrData));
  }

  updateMDRToDb(mdrData) {
    return http.post('/updateMDRToDb', JSON.stringify(mdrData));
  }

  writeMDRNum(mdrNum) {
    return http.post('/writeMDRNum', JSON.stringify(mdrNum));
  }

  fetchMDRInfo(issueID) {
    return http.post('/fetchMDRInfo', JSON.stringify({ ComplaintID: issueID }));
  }

  fetchAllMDRInfo() {
    return http.post('/fetchAllMDRInfo');
  }

  updateInvestigationToDb(investigationData) {
    return http.post('/updateInvestigationToDb', JSON.stringify(investigationData));
  }

  changePhase(info) {
    return http.post('/changePhase', JSON.stringify(info));
  }

  uploadFiles(files) {
    return http.post('/uploadFiles', JSON.stringify(files));
  }

  fetchAllFiles(issueID) {
    return http.post('/fetchAllFiles', JSON.stringify({ issueID }));
  }

  getFile(props) {
    return http.post('/getFile', JSON.stringify(props));
  }

  updateCustFollowup(custFollowUpValues) {
    return http.post('/updateCustFollowup', JSON.stringify(custFollowUpValues));
  }

  updateNotesToDB(values) {
    return http.post('/updateNotesToDB', JSON.stringify(values));
  }

  updateAccount(values) {
    return http.post('/updateAccount', JSON.stringify(values));
  }

  writeProducts(values) {
    return http.post('/writeProducts', JSON.stringify(values));
  }

  fetchUserProducts(issueID) {
    return http.post('/fetchUserProducts', JSON.stringify({ issueID }));
  }

  writeFile(file) {
    return http.post('/writeFile', JSON.stringify(file));
  }
}

export default new TestDataService();
