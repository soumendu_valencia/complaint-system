/**
 * Handles submission of form data.
 *
 * Takes in Object containing data from all fields of the form. Parses this
 * data then writes it to the database.
 *
 * @param {Object} complaint Object containing the current (old) complaint data
 * @param {Object} values    Object containing the updated (new) compaint data
 * @param {Object} userInfo  Object containing data of user submitting changes
 *
 * @returns {Object} changes Object containing information on what data was updated
 */
const parseChanges = (complaint, values, userInfo) => {
  let valuesDiff = '';

  Object.keys(values).forEach((key) => {
    if (values[key] !== undefined && complaint[key] !== values[key]) {
      valuesDiff += `${key.toString()}: ${complaint[key]} --> ${values[key]} | `;
    }
  });

  const changes = {
    userID: userInfo.UserID,
    userName: `${userInfo.FirstName} ${userInfo.LastName}`,
    timestamp: new Date().toLocaleString(),
    valuesDiff,
    issueID: complaint.issueID,
  };

  return changes;
};

/**
 * Formats date strings into a more readable format.
 *
 * @param {String} date String of a specific date and time
 *
 * @returns {String}    formatted date string
 */
const dateParser = (date) => {
  if (date === null) { return 'N/A'; }
  const year = date.substring(0, 4);
  const month = date.substring(5, 7);
  const day = date.substring(8, 10);
  return `${month}/${day}/${year}`;
};

/**
 * Calculates difference between two dates in days.
 *
 * @param {String} startDate String of a specific starting date and time
 * @param {String} endDate   String of a specific ending date and time
 *
 * @returns {number} result  Number of days between start and end date
 */
const daysElapsed = (startDate, endDate) => {
  const diffTime = Math.abs(endDate - Date.parse(startDate));
  const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
  return diffDays;
};

/**
 * Adds a specified number of days to a given date.
 *
 * @param {String} date String of a specific date and time
 * @param {number} days number of days to add
 *
 * @returns {Object} result Object containing a date "days" after the given date
 */
const addDays = (date, days) => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

module.exports = {
  parseChanges,
  dateParser,
  daysElapsed,
  addDays,
};
