/* eslint-disable no-nested-ternary */
/* eslint-disable new-cap */
import { jsPDF } from 'jspdf';
import autoTable from 'jspdf-autotable';
import { daysElapsed, dateParser } from './actionsIndex';
import logo from '../logo_icon.jpg';

const createPDF = (props) => {
  const {
    complaint,
    investigation,
    mdr,
    products,
  } = props;

  const {
    ComplaintID,
    Phase,
    DateReported,
    DateClosed,
    PersonIntake,
    DateEvent,
    DateAware,
    IssueIsComplaint,
    PersonFirstReview,
    PersonFinalReview,
    CustomerID,
    PatientID,
    IssueDescription,
    CustomerUpdates,
    RMANumber,
    ProductReturned,
  } = complaint;

  const {
    PersonInvestigation,
    InvestigationSummary,
    ComplaintConfirmed,
    RootCauseCode,
  } = investigation;

  const {
    MDRNumber,
    DueDate,
    DateMDRSubmitted,
    DateSuppMDRSubmitted,
  } = mdr;

  // const {
  //   userProducts,
  // } = products;

  const doc = new jsPDF({
    orientation: 'portrait',
    unit: 'in',
    format: [8.5, 11],
  });

  let prodStr = '';
  if (products && products.length > 0) {
    products.forEach((p) => {
      prodStr += `Product:  ${p.ProductName}\nSerial #:  ${p.ProductSN}\n\n`;
    });
    prodStr.trim();
  }

  const img = new Image();
  img.src = logo;
  // doc.setFontSize(24);
  // doc.setFont('Helvetica', 'bold');
  doc.addImage(img, 'JPEG', 7.25, 0.5, 0.75, 0.75);

  autoTable(doc, {
    theme: 'plain',
    body: [
      [
        { content: 'Complaint Report', colSpan: 3, styles: { fontSize: 20, fontStyle: 'bold' } },
      ],
      [
        { content: `Issue ID: ${ComplaintID}\nPhase: ${Phase}`, colSpan: 3, styles: { fontSize: 16, fontStyle: 'bold' } },
      ],
      [
        { content: 'Information:', colSpan: 3, styles: { fontSize: 14, fontStyle: 'bold', fillColor: '#BEDDF3' } },
      ],
      [
        { content: 'Date Created:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Date Closed:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Days Open:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${DateReported ? dateParser(DateReported) : 'ERR'}`, styles: { fontSize: 12 } },
        { content: `${DateClosed ? dateParser(DateClosed) : 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${DateClosed ? daysElapsed(DateReported, DateClosed) : 'N/A'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Created By:', colSpan: 3, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${PersonIntake || 'ERR'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'First Reviewer:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Investigator:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Final Reviewer:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${PersonFirstReview || 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${PersonInvestigation || 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${PersonFinalReview || 'N/A'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Date of Event :', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Date First Aware:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Is the issue a complaint?', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${DateEvent ? dateParser(DateEvent) : 'ERR'}`, styles: { fontSize: 12 } },
        { content: `${DateAware ? dateParser(DateAware) : 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${IssueIsComplaint ? 'Yes' : IssueIsComplaint === null ? 'N/A' : 'No'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Customer ID:', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Patient ID:', colSpan: 2, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${CustomerID || 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${PatientID || 'N/A'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Customer Products:', colSpan: 3, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: prodStr.length > 0 ? prodStr : 'N/A', colSpan: 3, styles: { fontSize: 12 } },
      ],
      [
        { content: 'Issue Description', colSpan: 3, styles: { fontSize: 14, fontStyle: 'bold', fillColor: '#BEDDF3' } },
      ],
      [
        { content: `${IssueDescription || 'N/A'}`, colSpan: 3, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Investigation', colSpan: 3, styles: { fontSize: 14, fontStyle: 'bold', fillColor: '#BEDDF3' } },
      ],
      [
        { content: 'Complaint confirmed?', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Root Cause Code:', colSpan: 2, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${ComplaintConfirmed ? 'Yes' : ComplaintConfirmed === null ? 'N/A' : 'No'}`, styles: { fontSize: 12 } },
        { content: `${RootCauseCode || 'N/A'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Investigation Summary', colSpan: 3, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${InvestigationSummary || 'N/A'}`, colSpan: 3, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'MDR Information', colSpan: 3, styles: { fontSize: 14, fontStyle: 'bold', fillColor: '#BEDDF3' } },
      ],
      [
        { content: 'MDR Number', colSpan: 3, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${MDRNumber || 'N/A'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Due Date', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Date MDR Submitted', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Date Supp. MDR Submitted', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${DueDate ? dateParser(DueDate) : 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${DateMDRSubmitted ? dateParser(DateMDRSubmitted) : 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${DateSuppMDRSubmitted ? dateParser(DateSuppMDRSubmitted) : 'N/A'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: '' },
      ],
      [
        { content: 'Customer Follow-up Information', colSpan: 3, styles: { fontSize: 14, fontStyle: 'bold', fillColor: '#BEDDF3' } },
      ],
      [
        { content: 'RMA Number', styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
        { content: 'Was product returned?', colSpan: 2, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${RMANumber || 'N/A'}`, styles: { fontSize: 12 } },
        { content: `${ProductReturned ? 'Yes' : 'No'}`, styles: { fontSize: 12 } },
      ],
      [
        { content: 'Customer Updates', colSpan: 3, styles: { fontSize: 12, fontStyle: 'bold', fillColor: '#F6F6F6' } },
      ],
      [
        { content: `${CustomerUpdates || 'N/A'}`, colSpan: 3, styles: { fontSize: 12 } },
      ],
    ],
  });

  doc.save(`Issue ${ComplaintID} - Report.pdf`);
  // const string = doc.output('datauristring');
  // const embed = `<embed width="100%" height="100%" src='" + ${string} + "'/>`;
  // const x = window.open();
  // x.document.open();
  // x.document.write(embed);
  // x.document.close();
};

export default createPDF;
