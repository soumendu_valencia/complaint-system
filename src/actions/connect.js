const { Connection } = require('tedious');

const config = {
  authentication: {
    options: {
      userName: 'dbAdmin',
      password: 'dB05172021',
    },
    type: 'default',
  },
  server: 'ValenciaTechnologies.database.windows.net',
  options: {
    database: 'VT_Complaint_Database_Test',
    encrypt: true,
  },
};

const connection = new Connection(config);
connection.connect();

module.exports = connection;
