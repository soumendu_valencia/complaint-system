const { Request } = require('tedious');
const { TYPES } = require('tedious');
const connection = require('../connect');

connection.on('connect', (err) => {
  if (err) {
    console.log('CONNECTION FAILED');
    throw err;
  }
  // eslint-disable-next-line no-use-before-define
  login();
});

if (connection.state !== connection.STATE.LOGGED_IN) {
  connection.connect();
}

function login(username) {
  const info = {};

  const request = new Request(
    'SELECT * FROM [dbo].[Logins] where Username = @Username;',
    (err) => {
      if (err) {
        throw err;
      }
      connection.close();
      return info;
    },
  );
  request.addParameter('Username', TYPES.NVarChar, username);

  request.on('row', (columns) => {
    columns.forEach((column) => {
      info[column.metadata.colName] = column.value;
    });
  });

  request.on('done', () => {
    console.log('Done is called!');
  });

  connection.execSql(request);
}

exports.login = login;
