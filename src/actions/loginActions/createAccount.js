const { Request } = require('tedious');
const { TYPES } = require('tedious');
const connection = require('../connect');

// connection.connect();

const createAccount = async (accountInfo) => new Promise((resolve, reject) => {
  try {
    const request = new Request(
      'INSERT INTO [dbo].[Logins] (PasswordHash, Salt, Username) VALUES (@PasswordHash, @Salt, @Username);',
      ((err) => {
        if (err) {
          reject(err);
        }
        connection.close();
        resolve(true);
      }),
    );
    // request.addParameter('UserID', TYPES.Int, 2);
    request.addParameter('PasswordHash', TYPES.NVarChar, accountInfo.hashedPass);
    request.addParameter('Salt', TYPES.NVarChar, accountInfo.userSalt);
    request.addParameter('Username', TYPES.NVarChar, accountInfo.username);

    connection.execSql(request);
  } catch (e) {
    reject(e);
  }
});

exports.createAccount = createAccount;
