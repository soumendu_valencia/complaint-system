const crypto = require('crypto');

const generateSalt = () => crypto.randomBytes(Math.ceil(10)).toString('hex').slice(0, 20);

const hasher = (password, salt) => {
  const hash = crypto.createHmac('sha512', salt);
  hash.update(password);
  const hashedPass = hash.digest('hex');

  return {
    salt,
    hashedPass,
  };
};

const hash = (password, salt) => {
  if (password == null || salt == null) {
    throw new Error('Must provide password and salt value');
  }

  if (typeof password !== 'string' || typeof salt !== 'string') {
    throw new Error('password must be a string and salt must either be a salt string or a number of rounds ');
  }

  return hasher(password, salt);
};

const compare = (password, hashVals) => {
  const passwordData = hasher(password, hashVals.salt);
  if (passwordData.hashedPass === hashVals.hashedPass) {
    return true;
  }
  return false;
};

module.exports = {
  generateSalt,
  hash,
  compare,
};
