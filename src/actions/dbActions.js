/* eslint-disable arrow-body-style */
const loginTool = require('./loginActions/loginTools');

const parseBit = (bitValue) => {
  const parsedBitValue = bitValue === 'TRUE' ? 1 : 0;
  return parsedBitValue;
};

const writeChange = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    userID,
    userName,
    timestamp,
    valuesDiff,
    issueID,
  } = props;
  const isUpdateIntakeSuccess = await pool.request()
    .input('userID', sql.Int, userID)
    .input('userName', sql.NVarChar, userName)
    .input('timestamp', sql.DateTime2, timestamp)
    .input('valuesDiff', sql.NVarChar, valuesDiff)
    .input('issueID', sql.Int, issueID)
    .query(
      `INSERT INTO [dbo].[Revisions] 
      (
        UserID,
        EmployeeName,
        RevisionDate,
        Changes,
        IssueID
      ) VALUES
      (
        @userID,
        @userName,
        @timestamp,
        @valuesDiff,
        @issueID
      );`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    })
    .catch((e) => {
      console.log('REVISIONS ERROR: ');
      console.log(e);
    });
  return isUpdateIntakeSuccess;
};

const pullAsReportedCodes = async (sql, connectionPool) => {
  const pool = await connectionPool;
  // const request = new sql.Request();
  const result = await pool.request()
    .query('SELECT AsReportedCode FROM [dbo].[AsReportedCodes]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const pullCustomers = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query('SELECT * FROM [dbo].[Customers]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const pullPatients = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query('SELECT * FROM [dbo].[Patients]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const getProducts = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query('SELECT * FROM [dbo].[Products]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const pullComplaint = async (sql, connectionPool, issueID) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .input('issueID', sql.Int, issueID)
    .query('SELECT * FROM [dbo].[ComplaintMaster] where ComplaintID=@issueID')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const pullAllComplaints = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query('SELECT ComplaintID, Phase, DateReported, AsReportedCode, PersonIntake, DateAware, MDRIsRequired, MDRConclusion1, CustomerID, PatientID FROM [dbo].[ComplaintMaster]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const fetchAllUsers = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query('SELECT * FROM [dbo].[Users]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const getSingleComplaint = async (sql, connectionPool, issueID) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .input('issueID', sql.Int, issueID)
    .query('SELECT * FROM [dbo].[ComplaintMaster] WHERE ComplaintID=@issueID')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const login = async (sql, connectionPool, username, password) => {
  const pool = await connectionPool;
  let isLoginSuccess = false;
  const loginResult = await pool.request()
    .input('username', sql.NVarChar, username)
    .query('SELECT * FROM [dbo].[Logins] where Username=@username')
    .then((response) => {
      if (response.recordset[0] && Object.keys(response.recordset[0]).length !== 0) {
        // eslint-disable-next-line prefer-destructuring
        // eslint-disable-next-line max-len
        isLoginSuccess = loginTool.compare(password, { salt: response.recordset[0].Salt, hashedPass: response.recordset[0].PasswordHash });
      }
      return isLoginSuccess;
    });

  let userInfo = {};
  if (loginResult) {
    userInfo = await pool.request()
      .input('username', sql.NVarChar, username)
      .query('SELECT * FROM [dbo].[Users] where Username=@username')
      .then((response) => {
        return response.recordset[0];
      });
  }
  return [loginResult, userInfo];
};

const updateAccount = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    hashedPass,
    userSalt,
    username,
    dateUpdated,
  } = props;
  const today = new Date().toLocaleDateString();
  const loginUpdate = await pool.request()
    .input('hashedPass', sql.NVarChar, hashedPass)
    .input('userSalt', sql.NVarChar, userSalt)
    .input('dateUpdated', sql.DateTime2, today)
    .input('username', sql.NVarChar, username)
    .query(
      `UPDATE [dbo].[Logins] SET
        PasswordHash = @hashedPass,
        Salt = @userSalt
        WHERE Username = @username;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  const userUpdate = await pool.request()
    .input('dateUpdated', sql.DateTime2, dateUpdated)
    .input('username', sql.NVarChar, username)
    .query(
      `UPDATE [dbo].[Users] SET
        PassLastUpdated = @dateUpdated
        WHERE Username = @username;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  return loginUpdate && userUpdate;
};

const writeIntakeToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    customerID,
    patientID,
    personIntake,
    dateAware,
    dateReported,
    dateEvent,
    issueDescription,
    phase,
    custFollowupReq,
    prodReturn,
    rmaNum,
  } = props;
  const isIntakeSuccess = await pool.request()
    .input('customerID', sql.NVarChar, customerID)
    .input('patientID', sql.NVarChar, patientID)
    .input('personIntake', sql.NVarChar, personIntake)
    .input('dateAware', sql.DateTime2, dateAware)
    .input('dateReported', sql.DateTime2, dateReported)
    .input('dateEvent', sql.DateTime2, dateEvent)
    .input('issueDescription', sql.NVarChar, issueDescription)
    .input('phase', sql.NVarChar, phase)
    .input('custFollowupReq', sql.Bit, parseBit(custFollowupReq))
    .input('prodReturn', sql.Bit, parseBit(prodReturn))
    .input('rmaNum', sql.NVarChar, rmaNum)
    .query(
      `INSERT INTO [dbo].[ComplaintMaster] 
        (
          CustomerID,
          PatientId,
          PersonIntake,
          DateAware,
          DateReported,
          DateEvent,
          IssueDescription,
          Phase,
          CustFollowUpNeeded,
          ProdReturnRequested,
          RMANumber
        ) VALUES
        (
          @customerID,
          @patientID,
          @personIntake,
          @dateAware,
          @dateReported,
          @dateEvent,
          @issueDescription,
          @phase,
          @custFollowupReq,
          @prodReturn,
          @rmaNum
        );
        SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return { response: true, scopeID: response.recordset[0].SCOPE_IDENTITY };
      }
      return { response: false, scopeID: null };
    });
  return isIntakeSuccess;
};

const writeMDRToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
    typeMDR,
    dueDate,
    mdrClassification,
    mdrStatus,
    mdrReviewDate,
  } = props;
  const isMDRSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('typeMDR', sql.Bit, parseBit(typeMDR))
    .input('dueDate', sql.DateTime2, dueDate)
    .input('mdrClassification', sql.NVarChar, mdrClassification)
    .input('mdrStatus', sql.NVarChar, mdrStatus)
    .input('mdrReviewDate', sql.DateTime2, mdrReviewDate)
    .query(
      `INSERT INTO [dbo].[MDR] 
        (
          IssueID,
          Type,
          DueDate,
          Classification,
          Status,
          ReviewDate
        ) VALUES
        (
          @issueID,
          @typeMDR,
          @dueDate,
          @mdrClassification,
          @mdrStatus,
          @mdrReviewDate
        );
        SELECT SCOPE_IDENTITY() AS [SCOPE_IDENTITY];`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return { response: true, scopeID: response.recordset[0].SCOPE_IDENTITY };
      }
      return { response: false, scopeID: null };
    });
  return isMDRSuccess;
};

const writeMDRNum = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
    mdrNum,
  } = props;

  const isWriteMDRNumSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('mdrNum', sql.NVarChar, mdrNum)
    .query(
      `UPDATE [dbo].[MDR] SET
        MDRNumber = @mdrNum
        WHERE IssueID = @issueID;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  return isWriteMDRNumSuccess;
};

const writeInvestigationToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
  } = props;
  const isInvestigationSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .query(
      `INSERT INTO [dbo].[Investigations] 
        (
          IssueID
        ) VALUES
        (
          @issueID
        );`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  return isInvestigationSuccess;
};

const fetchInvestigation = async (sql, connectionPool, props) => {
  const { issueID } = props;
  const pool = await connectionPool;
  const result = await pool.request()
    .input('issueID', sql.NVarChar, issueID)
    .query('SELECT * FROM [dbo].[Investigations] WHERE IssueID=@issueID')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const fetchAllInvestigations = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query('SELECT * FROM [dbo].[Investigations]')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const updateIntakeToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
    customerID,
    patientID,
    dateAware,
    dateReported,
    dateEvent,
    issueDescription,
    phase,
    custFollowupReq,
    prodReturn,
    rmaNum,
  } = props;

  const isUpdateIntakeSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('customerID', sql.NVarChar, customerID)
    .input('patientID', sql.NVarChar, patientID)
    .input('dateAware', sql.DateTime2, dateAware)
    .input('dateReported', sql.DateTime2, dateReported)
    .input('dateEvent', sql.DateTime2, dateEvent)
    .input('issueDescription', sql.NVarChar, issueDescription)
    .input('phase', sql.NVarChar, phase)
    .input('custFollowupReq', sql.Bit, parseBit(custFollowupReq))
    .input('prodReturn', sql.Bit, parseBit(prodReturn))
    .input('rmaNum', sql.NVarChar, rmaNum)
    .query(
      `UPDATE [dbo].[ComplaintMaster] SET
        CustomerID = @customerID,
        PatientID = @patientID,
        DateAware = @dateAware,
        DateReported = @dateReported,
        DateEvent = @dateEvent,
        IssueDescription = @issueDescription,
        Phase = @phase,
        CustFollowUpNeeded = @custFollowupReq,
        ProdReturnRequested = @prodReturn,
        RMANumber = @rmaNum
        WHERE ComplaintID = @issueID;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    })
    .catch((e) => {
      console.log('ERROR: ');
      console.log(e);
    });
  return isUpdateIntakeSuccess;
};

const writeFirstRevToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
    isComplaint,
    issueRationale,
    immediateSafetyIssue,
    asReportedCode,
    isMDRReq,
    mdrNum,
    mdrReviewDate,
    phase,
    status,
    personFirstRev,
    dateFirstRev,
    mdrq1,
    mdrq2,
    mdrq3,
  } = props;

  const isFirstRevSuccessful = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('isComplaint', sql.Bit, parseBit(isComplaint))
    .input('immediateSafetyIssue', sql.NVarChar, immediateSafetyIssue)
    .input('asReportedCode', sql.NVarChar, asReportedCode)
    .input('issueRationale', sql.NVarChar, issueRationale)
    .input('isMDRReq', sql.Bit, parseBit(isMDRReq))
    .input('mdrNum', sql.NVarChar, mdrNum)
    .input('mdrReviewDate', sql.DateTime2, mdrReviewDate)
    .input('phase', sql.NVarChar, phase)
    .input('status', sql.NVarChar, status)
    .input('personFirstRev', sql.NVarChar, personFirstRev)
    .input('dateFirstRev', sql.DateTime2, dateFirstRev)
    .input('mdrq1', sql.Bit, parseBit(mdrq1))
    .input('mdrq2', sql.Bit, parseBit(mdrq2))
    .input('mdrq3', sql.Bit, parseBit(mdrq3))
    .query(
      `UPDATE [dbo].[ComplaintMaster] SET
        IssueIsComplaint = @isComplaint,
        AsReportedCode = @asReportedCode,
        IssueNotComplaintRationale = @issueRationale,
        ImmediateSafetyIssue = @immediateSafetyIssue,
        MDRIsRequired = @isMDRReq,
        MDRNumber1 = @mdrNum,
        DateMDRReview1 = @mdrReviewDate,
        Phase = @phase,
        StatusOpenClosed = @status,
        PersonFirstReview = @personFirstRev,
        DateFirstReview = @dateFirstRev,
        MDRQ1 = @mdrq1,
        MDRQ2 = @mdrq2,
        MDRQ3 = @mdrq3
        WHERE ComplaintID = @issueID;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  return isFirstRevSuccessful;
};

const createAccount = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    dateAccountCreated,
    email,
    firstName,
    lastName,
    hashedPass,
    userSalt,
    username,
    role,
  } = props;
  const today = new Date().toLocaleDateString();
  const isLoginWriteSuccessful = await pool.request()
    .input('hashedPass', sql.NVarChar, hashedPass)
    .input('userSalt', sql.NVarChar, userSalt)
    .input('username', sql.NVarChar, username)
    .input('dateUpdated', sql.DateTime2, today)
    .query(
      `INSERT INTO [dbo].[Logins] 
      (
        PasswordHash,
        Salt,
        Username,
        DateLastUpdated
      ) VALUES
      (
        @hashedPass,
        @userSalt,
        @username,
        @dateUpdated
      );`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    })
    .catch((e) => {
      console.log('ERROR: ');
      console.log(e);
    });
  const isUserWriteSuccessful = await pool.request()
    .input('email', sql.NVarChar, email)
    .input('firstName', sql.NVarChar, firstName)
    .input('lastName', sql.NVarChar, lastName)
    .input('dateAccountCreated', sql.DateTime, dateAccountCreated)
    .input('username', sql.NVarChar, username)
    .input('role', sql.Int, parseInt(role, 10))
    .input('dateUpdated', sql.DateTime2, today)
    .query(
      `INSERT INTO [dbo].[Users]
      (
        Email,
        FirstName,
        LastName,
        AccountCreatedDate,
        Username,
        Role,
        PassLastupdated
      ) VALUES
      (
        @email,
        @firstName,
        @lastName,
        @dateAccountCreated,
        @username,
        @role,
        @dateUpdated
      );`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  return isLoginWriteSuccessful && isUserWriteSuccessful;
};

const fetchMDRInfo = async (sql, connectionPool, issueID) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .input('issueID', sql.Int, issueID)
    .query('SELECT * FROM [dbo].[MDR] WHERE IssueID=@issueID')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const fetchAllMDRInfo = async (sql, connectionPool) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .query("SELECT * FROM [dbo].[MDR] WHERE Status='OPEN'")
    .then((response) => {
      return response.recordset;
    });
  return result;
};

// TODO: CRITICAL -- need a where clause. dunno what was going on when I wrote this query
const updateMDRToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
    mdrStatus,
    mdrSubmitted,
    dateMDRSubmitted,
    suppMDRNeeded,
    dateSuppSubmitted,
    mdrNum,
  } = props;
  const isUpdateMDRSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('mdrStatus', sql.NVarChar, mdrStatus)
    .input('mdrSubmitted', sql.Bit, parseBit(mdrSubmitted))
    .input('dateMDRSubmitted', sql.DateTime2, dateMDRSubmitted)
    .input('suppMDRNeeded', sql.Bit, parseBit(suppMDRNeeded))
    .input('dateSuppSubmitted', sql.DateTime2, dateSuppSubmitted)
    .input('mdrNum', sql.NVarChar, mdrNum)
    .query(
      `UPDATE [dbo].[MDR] SET
        Status = @mdrStatus,
        MDRSubmitted = @mdrSubmitted,
        DateSubmitted = @dateMDRSubmitted,
        SupplementalNeeded = @suppMDRNeeded,
        DateSuppSubmitted = @dateSuppSubmitted,
        MDRNumber = @mdrNum
        WHERE IssueID=@issueID;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] > 0) {
        return true;
      }
      return false;
    });
  return isUpdateMDRSuccess;
};

// const updateCMasterInv = async (sql, connectionPool, props) => {
//   const pool = await connectionPool;
//   const {
//     issueID,
//     dateInvestigation,
//     investigator,
//     phase,
//   } = props;
//   const isUpdateCMasterInvSuccess = await pool.request()
//     .input('issueID', sql.Int, issueID)
//     .input('dateInvestigation', sql.DateTime2, dateInvestigation)
//     .input('investigator', sql.NVarChar, investigator)
//     .input('phase', sql.NVarChar, phase)
//     .query(
//       `UPDATE [dbo].[ComplaintMaster] SET
//         DateInvestigationClosed = @dateInvestigation,
//         PersonInvestigationClose = @investigator
//         WHERE ComplaintID = @issueID;`,
//     ).then((response) => {
//       if (response.rowsAffected[0] === 1) {
//         return true;
//       }
//       return false;
//     });
// };

const updateInvestigationToDb = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueID,
    dateInvestigation,
    invStatus,
    capaResults,
    complaintConfirmed,
    rootCauseCode,
    riskResults,
    dhrRevResults,
    investigationSummary,
    investigationNarrative,
  } = props;

  const isUpdateInvestigationSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('status', sql.NVarChar, invStatus)
    .input('rootCauseCode', sql.NVarChar, rootCauseCode)
    .input('capaResults', sql.NVarChar, capaResults)
    .input('riskResults', sql.NVarChar, riskResults)
    .input('dhrRevResults', sql.NVarChar, dhrRevResults)
    .input('investigationSummary', sql.NVarChar, investigationSummary)
    .input('complaintConfirmed', sql.Bit, parseBit(complaintConfirmed))
    .input('investigationNarrative', sql.NVarChar, investigationNarrative)
    .input('dateInvestigation', sql.DateTime2, dateInvestigation)
    .query(
      `UPDATE [dbo].[Investigations] SET
        InvestigationStatus = @status,
        RootCauseCode = @rootCauseCode,
        CAPAFileReviewResults = @capaResults,
        RiskFileReviewResults = @riskResults,
        DHRReviewResults = @dhrRevResults,
        InvestigationSummary = @investigationSummary,
        ComplaintConfirmed = @complaintConfirmed,
        InvestigationNarrative = @investigationNarrative,
        DateInvClosed = @dateInvestigation
        WHERE IssueID = @issueID;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        // const cMasterUpdated = updateCMasterInv(props).then((reponse) => {
        //   if (response) {
        //     return true;
        //   } else {
        //     return false;
        //   }
        // });
        return true;
      }
      return false;
    });
  return isUpdateInvestigationSuccess;
};

const changePhase = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const {
    issueId,
    phase,
    status,
    dateClosed,
  } = props;

  const isPhaseChangeSuccess = await pool.request()
    .input('issueId', sql.Int, issueId)
    .input('phase', sql.NVarChar, phase)
    .input('status', sql.NVarChar, status)
    .input('dateClosed', sql.DateTime2, dateClosed)
    .query(
      `UPDATE [dbo].[ComplaintMaster] SET
        Phase = @phase,
        StatusOpenClosed = @status,
        DateClosed = @dateClosed
        WHERE ComplaintID = @issueId;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  return isPhaseChangeSuccess;
};

const uploadFiles = async (sql, connectionPool, file) => {
  const pool = await connectionPool;
  const {
    issueID,
    fileName,
    dateUploaded,
    personUpload,
    phase,
    fileType,
  } = file;

  const uploadFileResult = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('fileName', sql.NVarChar, fileName)
    .input('dateUploaded', sql.DateTime2, dateUploaded)
    .input('personUpload', sql.NVarChar, personUpload)
    .input('phase', sql.NVarChar, phase)
    .input('fileType', sql.NVarChar, fileType)
    .query(
      `INSERT INTO [dbo].[Files] 
        (
          IssueID,
          FileName,
          UploadDate,
          PersonUpload,
          AssociatedPhase,
          FileType
        ) VALUES
        (
          @issueID,
          @fileName,
          @dateUploaded,
          @personUpload,
          @phase,
          @fileType
        );`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });
  // const table = new sql.Table('Files');

  // table.create = true;
  // table.columns.add('Id', sql.Int, { nullable: false, primary: true });
  // table.columns.add('IssueID', sql.Int, { nullable: false });
  // table.columns.add('FileName', sql.NVarChar(sql.MAX), { nullable: false });
  // table.columns.add('UploadDate', sql.DateTime2, { nullable: false });
  // table.columns.add('PersonUpload', sql.NVarChar(50), { nullable: false });
  // table.columns.add('AssociatedPhase', sql.NVarChar(50), { nullable: true });
  // table.columns.add('FileType', sql.NVarChar(50), { nullable: true });
  // table.columns.add('File', sql.VarBinary(sql.MAX), { nullable: false });

  // fileList.forEach((file) => {
  //   const fileBuffer = Buffer.from(new Uint16Array(file.fileBuffer));
  //   table.rows.add(
  //     i + 1,
  //     file.issueID,
  //     file.fileName,
  //     file.uploadDate,
  //     file.personUpload,
  //     file.phase,
  //     file.fileType,
  //     fileBuffer,
  //   );
  // });

  // const request = new sql.Request();
  // request

  // const testvar = await pool.request()
  //   .bulk(table).then((response) => {
  //     if (response.rowsAffected[0] === fileList.length) {
  //       return true;
  //     }
  //     return false;
  //   });

  return uploadFileResult;
};

const fetchAllFiles = async (sql, connectionPool, issueID) => {
  const pool = await connectionPool;
  const result = await pool.request()
    .input('issueID', sql.Int, issueID)
    .query('SELECT * FROM [dbo].[Files] WHERE IssueId=@issueID')
    .then((response) => {
      return response.recordset;
    });
  return result;
};

const updateCustFollowup = async (sql, connectionPool, props) => {
  const pool = await connectionPool;

  const {
    issueID,
    custFollowupReq,
    isProdReturned,
    customerUpdates,
    rmaNum,
  } = props;

  const isUpdateCustFollowupSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('custFollowupReq', sql.Bit, parseBit(custFollowupReq))
    .input('isProdReturned', sql.Bit, parseBit(isProdReturned))
    .input('customerUpdates', sql.NVarChar, customerUpdates)
    .input('rmaNum', sql.NVarChar, rmaNum)
    .query(
      `UPDATE [dbo].[ComplaintMaster] SET
        CustFollowUpNeeded = @custFollowupReq,
        ProductReturned = @isProdReturned,
        CustomerUpdates = @customerUpdates,
        RMANumber = @rmaNum
        WHERE ComplaintID = @issueID;`,
    )
    .then((response) => {
      if (response.rowsAffected[0] === 1) {
        return true;
      }
      return false;
    });

  return isUpdateCustFollowupSuccess;
};

const updateNotesToDB = async (sql, connectionPool, values) => {
  const pool = await connectionPool;

  const {
    issueID,
    Notes,
  } = values;

  const isUpdateNotesSuccess = await pool.request()
    .input('issueID', sql.Int, issueID)
    .input('Notes', sql.NVarChar, Notes)
    .query(
      `UPDATE [dbo].[ComplaintMaster] SET
        Notes = @Notes
        WHERE ComplaintID = @issueID;`,
    )
    .then((response) => {
      return response.rowsAffected[0] === 1;
    });

  return isUpdateNotesSuccess;
};

const writeProducts = async (sql, connectionPool, prodList) => {
  const pool = await connectionPool;
  const { scopeID, savedProdArr } = prodList;

  const table = new sql.Table('CustomerProducts');

  table.create = true;
  // table.columns.add('Id', sql.Int, { nullable: false, primary: true });
  table.columns.add('IssueID', sql.Int, { nullable: false });
  table.columns.add('ProductName', sql.NVarChar(50), { nullable: true });
  table.columns.add('ProductSN', sql.NVarChar(50), { nullable: true });
  table.columns.add('CustomerID', sql.Int, { nullable: true });

  // add here rows to insert into the table
  savedProdArr.forEach((prod) => {
    // const fileBuffer = Buffer.from(new Uint16Array(file.fileBuffer));
    table.rows.add(
      // i + 1,
      scopeID,
      prod.prodName,
      prod.serialNum,
    );
  });

  // const request = new sql.Request();
  // request

  const testvar = await pool.request()
    .bulk(table).then((response) => {
      if (response.rowsAffected[0] === prodList.length) {
        return true;
      }
      return false;
    });

  return testvar;
};

const fetchUserProducts = async (sql, connectionPool, props) => {
  const pool = await connectionPool;
  const { issueID } = props;

  const userProds = await pool.request()
    .input('issueID', sql.Int, issueID)
    .query(
      'SELECT * FROM [dbo].[CustomerProducts] WHERE IssueID=@issueID',
    )
    .then((response) => {
      return response.recordset;
    });

  return userProds;
};

module.exports = {
  writeChange,
  pullAsReportedCodes,
  getProducts,
  pullCustomers,
  pullPatients,
  pullComplaint,
  pullAllComplaints,
  fetchAllUsers,
  getSingleComplaint,
  login,
  updateAccount,
  writeIntakeToDb,
  updateIntakeToDb,
  writeFirstRevToDb,
  createAccount,
  writeMDRToDb,
  writeMDRNum,
  writeInvestigationToDb,
  fetchInvestigation,
  fetchAllInvestigations,
  fetchMDRInfo,
  fetchAllMDRInfo,
  updateMDRToDb,
  updateInvestigationToDb,
  changePhase,
  uploadFiles,
  fetchAllFiles,
  updateCustFollowup,
  updateNotesToDB,
  writeProducts,
  fetchUserProducts,
};
