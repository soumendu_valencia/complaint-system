/* eslint-disable react/prop-types */
/* eslint-disable max-len */
/* eslint-disable react/jsx-filename-extension */
import React, { useState, useEffect } from 'react';
import {
  BrowserRouter as Router, Switch, Route,
} from 'react-router-dom';
import { connect } from 'react-redux';
import EditComplaint from './components/editComplaint/EditComplaint';
import HomeScreen from './components/HomeScreen/HomeScreen';
import vtcLogo from './logo_icon.jpg';

import {
  // ExistingComplaints,
  Login,
  // UserDashboard,
  // Settings,
} from './components/index';

import './App.css';
import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';

function App(props) {
  const { loginSuccess } = props;
  const [loggedIn, setLoggedIn] = useState(JSON.parse(sessionStorage.getItem('userLoggedIn')));

  useEffect(() => {
    setLoggedIn(JSON.parse(sessionStorage.getItem('userLoggedIn')));
  }, [loginSuccess]);

  if (!loggedIn) {
    return (
      <Router>
        <Switch>
          <Route path="/" component={Login} />
        </Switch>
      </Router>
    );
  }

  return (
    <>
      <img
        src={vtcLogo}
        className="vtc-logo"
        draggable={false}
        alt="Valencia Technologies Logo"
      />
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/home" component={HomeScreen} />
          <Route path="/editComplaint" component={EditComplaint} />
          <Route path="/viewComplaint" component={EditComplaint} />
        </Switch>
      </Router>
    </>
  );
}

const mapStateToProps = (state) => ({
  loginSuccess: state.login.loginSuccess,
});

export default connect(mapStateToProps)(App);
// export default App;
