/* eslint-disable react/prop-types */
/* eslint-disable array-callback-return */
/* eslint-disable max-len */
/* eslint-disable consistent-return */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import {
  Tabs, Tab, InputGroup, Button,
} from '@blueprintjs/core';
import { matchSorter } from 'match-sorter';
import { resetLogin } from '../Login/LoginSlice';
import { fetchAllComplaints } from '../ExistingComplaints/existingComplaintsSlice';
import UserDashboard from '../UserDashboard/UserDashboard';
import IntakeForm from '../IntakeForm/IntakeForm';
import AllComplaints from '../AllComplaints/AllComplaints';
import Settings from '../Settings/Settings';
import CustomTable from '../CustomTable/CustomTable';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './HomeScreen.css';
import '../../App.css';

function HomeScreen(props) {
  const [recentIssues, setRecentIssues] = useState(' ');
  const [showSearchTable, setShowSearchTable] = useState(false);
  const [searchResults, setSearchResults] = useState([]);

  const { allComplaints } = props;
  const dispatch = useDispatch();
  const history = useHistory();

  const parseRecentIssues = (issueIDs) => {
    let issuesElmnt = '';
    issueIDs.forEach((issue) => {
      issuesElmnt = `${issue.scopeId}\n${issuesElmnt}`;
    });
    setRecentIssues(issuesElmnt);
    return issuesElmnt;
  };

  useEffect(() => {
    dispatch(fetchAllComplaints());
    parseRecentIssues(JSON.parse(window.sessionStorage.getItem('rcIDs')));
    window.sessionStorage.removeItem('complaintFiles');
  }, []);

  /* Search function that uses fuzzy (approximate) key-word matching */
  function fuzzySearchMutipleWords(rows, keys, filterValue) {
    if (!filterValue || !filterValue.length) {
      return rows;
    }

    const terms = filterValue.split(' ');
    if (!terms) {
      return rows;
    }

    return terms.reduceRight(
      (results, term) => matchSorter(results, term, { keys }),
      rows,
    );
  }

  /*
    Search handler. Adds a slight delay between typing in a search term and actually searching for it.
    Allows for a smoother search experience.
   */
  let delayTimer;
  const handleSearch = (searchValue) => {
    setShowSearchTable(searchValue.length > 0);
    clearTimeout(delayTimer);
    // let results;
    delayTimer = setTimeout(() => {
      const results = fuzzySearchMutipleWords(allComplaints, ['ComplaintID', 'Phase', 'PersonIntake'], searchValue);
      setSearchResults(results);
    }, 1000);
  };

  /* Logs user out of their account. Takes user back to login screen. */
  const handleLogOut = () => {
    dispatch(resetLogin());
    window.sessionStorage.clear();
    window.sessionStorage.setItem('userLoggedIn', 'false');
    history.push('/');
  };

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));

  return (
    <>
      <div className="dashboard-wrapper">
        <div className="greeting-wrapper">
          <h2 className="greeting">
            Welcome,
            {' '}
            {storedUserInfo.FirstName}
          </h2>
          <Button
            type="button"
            minimal
            outlined
            onClick={() => handleLogOut()}
          >
            Log Out
          </Button>
        </div>
        <div className="user-dashboard-wrapper">
          <Tabs className="edit-complaint-tablist" key="test" vertical large>
            <Tab
              className="outline-none"
              id="db"
              title="Home Screen"
              panel={showSearchTable ? (
                <>
                  <h2>Search results:</h2>
                  <CustomTable data={searchResults} customHeight={0.8} />
                </>
              ) : (
                <UserDashboard />
              )}
            />
            <Tab
              className="outline-none"
              id="in"
              title="Create New Issue"
              panel={storedUserInfo.Role === 0 || storedUserInfo.Role === 2 ? (
                <IntakeForm />
              ) : (
                <h1>
                  You are not allowed to create new issues.
                </h1>
              )}
            />
            <Tab
              className="outline-none"
              id="op"
              title="All Complaints"
              panel={showSearchTable ? (
                <>
                  <h2>Search results:</h2>
                  <CustomTable data={searchResults} customHeight={0.8} />
                </>
              ) : (
                <AllComplaints />
              )}
            />
            <Tab
              className="outline-none"
              id="st"
              title="Settings"
              panel={<Settings />}
            />
            <div className="margin-top-20 margin-left-4p">
              <h2>Search:</h2>
              <div className="bp3-input-group .modifier">
                <InputGroup
                  asyncControl
                  type="search"
                  leftIcon="search"
                  placeholder="Search issues"
                  onChange={(e) => {
                    handleSearch(e.target.value);
                  }}
                />
              </div>
            </div>
            <div className="callout-wrapper bp3-callout bp3-intent-primary text-align-center .modifier">
              <h4 className="bp3-heading">Recently Created Issues:</h4>
              <b>{recentIssues}</b>
            </div>
          </Tabs>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  allComplaints: state.existingComplaints.complaints,
});

export default connect(mapStateToProps)(HomeScreen);
