/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Button } from '@blueprintjs/core';
import CustomTable from '../CustomTable/CustomTable';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import './AllComplaints.css';
import '../../App.css';

function OpenComplaints(props) {
  // const [openComplaints, setOpenComplaints] = useState([]);
  const {
    complaints,
    intakeComplaints,
    firstRevComplaints,
    investigationComplaints,
    mdrComplaints,
    finalRevComplaints,
    openComplaints,
    closedComplaints,
    // intakeUsers,
    complaintsFetched,
  } = props;

  const [ARCodes, setARCodes] = useState([]);
  const [arCode, setARCode] = useState('All');
  const [currTable, setCurrTable] = useState(complaints);
  const [currPhase, setCurrPhase] = useState('All');
  const [reportable, setReportable] = useState('All');
  // const [filterStr, setFilterStr] = useState(`Phase: ${currPhase}\nReportable: ${reportable}`);
  const [loading, setLoading] = useState('bp3-skeleton');

  const resetFilters = () => {
    setCurrPhase('All');
    setReportable('All');
    setARCode('All');
  };

  const reportableMatch = (p, r) => reportable === 'All'
    || (reportable === 'YES' && r)
    || (reportable === 'NO' && !r)
    || (reportable === '--' && (p === 'Intake' || p === 'First Review') && !r);

  const ARCodesMatch = (a) => arCode === 'All'
    || arCode === a;

  const filterCols = () => {
    if (complaintsFetched) {
      switch (currPhase) {
        case 'All':
          setCurrTable(complaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
            && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'Intake':
          setCurrTable(intakeComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'First Review':
          setCurrTable(firstRevComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'Investigation':
          setCurrTable(investigationComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'MDRs':
          setCurrTable(mdrComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'Final Review':
          setCurrTable(finalRevComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'Open':
          setCurrTable(openComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        case 'Closed':
          setCurrTable(closedComplaints.filter((c) => reportableMatch(c.Phase, c.MDRIsRequired)
          && ARCodesMatch(c.AsReportedCode)));
          break;
        default:
          break;
      }
    }
  };

  // TODO: Look into having one filter function that takes in all params to give layered sorting capabilities
  useEffect(() => {
    filterCols();
    // setFilterStr(`Phase: ${currPhase}\nAs-Reported Code: ${arCode}\nReportable: ${reportable}`);
  }, [currPhase, reportable, arCode]);

  useEffect(() => {
    if (complaintsFetched) {
      setCurrTable(complaints);
      setARCodes(JSON.parse(window.sessionStorage.getItem('asReportedCodes')));
      setLoading('');
      // const intakeUsersList = [];
      // intakeUsers.forEach((user) => intakeUsersList.push({ value: user }));
      // console.table(intakeUsersList);
    }
  }, [complaintsFetched]);

  /* Callback func. for child component to update state in parent (this) component */
  const changePhase = (phase) => {
    setCurrPhase(phase);
  };

  /* Callback func. for child component to update state in parent (this) component */
  const changeReportable = (isReportable) => {
    setReportable(isReportable);
  };

  const changeARCode = (code) => {
    setARCode(code);
  };

  return (
    <>
      <div className="dashboard-wrapper">
        <div className="complaints-table-wrapper">
          <h2>All complaints:</h2>
          <div className={loading}>
            <Button
              type="button"
              // className="bp3-button"
              minimal
              outlined
              icon="filter"
              onClick={() => resetFilters()}
            >
              Reset Filters
            </Button>
            <br />
            <div className="pre-line font-weight-bold margin-top-1p margin-bottom-1p">
              <table>
                <tbody>
                  <tr>
                    <td>
                      <span className="bp3-tag bp3-round bp3-large bp3-intent-primary">
                        Phase:
                        {' '}
                        {currPhase}
                      </span>
                    </td>
                    <td>
                      <span className="bp3-tag bp3-round bp3-large bp3-intent-primary">
                        As-Reported Code:
                        {' '}
                        {arCode}
                      </span>
                    </td>
                    <td>
                      <span className="bp3-tag bp3-round bp3-large bp3-intent-primary">
                        Reportable:
                        {' '}
                        {reportable}
                      </span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            {/* <p
              className="pre-line font-weight-bold margin-top-1p margin-bottom-1p"
            >
              {filterStr}
            </p> */}
            <CustomTable
              data={currTable}
              onPhaseChange={changePhase}
              onReportableChange={changeReportable}
              onAsReportedCodeChange={changeARCode}
              asReportedCodes={ARCodes}
              customHeight={0.8}
              customWidth={0.8}
            />
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaints: state.existingComplaints.complaints,
  complaintsFetched: state.existingComplaints.complaintsFetched,
  userInfo: state.login.userInfo,
  intakeComplaints: state.existingComplaints.intakeComplaints,
  firstRevComplaints: state.existingComplaints.firstRevComplaints,
  investigationComplaints: state.existingComplaints.investigationComplaints,
  mdrComplaints: state.existingComplaints.mdrComplaints,
  finalRevComplaints: state.existingComplaints.finalRevComplaints,
  openComplaints: state.existingComplaints.openComplaints,
  closedComplaints: state.existingComplaints.closedComplaints,
});

export default connect(mapStateToProps)(OpenComplaints);
