/* eslint-disable max-len */
import React from 'react';
import { Callout } from '@blueprintjs/core';

const required = (value) => (value ? undefined : 'Required');
const doNothing = () => undefined;
const maxLength = (max) => (value) => (value && value.length > max ? `Must be ${max} characters or less` : undefined);
const maxLength15 = maxLength(15);
const minLength = (min) => (value) => (value && value.length < min ? `Must be ${min} characters or more` : undefined);
const minLength8 = minLength(8);
const number = (value) => (value && Number.isNaN(Number(value)) ? 'Must be a number' : undefined);
const minValue = (min) => (value) => (value && value < min ? `Must be at least ${min}` : undefined);
const minValue18 = minValue(18);
const password = (value) => (value && /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/i.test(value)
  ? undefined
  : (
    <Callout
      intent="warning"
      icon="warning-sign"
    >
      Password must have at least: 8 characters, one uppercase letter, one lowercase letter, one number, one special character
    </Callout>
  )
);
const email = (value) => (value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)
  ? 'Invalid email address'
  : undefined);
const customDomainEmail = (value) => (value && /[A-Za-z0-9]+@valenciatechnologies.com/i.test(value)
  ? undefined
  : 'Invalid email address');
const alphaNumeric = (value) => (value && /[^a-zA-Z0-9 ]/i.test(value)
  ? 'Only alphanumeric characters'
  : undefined);
const phoneNumber = (value) => (value && !/^(0|[1-9][0-9]{9})$/i.test(value)
  ? 'Invalid phone number, must be 10 digits'
  : undefined);
const mdrSubmitted = (value) => (value && value === 'TRUE' ? '' : 'MDR must be submitted before closing.');
const composeValidators = (...validators) => (value) => validators.reduce((error, validator) => error || validator(value), undefined);

export {
  required,
  doNothing,
  maxLength,
  maxLength15,
  minLength,
  minLength8,
  number,
  minValue,
  minValue18,
  password,
  email,
  customDomainEmail,
  alphaNumeric,
  phoneNumber,
  mdrSubmitted,
  composeValidators,
};
