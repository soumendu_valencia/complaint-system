/* eslint-disable no-shadow */
/* eslint-disable no-else-return */
/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
// import { useHistory } from 'react-router-dom';
import {
  Table, Column, HeaderCell, Cell,
} from 'rsuite-table';
// import { matchSorter } from 'match-sorter';
import { connect } from 'react-redux';

import 'rsuite-table/dist/css/rsuite-table.css';
// import { fetchAllComplaints } from '../ExistingComplaints/existingComplaintsSlice';

function CustomInvestigationTable(props) {
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);

  const { data, hgt } = props;

  const today = new Date();
  const daysElapsed = (dateCreated) => {
    const diffTime = Math.abs(today - Date.parse(dateCreated));
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
  };

  const getData = () => {
    if (sortColumn && sortType) {
      return data.sort((a, b) => {
        let x = a[sortColumn];
        let y = b[sortColumn];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortType === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return data;
  };

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setSortColumn(sortColumn);
      setSortType(sortType);
    }, 500);
  };

  return (
    <Table
      data={getData()}
      height={hgt}
      width={1100}
      sortColumn={sortColumn}
      sortType={sortType}
      onSortColumn={handleSortColumn}
      loading={loading}
    >
      <Column width={100} align="center" sortable>
        <HeaderCell>Issue ID</HeaderCell>
        <Cell dataKey="IssueID" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Description</HeaderCell>
        <Cell dataKey="InvestigationSummary" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Investigator</HeaderCell>
        <Cell dataKey="QAApprovalPerson" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Root Cause</HeaderCell>
        <Cell dataKey="AsAnalyzedCode" />
      </Column>
    </Table>
  );
}

const mapStateToProps = (state) => ({
  complaints: state.investigation.investigations,
  userInfo: state.login.userInfo,
});

export default connect(mapStateToProps)(CustomInvestigationTable);
