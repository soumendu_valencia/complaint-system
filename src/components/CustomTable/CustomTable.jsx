/* eslint-disable max-len */
/* eslint-disable no-nested-ternary */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import {
  Table, Column, HeaderCell, Cell,
} from 'rsuite-table';
import {
  Menu,
  MenuItem,
  Popover,
  Position,
  Icon,
} from '@blueprintjs/core';
// import { Popover2, Classes } from '@blueprintjs/popover2';
// eslint-disable-next-line no-unused-vars
import { dateParser } from '../../actions/actionsIndex';

import 'rsuite-table/dist/css/rsuite-table.css';

export default function CustomTable(props) {
  // eslint-disable-next-line prefer-const
  const {
    data,
    customHeight,
    customWidth,
    onPhaseChange,
    onReportableChange,
    onAsReportedCodeChange,
    // asReportedCodes,
  } = props;

  const getWindowDimensions = () => {
    const { innerWidth: width, innerHeight: height } = window;
    return {
      width,
      height,
    };
  };

  const useWindowDimensions = () => {
    const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

    useEffect(() => {
      const handleResize = () => {
        setWindowDimensions(getWindowDimensions());
      };

      window.addEventListener('resize', handleResize);
      return () => window.removeEventListener('resize', handleResize);
    }, []);

    return windowDimensions;
  };

  const { height, width } = useWindowDimensions();

  const today = new Date();
  const daysElapsed = (dateCreated) => {
    const diffTime = Math.abs(today - Date.parse(dateCreated));
    const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
    return diffDays;
  };

  // eslint-disable-next-line no-unused-vars
  const phaseContent = () => (
    <Menu>
      <MenuItem onClick={() => onPhaseChange('All')} text="All" />
      <MenuItem onClick={() => onPhaseChange('Intake')} text="Issue Intake" />
      <MenuItem onClick={() => onPhaseChange('First Review')} text="First Review" />
      <MenuItem onClick={() => onPhaseChange('Investigation')} text="Investigation" />
      <MenuItem onClick={() => onPhaseChange('MDRs')} text="MDRs" />
      <MenuItem onClick={() => onPhaseChange('Final Review')} text="Final Review" />
      <MenuItem onClick={() => onPhaseChange('Open')} text="Open" />
      <MenuItem onClick={() => onPhaseChange('Closed')} text="Closed" />
    </Menu>
  );

  const reportableContent = () => (
    <Menu>
      <MenuItem onClick={() => onReportableChange('All')} text="All" />
      <MenuItem onClick={() => onReportableChange('--')} text="Undetermined" />
      <MenuItem onClick={() => onReportableChange('YES')} text="Yes" />
      <MenuItem onClick={() => onReportableChange('NO')} text="No" />
    </Menu>
  );

  const asReportedContent = () => (
    <Menu>
      <MenuItem onClick={() => onAsReportedCodeChange('All')} text="All" />
      <MenuItem onClick={() => onAsReportedCodeChange('?')} text="?" />
      <MenuItem onClick={() => onAsReportedCodeChange('Clock Drift')} text="Clock Drift" />
      <MenuItem onClick={() => onAsReportedCodeChange('Extrusion')} text="Extrusion" />
      <MenuItem onClick={() => onAsReportedCodeChange('Infection - Incision Site')} text="Infection - Incision Site" />
      <MenuItem onClick={() => onAsReportedCodeChange('Migration')} text="Migration" />
      <MenuItem onClick={() => onAsReportedCodeChange('Premature Battery Depletion')} text="Premature Battery Depletion" />
    </Menu>
    // // eslint-disable-next-line quotes
    // let arCodesMenu = `<Menu><MenuItem key="All" onClick={() => onAsReportedCodeChange('ALL')} text="All" />`;
    // asReportedCodes.forEach((arc) => {
    //   if (arc) {
    //     console.log(arc);
    //     // eslint-disable-next-line max-len
    //     arCodesMenu += `<MenuItem key="${arc.AsReportedCode}" onClick={() => onAsReportedCodeChange('${arc.AsReportedCode}')} text="${arc.AsReportedCode}" />`;
    //   }
    // });
    // // eslint-disable-next-line quotes
    // arCodesMenu += `</Menu>`;
    // console.log(arCodesMenu);
    // return arCodesMenu;
  );

  return (
    <Table
      data={data}
      height={customHeight ? customHeight * height : 0.375 * height}
      width={customWidth ? customWidth * width : 0.8 * width}
      // sortType={sortType}
      // expandedRowKeys={expandedRowKeys}
      // renderRowExpanded={renderRowExpanded}
      headerHeight={35}
      rowHeight={35}
    >
      <Column width={80} align="center" fixed="left">
        <HeaderCell>Issue ID</HeaderCell>
        <Cell dataKey="ComplaintID" />
      </Column>
      <Column width={120} align="center" fixed="left">
        <HeaderCell>Action</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              <a href={`/viewComplaint/${rowData.ComplaintID}/${rowData.Phase}`}>
                {' '}
                <b>View</b>
                {' '}
              </a>
              {' '}
              |
              <a href={`/editComplaint/${rowData.ComplaintID}/${rowData.Phase}`}>
                {' '}
                <b>Edit</b>
                {' '}
              </a>
            </span>
          )}
        </Cell>
      </Column>
      <Column width={100} align="center">
        <HeaderCell>
          {onPhaseChange ? (
            <Popover
              content={phaseContent()}
              position={Position.BOTTOM}
            >
              <>
                Phase
                {' '}
                <Icon icon="double-caret-vertical" />
              </>
            </Popover>
          ) : (
            'Phase'
          )}
        </HeaderCell>
        <Cell dataKey="Phase" />
      </Column>
      <Column width={100} align="center">
        <HeaderCell>Date Created</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              {dateParser(rowData.DateReported)}
            </span>
          )}
        </Cell>
      </Column>
      <Column width={100} align="center">
        <HeaderCell>Days Open</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              {rowData.Phase === 'Closed' ? (
                // TODO: change this to display date between date opened and date closed
                daysElapsed(rowData.DateReported)
              ) : (
                daysElapsed(rowData.DateReported)
              )}
            </span>
          )}
        </Cell>
      </Column>
      <Column minwidth={200} flexGrow={1} align="center">
        <HeaderCell>
          {onAsReportedCodeChange ? (
            <Popover
              content={asReportedContent()}
              position={Position.BOTTOM}
            >
              <>
                As-Reported Code
                {' '}
                <Icon icon="double-caret-vertical" />
              </>
            </Popover>
          ) : (
            'As-Reported Code'
          )}
        </HeaderCell>
        <Cell dataKey="AsReportedCode" />
      </Column>
      <Column width={150} align="center">
        <HeaderCell>Created By</HeaderCell>
        <Cell dataKey="PersonIntake" />
      </Column>
      <Column width={100} align="center">
        <HeaderCell>Date First Aware</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              {dateParser(rowData.DateAware)}
            </span>
          )}
        </Cell>
      </Column>
      <Column width={100} align="center">
        <HeaderCell>
          {onReportableChange ? (
            <Popover
              content={reportableContent()}
              position={Position.BOTTOM}
            >
              <>
                Reportable?
                {' '}
                <Icon icon="double-caret-vertical" />
              </>
            </Popover>
          ) : (
            'Reportable?'
          )}
        </HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              {rowData.MDRIsRequired ? 'YES'
                : rowData.Phase === 'Intake' || rowData.Phase === 'First Review' || rowData.Phase === 'FirstReview'
                  ? '--' : 'NO'}
            </span>
          )}
        </Cell>
        {/* <Cell dataKey="MDRIsRequired" /> */}
      </Column>
      <Column width={100} align="center">
        <HeaderCell>Customer ID</HeaderCell>
        <Cell dataKey="CustomerID" />
      </Column>
      <Column width={100} align="center">
        <HeaderCell>Patient ID</HeaderCell>
        <Cell dataKey="PatientID" />
      </Column>
    </Table>
  );
}
