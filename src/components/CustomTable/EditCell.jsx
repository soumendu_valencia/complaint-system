/* eslint-disable no-unused-expressions */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from 'react';
import { Cell } from 'rsuite-table';

const EditCell = ({
  rowData, dataKey, onChange, ...props
}) => (
  <Cell {...props}>
    {rowData.status === 'EDIT' ? (
      <input
        className="input"
        defaultValue={rowData[dataKey]}
        onChange={(event) => {
          onChange && onChange(rowData.id, dataKey, event.target.value);
        }}
      />
    ) : (
      rowData[dataKey]
    )}
  </Cell>
);

export default EditCell;
