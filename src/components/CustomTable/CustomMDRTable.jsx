/* eslint-disable no-shadow */
/* eslint-disable no-else-return */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
// import { useHistory } from 'react-router-dom';
import {
  Table, Column, HeaderCell, Cell,
} from 'rsuite-table';
import { connect } from 'react-redux';
// import { matchSorter } from 'match-sorter';
import { dateParser } from '../../actions/actionsIndex';

import 'rsuite-table/dist/css/rsuite-table.css';
// import { fetchAllComplaints } from '../ExistingComplaints/existingComplaintsSlice';

function CustomMDRTable(props) {
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);

  const { data } = props;
  // const history = useHistory();

  const getData = () => {
    if (sortColumn && sortType) {
      return data.sort((a, b) => {
        let x = a[sortColumn];
        let y = b[sortColumn];
        if (typeof x === 'string') {
          x = x.charCodeAt();
        }
        if (typeof y === 'string') {
          y = y.charCodeAt();
        }
        if (sortType === 'asc') {
          return x - y;
        } else {
          return y - x;
        }
      });
    }
    return data;
  };

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setSortColumn(sortColumn);
      setSortType(sortType);
    }, 500);
  };

  // const handleView = (complaintID) => {
  //   history.push(`/viewComplaint/${complaintID}`);
  // };

  // const handleEdit = (complaintID, phase) => {
  //   history.push(`/editComplaint/${complaintID}/${phase}`);
  // };

  return (
    <Table
      data={getData()}
      height={500}
      width={1300}
      sortColumn={sortColumn}
      sortType={sortType}
      onSortColumn={handleSortColumn}
      loading={loading}
    >
      <Column width={100} align="center" flexGrow={1} sortable>
        <HeaderCell>Issue ID</HeaderCell>
        <Cell dataKey="IssueID" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>MDR Number</HeaderCell>
        <Cell dataKey="MDRNumber" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Due Date</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              {dateParser(rowData.DueDate)}
            </span>
          )}
        </Cell>
        {/* <Cell dataKey="DueDate" /> */}
      </Column>
      {/* <Column width={120} align="center" fixed="right">
        <HeaderCell>Action</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              <a href={`/editComplaint/${rowData.IssueID}/${rowData.CurrentPhase}`}>
                {' '}
                <b>Edit</b>
                {' '}
              </a>
            </span>
          )}
        </Cell>
      </Column> */}
    </Table>
  );
}

const mapStateToProps = (state) => ({
  complaints: state.existingComplaints.complaints,
  mdrInfo: state.viewComplaint.mdrInfo,
  userInfo: state.login.userInfo,
});

export default connect(mapStateToProps)(CustomMDRTable);
