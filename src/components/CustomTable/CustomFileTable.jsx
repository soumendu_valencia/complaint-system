/* eslint-disable no-plusplus */
/* eslint-disable no-shadow */
/* eslint-disable no-else-return */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
  Table, Column, HeaderCell, Cell,
} from 'rsuite-table';

import 'rsuite-table/dist/css/rsuite-table.css';

export default function CustomMDRTable(props) {
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);

  const { data } = props;

  // const getData = () => {
  //   if (sortColumn && sortType) {
  //     return data.sort((a, b) => {
  //       let x = a[sortColumn];
  //       let y = b[sortColumn];
  //       if (typeof x === 'string') {
  //         x = x.charCodeAt();
  //       }
  //       if (typeof y === 'string') {
  //         y = y.charCodeAt();
  //       }
  //       if (sortType === 'asc') {
  //         return x - y;
  //       } else {
  //         return y - x;
  //       }
  //     });
  //   }
  //   return data;
  // };

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setSortColumn(sortColumn);
      setSortType(sortType);
    }, 500);
  };

  return (
    <Table
      data={data}
      height={500}
      width={500}
      sortColumn={sortColumn}
      sortType={sortType}
      onSortColumn={handleSortColumn}
      loading={loading}
    >
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>File Name</HeaderCell>
        <Cell dataKey="fileName" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Type</HeaderCell>
        <Cell dataKey="fileType" />
      </Column>
    </Table>
  );
}
