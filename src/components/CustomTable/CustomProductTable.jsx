/* eslint-disable no-plusplus */
/* eslint-disable no-shadow */
/* eslint-disable no-else-return */
/* eslint-disable react/prop-types */
import React, { useState } from 'react';
// import { useHistory } from 'react-router-dom';
import {
  Table, Column, HeaderCell, Cell,
} from 'rsuite-table';
// import { matchSorter } from 'match-sorter';
import { connect } from 'react-redux';
import EditCell from './EditCell';

import 'rsuite-table/dist/css/rsuite-table.css';
// import { fetchAllComplaints } from '../ExistingComplaints/existingComplaintsSlice';

function CustomMDRTable(props) {
  const [sortColumn, setSortColumn] = useState();
  const [sortType, setSortType] = useState();
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);

  const { propData } = props;

  const parseData = (rawData) => {
    const tempArr = [];
    if (rawData === undefined) { return []; }
    for (let i = 0; i < rawData.length; i++) {
      tempArr.push({ product: propData[i], serialNumber: '', lotNumber: '' });
    }
    setData(tempArr);
    return data;
  };

  // useEffect(() => {
  //   parseData(propData);
  // }, []);

  // const history = useHistory();

  // const getData = () => {
  //   if (sortColumn && sortType) {
  //     return data.sort((a, b) => {
  //       let x = a[sortColumn];
  //       let y = b[sortColumn];
  //       if (typeof x === 'string') {
  //         x = x.charCodeAt();
  //       }
  //       if (typeof y === 'string') {
  //         y = y.charCodeAt();
  //       }
  //       if (sortType === 'asc') {
  //         return x - y;
  //       } else {
  //         return y - x;
  //       }
  //     });
  //   }
  //   return data;
  // };

  const handleSortColumn = (sortColumn, sortType) => {
    setLoading(true);
    setTimeout(() => {
      setLoading(false);
      setSortColumn(sortColumn);
      setSortType(sortType);
    }, 500);
  };

  // const handleView = (complaintID) => {
  //   history.push(`/viewComplaint/${complaintID}`);
  // };

  // const handleEdit = (complaintID, phase) => {
  //   history.push(`/editComplaint/${complaintID}/${phase}`);
  // };

  const handleChange = (id, key, value) => {
    const nextData = Object.assign([], data);
    nextData.find((item) => item.id === id)[key] = value;
    setData(nextData);
  };

  return (
    <Table
      data={parseData(propData)}
      height={500}
      width={500}
      sortColumn={sortColumn}
      sortType={sortType}
      onSortColumn={handleSortColumn}
      loading={loading}
    >
      <Column width={100} align="center" flexGrow={1} sortable>
        <HeaderCell>Product Name</HeaderCell>
        <Cell dataKey="product" />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Serial Number</HeaderCell>
        <EditCell
          onChange={handleChange}
        />
      </Column>
      <Column width={100} align="center" flexGrow={1}>
        <HeaderCell>Lot Number</HeaderCell>
        <Cell dataKey="DueDate" />
      </Column>
      {/* <Column width={120} align="center" fixed="right">
        <HeaderCell>Action</HeaderCell>
        <Cell>
          {(rowData) => (
            <span>
              <a href={`/editComplaint/${rowData.IssueID}/${rowData.CurrentPhase}`}>
                {' '}
                <b>Edit</b>
                {' '}
              </a>
            </span>
          )}
        </Cell>
      </Column> */}
    </Table>
  );
}

const mapStateToProps = (state) => ({
  complaints: state.existingComplaints.complaints,
  mdrInfo: state.viewComplaint.mdrInfo,
  userInfo: state.login.userInfo,
});

export default connect(mapStateToProps)(CustomMDRTable);
