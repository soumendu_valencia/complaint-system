/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const fetchAsReportedCodes = createAsyncThunk(
  'api/fetchAsReportedCodes',
  async () => {
    const response = await TestDataService.getAsReportedCodes();
    return response.data;
  },
);

export const writeFirstRevToDb = createAsyncThunk(
  'api/writeFirstRevToDb',
  async (values) => {
    const response = await TestDataService.writeFirstRevToDb(values);
    return response.data;
  },
);

export const writeMDRToDb = createAsyncThunk(
  'api/writeMDRToDb',
  async (values) => {
    const response = await TestDataService.writeMDRToDb(values);
    return response.data;
  },
);

export const updateMDRToDb = createAsyncThunk(
  'api/updateMDRToDb',
  async (values) => {
    const response = await TestDataService.updateMDRToDb(values);
    return response.data;
  },
);

export const firstReviewSlice = createSlice({
  name: 'firstReview',
  initialState: {
    isSubmitted: null,
    formData: {},
    asReportedCodes: {},
    mdrIsSubmitted: false,
    mdrUpdateSuccess: false,
  },
  reducers: {
    updateInfo: (state, action) => {
      state.formData = action.payload;
    },
    resetIsSubmitted: (state) => {
      state.isSubmitted = null;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(fetchAsReportedCodes.fulfilled, (state, action) => {
      state.asReportedCodes = action.payload;
      window.sessionStorage.setItem('asReportedCodes', JSON.stringify(action.payload));
    });
    builder.addCase(writeFirstRevToDb.fulfilled, (state, action) => {
      state.isSubmitted = action.payload.result.toString();
    });
    builder.addCase(writeMDRToDb.fulfilled, (state, action) => {
      state.mdrIsSubmitted = action.payload.result;
    });
    builder.addCase(updateMDRToDb.fulfilled, (state, action) => {
      state.mdrUpdated = action.payload;
    });
  },
});

export const selectFirstReviewInfo = (state) => state.firstReview.formData;

export const { updateInfo, resetIsSubmitted } = firstReviewSlice.actions;

export default firstReviewSlice.reducer;
