import React from 'react';
import { connect } from 'react-redux';
import { FormSpy } from 'react-final-form';
import { updateInfo } from './firstReviewSlice';

// eslint-disable-next-line react/prop-types
const FirstReviewToRedux = ({ form }) => (
  <FormSpy onChange={(state) => updateInfo(form, state)} />
);

export default connect(undefined, { updateInfo })(FirstReviewToRedux);
