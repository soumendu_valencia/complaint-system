/* eslint-disable no-nested-ternary */
/* eslint-disable no-restricted-globals */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Radio, Alert, Intent, TextArea, HTMLSelect, Collapse, Button, Classes,
} from '@blueprintjs/core';
import { Popover2, Tooltip2 } from '@blueprintjs/popover2';
import { useDispatch, connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import FirstReviewToRedux from './FirstReviewToRedux';
import { writeFirstRevToDb, writeMDRToDb, resetIsSubmitted } from './firstReviewSlice';
import { writeInvestigationToDb } from '../InvestigationsPanel/investigationSlice';
import { writeChange } from '../../app/changesSlice';
import { parseChanges, dateParser, addDays } from '../../actions/actionsIndex';
import * as validators from '../Validation';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './FirstReview.css';
import '../../App.css';

function FirstReview(props) {
  const dispatch = useDispatch();
  const [showAlert, setShowAlert] = useState(false);
  const [collapse, setCollapse] = useState(true);

  const {
    isSubmitted,
    complaintInfo,
    mdrInfo,
    // mdrId,
  } = props;

  const {
    ComplaintID,
    IssueDescription,
    IssueIsComplaint,
    IssueNotComplaintRationale,
    ImmediateSafetyIssue,
    AsReportedCode,
    MDRIsRequired,
    DateMDRReview1,
    FirstRevComments,
    PersonIntake,
    DateAware,
    MDRQ1,
    MDRQ2,
    MDRQ3,
    DateFirstReview,
    PersonFirstReview,
  } = complaintInfo[0];

  const {
    Type,
    Classification,
    DueDate,
    Status,
    ReviewDate,
  } = mdrInfo[0];

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo')) ?? {
    FirstName: 'ERROR',
    LastName: 'ERROR',
    Username: 'ERROR',
    Role: 3,
  };

  const history = useHistory();

  useEffect(() => {
    if (isSubmitted === 'true') {
      dispatch(resetIsSubmitted());
      history.push('/home');
      history.go(0);
    }
  }, [isSubmitted]);

  // useEffect(() => {
  //   if (mdrId) {
  //     dispatch()
  //   }
  // }, [mdrId]);

  const prompt1String = 'Does the information reasonably suggest that the device may have caused or contributed to a Death or a Serious Injury?';
  const prompt2String = 'Does the information reasonably suggest that the device malfunctioned, and if the malfunction were to recur, the device would be LIKELY to cause or contribute to a death or serious injury?';
  const prompt3String = 'Is remedial action necessary due to public health risk?';

  const mdrClassificationList = [
    { value: '' },
    { value: 'Serious Injury' },
    { value: 'Death' },
    { value: 'Malfuntion' },
  ];

  /* Parsing of asReportedCodes JSON object into array */
  const asReportedCodesArray = Object.values(JSON.parse(window.sessionStorage.getItem('asReportedCodes'))).map((obj) => obj);
  const asReportedCodesList = (() => {
    // eslint-disable-next-line consistent-return
    const temp = asReportedCodesArray.map((i) => {
      if (i.AsReportedCode !== '?') {
        return { value: i.AsReportedCode, label: i.AsReportedCode };
      }
    });
    temp.unshift({ value: '?', label: '?' });
    return temp;
  })();

  /**
   * Handles submission of form data.
   *
   * Takes in Object containing data from all fields of the form. Parses this
   * data then writes it to the database.
   *
   * @param {Object} values                      Object containing data from user-submitted form
   * @param {String} values.immediateSafetyIssue Either 'YES' or 'NO' for whether issue is urgent safety issue
   * @param {String} values.isComplaint          Either 'YES' or 'NO' for whether issue is a complaint
   * @param {String} values.issueRationale       Reasoning for determination that issue is not a complaint
   * @param {String} values.asReportedCode       As-Reported Code for this issue
   * @param {String} values.MDRQ1                Either 'YES' or 'NO' for first MDR determination question
   * @param {String} values.MDRQ2                Either 'YES' or 'NO' for second MDR determination question
   * @param {String} values.MDRQ3                Either 'YES' or 'NO' for third MDR determination question
   * @param {String} values.typeMDR              Either 'YES' or 'NO', with 'YES' being 30 day and 'NO' being 5 day
   * @param {String} values.mdrClassification    Either 'Serious Injury', 'Death', or 'Malfunction'
   * @param {String} values.firstRevComments     Miscellaneous comments given by the First Reviewer
   */
  const handleFirstRevSubmit = async (values) => {
    const today = new Date();
    const userName = `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`;

    values.isMDRReq = values.mdrq1 === 'TRUE' || values.mdrq2 === 'TRUE' || values.mdrq3 === 'TRUE' ? 'TRUE' : 'FALSE';
    values.issueID = ComplaintID;
    values.personFirstRev = userName;
    values.dateFirstRev = today.toLocaleDateString();
    values.issueRationale = values.issueRationale ? `${IssueNotComplaintRationale || ''}\n\n${userName} - ${new Date().toString().substring(0, 24)}\n${values.issueRationale}` : IssueNotComplaintRationale;
    values.asReportedCode = values.asReportedCode ?? AsReportedCode;
    values.mdrReviewDate = DateMDRReview1 ?? today.toLocaleDateString();
    values.firstRevComments = values.firstRevComments ? `${FirstRevComments}\n\n${userName} - ${new Date().toString().substring(0, 24)}\n${values.firstRevComments}` : FirstRevComments;

    if (values.isComplaint === 'FALSE') {
      values.phase = 'Closed';
      values.status = 'CLOSED';
      values.personClose = userName;
    } else {
      values.phase = 'Investigation';
      values.status = 'OPEN';
      values.personClose = null;
    }

    if (!Type && values.isComplaint === 'TRUE' && values.isMDRReq === 'TRUE') {
      const numDaysToAdd = values.mdrq3 === 'TRUE' ? 5 : 30;
      const mdrInfoParsed = {
        issueID: ComplaintID,
        typeMDR: values.mdrq3,
        dueDate: addDays(DateAware, numDaysToAdd),
        mdrClassification: values.mdrClassification,
        mdrStatus: 'OPEN',
        mdrReviewDate: values.mdrReviewDate,
      };

      const mdrFields = {
        issueID: ComplaintID,
        typeMDR: Type === null ? Type : Type ? 'TRUE' : 'FALSE',
        dueDate: DueDate,
        mdrClassification: Classification,
        mdrStatus: Status,
        mdrReviewDate: ReviewDate,
      };

      const mdrChanges = parseChanges(mdrFields, values, storedUserInfo);
      dispatch(writeChange(mdrChanges));
      dispatch(writeMDRToDb(mdrInfoParsed));
    }

    const complaintFields = {
      issueID: ComplaintID,
      isMDRReq: MDRIsRequired === null ? '' : MDRIsRequired ? 'TRUE' : 'FALSE',
      personFirstRev: PersonFirstReview,
      dateFirstRev: DateFirstReview,
      mdrq1: MDRQ1 === null ? '' : MDRQ1 ? 'TRUE' : 'FALSE',
      mdrq2: MDRQ2 === null ? '' : MDRQ2 ? 'TRUE' : 'FALSE',
      mdrq3: MDRQ3 === null ? '' : MDRQ3 ? 'TRUE' : 'FALSE',
      issueRationale: IssueNotComplaintRationale,
      asReportedCode: AsReportedCode,
      mdrReviewDate: DateMDRReview1,
      firstRevComments: '',
    };

    const changes = parseChanges(complaintFields, values, storedUserInfo);

    if (changes.valuesDiff.length > 0) {
      dispatch(writeChange(changes));
    }

    if (values.isComplaint === 'TRUE') {
      dispatch(writeInvestigationToDb(values));
    }

    dispatch(writeFirstRevToDb(values));
  };

  const handleCancel = () => {
    history.push('/home');
    history.go(0);
  };

  const handleConfirm = () => {
    setShowAlert(false);
  };

  const helpText = (
    <div className="help-text-wrapper">
      <p className="help-text">
        {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
        If the issue presents a possible <b>immediate</b> and <b>serious</b> safety concern to the population of eCoin patients, promptly escalate the issue to the head of QA.
      </p>
    </div>
  );

  const FirstRevForm = ({ subscription }) => (
    <Form
      onSubmit={handleFirstRevSubmit}
      subscription={subscription}
      // TODO: add MDRQ1, 2, 3 values to initialValues. Maybe try integration with form via Field tag
      initialValues={{
        isComplaint: IssueIsComplaint ? 'TRUE' : IssueIsComplaint === null ? '' : 'FALSE',
        issueRationale: IssueNotComplaintRationale,
        immediateSafetyIssue: ImmediateSafetyIssue ? 'TRUE' : ImmediateSafetyIssue === null ? '' : 'FALSE',
        asReportedCode: AsReportedCode,
        isMDRReq: MDRIsRequired ? 'TRUE' : 'FALSE',
        mdrq1: MDRQ1 ? 'TRUE' : MDRQ1 === null ? '' : 'FALSE',
        mdrq2: MDRQ2 ? 'TRUE' : MDRQ2 === null ? '' : 'FALSE',
        mdrq3: MDRQ3 ? 'TRUE' : MDRQ3 === null ? '' : 'FALSE',
        typeMDR: Type ? 'TRUE' : 'FALSE',
        mdrClassification: Classification || '',
        firstRevComments: FirstRevComments,
      }}
      render={({
        handleSubmit, submitting, values,
      }) => (
        <>
          <form onSubmit={handleSubmit} autoComplete="off">
            <FirstReviewToRedux form="firstReviewForm" />
            <table className="form-table">
              <tbody>
                <tr>
                  <td>
                    <label>
                      <p>
                        <b>
                          Is this an urgent safety issue?
                          { '  ' }
                        </b>
                        <Popover2
                          content={helpText}
                          placement="right"
                          intent="primary"
                          popoverClassName={Classes.POPOVER_BACKDROP}
                        >
                          <Tooltip2
                            content={helpText}
                            openOnTargetFocus={false}
                            placement="right"
                            usePortal={false}
                          >
                            <a><em>Help</em></a>
                          </Tooltip2>
                        </Popover2>
                      </p>
                      <Field
                        name="immediateSafetyIssue"
                        validate={validators.required}
                      >
                        {({ input, meta }) => (
                          <div>
                            <>
                              <Radio
                                {...input}
                                label="Yes"
                                type="radio"
                                value="TRUE"
                                defaultChecked={values.immediateSafetyIssue === 'TRUE'}
                              />
                              <Radio
                                {...input}
                                label="No"
                                type="radio"
                                value="FALSE"
                                defaultChecked={values.immediateSafetyIssue === 'FALSE'}
                              />
                            </>
                            {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                          </div>
                        )}
                      </Field>
                    </label>
                  </td>
                  {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                  <td>
                    <label>
                      <b>Is the issue a complaint?</b>
                      <div className="row-wrapper-10 issue-rationale-wrapper">
                        <Field
                          name="isComplaint"
                          // type="radio"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <Radio
                                {...input}
                                label="Yes"
                                value="TRUE"
                                defaultChecked={IssueIsComplaint !== null && IssueIsComplaint !== false}
                              />
                              <Radio
                                {...input}
                                label="No"
                                value="FALSE"
                                defaultChecked={IssueIsComplaint !== null && IssueIsComplaint === false}
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </div>
                    </label>
                  </td>
                  <td>
                    {values.isComplaint === 'FALSE' ? (
                      <>
                        <div className="row-wrapper-10 issue-rationale-wrapper">
                          <label>
                            <p><b>Rationale if not a complaint:</b></p>
                            <Field
                              name="issueRationale"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <TextArea
                                    {...input}
                                    className="bp3-input bp3-fill"
                                    growVertically
                                    large
                                    fill
                                  />
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                      </>
                    ) : (
                      <>
                      </>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colSpan={3}>
                    <label>
                      <p><b>As-Reported Code:</b></p>
                      <Field
                        name="asReportedCode"
                        validate={validators.required}
                      >
                        {({ input, meta }) => (
                          <div>
                            <HTMLSelect
                              {...input}
                              options={asReportedCodesList}
                              onChange={input.onChange}
                            />
                            {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                          </div>
                        )}
                      </Field>
                    </label>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colSpan={2}>
                    {values.isComplaint === 'TRUE' ? (
                      <div>
                        <div>
                          <label>
                            <b>{prompt1String}</b>
                            <Field
                              name="mdrq1"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      {...input}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      defaultChecked={values.mdrq1 === 'TRUE'}
                                    />
                                    <Radio
                                      {...input}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      defaultChecked={values.mdrq1 === 'FALSE'}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                        <br />
                        <div>
                          <label>
                            <b>{prompt2String}</b>
                            <Field
                              name="mdrq2"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      {...input}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      defaultChecked={values.mdrq2 === 'TRUE'}
                                    />
                                    <Radio
                                      {...input}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      defaultChecked={values.mdrq2 === 'FALSE'}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                        <br />
                        <div>
                          <label>
                            <b>{prompt3String}</b>
                            <Field
                              name="mdrq3"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      {...input}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      defaultChecked={values.mdrq3 === 'TRUE'}
                                    />
                                    <Radio
                                      {...input}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      defaultChecked={values.mdrq3 === 'FALSE'}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                        <br />
                        {values.mdrq1 === 'TRUE' || values.mdrq2 === 'TRUE' || values.mdrq3 === 'TRUE' ? (
                          <div className="mdr-info-text">
                            <div>
                              <b>MDR Needed:</b>
                              {' '}
                              {values.mdrq3 === 'TRUE' ? '5 Day' : '30 Day'}
                            </div>
                            <div>
                              <b>MDR Due Date:</b>
                              {' '}
                              {!DueDate ? (
                                <>
                                  {addDays(DateAware, values.mdrq3 === 'TRUE' ? 5 : 30).toLocaleDateString()}
                                </>
                              ) : (
                                <>
                                  {DueDate}
                                </>
                              )}
                            </div>
                            <label>
                              <b>Type of Reportable Event:</b>
                              <Field
                                name="mdrClassification"
                                validate={validators.required}
                              >
                                {({ input, meta }) => (
                                  <div>
                                    <HTMLSelect
                                      {...input}
                                      intent="primary"
                                      options={mdrClassificationList}
                                      onChange={input.onChange}
                                    />
                                    {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                  </div>
                                )}
                              </Field>
                            </label>
                          </div>
                        ) : (
                          <div className="mdr-info-text">
                            <b>MDR NOT Needed</b>
                          </div>
                        )}
                      </div>
                    ) : (
                      <></>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colSpan="3">
                    <label className="first-rev-comments">
                      <p><b>Comments</b></p>
                      <Field
                        name="firstRevComments"
                      >
                        {({ input, meta }) => (
                          <div>
                            <TextArea
                              {...input}
                              className="bp3-input bp3-fill"
                              growVertically
                              large
                              fill
                            />
                            {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                          </div>
                        )}
                      </Field>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>
            <Alert
              cancelButtonText="Cancel"
              confirmButtonText="Cancel First Review"
              icon="trash"
              intent={Intent.DANGER}
              isOpen={showAlert}
              onCancel={handleConfirm}
              onConfirm={handleCancel}
            >
              <p>Are you sure you want to cancel? Inputted data won&apos;t be saved.</p>
            </Alert>
            <div className="row-wrapper-80">
              <button
                type="button"
                className="bp3-button .modifier"
                disabled={submitting}
                onClick={() => setShowAlert(true)}
              >
                Cancel
              </button>
              <button
                type="submit"
                className="bp3-button .modifier"
              >
                Complete First Review
              </button>
            </div>
          </form>
        </>
      )}
    />
  );

  return (
    <>
      <h1>First Review</h1>
      <div className="info-card info-container">
        <Button
          minimal
          className="info-button"
          onClick={() => setCollapse(!collapse)}
        >
          {collapse ? 'Hide Information ▲' : 'Show Information ▼'}
        </Button>
        <Collapse isOpen={collapse}>
          <table className="form-table">
            <tbody>
              <tr>
                <td>
                  <label>
                    <b>Intake submitted by:</b>
                    <div>
                      { PersonIntake || 'N/A' }
                    </div>
                  </label>
                </td>
                <td>
                  <label>
                    <b>Date First Aware:</b>
                    <div>
                      { DateAware ? dateParser(DateAware) : 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td colSpan={2}>
                  <label>
                    <b>Issue Description</b>
                    <div>
                      { IssueDescription }
                    </div>
                  </label>
                </td>
              </tr>
            </tbody>
          </table>
        </Collapse>
      </div>
      <div className="first-review-wrapper">
        <div className="width-100p">
          <FirstRevForm />
        </div>
        {/* <div className="right-pane-first-rev">

        </div> */}
      </div>
    </>
  );
}

const mapStateToProps = (state) => (
  {
    isSubmitted: state.firstReview.isSubmitted,
    intakeData: state.intakeForm.formData,
    mdrInfo: state.viewComplaint.mdrInfo,
    complaintInfo: state.viewComplaint.complaintInfo,
    // mdrId: state.mdr.mdrId,
  });

export default connect(mapStateToProps)(FirstReview);
