/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import {
  Tab, Tabs, Drawer, Classes, Position, TextArea, Intent, Toaster, Spinner, Button,
} from '@blueprintjs/core';
import { Form, Field } from 'react-final-form';
// import { writeChange } from '../../app/changesSlice';
import * as validators from '../Validation';
import backArrow from '../../back-arrow.png';

import ViewMDR from '../MDR/ViewMDR';
import ViewFinalReview from '../FinalReview/ViewFinalReview';
import ViewIntake from '../ViewComplaint/ViewIntake/ViewIntake';
import ViewInvestigation from '../InvestigationsPanel/ViewInvestigation';
import ViewCustomerFollowup from '../CustomerFollowup/ViewCustomerFollowup';
import ViewFirstReview from '../ViewComplaint/ViewFirstReview/ViewFirstReview';

import MDR from '../MDR/MDR';
import EditIntake from './EditIntake/EditIntake';
import FirstReview from '../FirstReview/FirstReview';
import FinalReview from '../FinalReview/FinalReview';
import Investigation from '../InvestigationsPanel/Investigation';
import CustomerFollowup from '../CustomerFollowup/CustomerFollowup';
import {
  fetchSingleComplaint, fetchMDRInfo, updateNotesToDB, resetNotes, fetchUserProducts,
} from '../ViewComplaint/ViewComplaintSlice';
import { fetchInvestigation } from '../InvestigationsPanel/investigationSlice';
import { parseChanges } from '../../actions/actionsIndex';
import { writeChange, fetchAllFiles } from '../../app/changesSlice';
import createPDF from '../../actions/createPDF';

import '../../App.css';
import 'normalize.css';
import './EditComplaint.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';

const editComplaintToaster = Toaster.create({});

function EditComplaint(props) {
  const [isViewOnly, setIsViewOnly] = useState(true);
  const [displayNotes, setDisplayNotes] = useState('Loading...');
  const [localNotes, setLocalNotes] = useState('');
  const [selectedTab, setSelectedTab] = useState(null);
  const [showPanel, setShowPanel] = useState(false);
  const [showDrawer, setShowDrawer] = useState(false);
  const [assignedPhase, setAssignedPhase] = useState('');
  const [hasMDR, setHasMDR] = useState(false);

  const {
    complaintInfo,
    notesUpdated,
    fetchComplete,
    investigationInfo,
    mdrInfo,
    userProducts,
  } = props;
  const path = window.location.pathname;
  const dispatch = useDispatch();

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`;
  const role = storedUserInfo.Role;

  /**
   * Arrays define which roles only have View access to specific phases.
   *
   * 0: Admin -> All
   * 1: Investigator -> Investigation
   * 2: Intaker -> Intake, Customer Followup
   * 3: Viewer -> None
   * 4: Compliance User -> First Rev, MDR, Customer Followup, Final Review
   */
  const intakeViewers = [1, 3, 4];
  const invViewers = [2, 3, 4];
  const custFollowUpViewers = [3];
  const firstFinalMDRViewers = [1, 2, 3];

  const intakeViewOnly = intakeViewers.includes(role) || isViewOnly;
  const firstRevMDRFinalRevViewOnly = firstFinalMDRViewers.includes(role) || isViewOnly;
  const investigationsViewOnly = invViewers.includes(role) || isViewOnly;
  const custFollowupViewOnly = custFollowUpViewers.includes(role) || isViewOnly;

  /**
   * Fetches all information associated with the complaint.
   */
  useEffect(() => {
    setIsViewOnly(path.split('/').slice(-3)[0] === 'viewComplaint');
    dispatch(fetchSingleComplaint(path.split('/').slice(-2)[0]));
    dispatch(fetchInvestigation(path.split('/').slice(-2)[0]));
    dispatch(fetchUserProducts(path.split('/').slice(-2)[0]));
    dispatch(fetchAllFiles(path.split('/').slice(-2)[0]));
    dispatch(fetchMDRInfo(path.split('/').slice(-2)[0]));
  }, []);

  /**
   * Sets which tab is displayed by default based on Phase of complaint.
   */
  useEffect(() => {
    if (fetchComplete) {
      setAssignedPhase(complaintInfo[0].Phase);
      switch (complaintInfo[0].Phase) {
        case 'Intake':
          setSelectedTab('int');
          break;
        case 'FirstReview':
          setSelectedTab('fsr');
          break;
        case 'First Review':
          setSelectedTab('fsr');
          break;
        case 'Investigation':
          setSelectedTab('inv');
          break;
        case 'MDR':
          setSelectedTab('mdr');
          break;
        case 'Customer Followup':
          setSelectedTab('cfu');
          break;
        case 'CustomerFollowup':
          setSelectedTab('cfu');
          break;
        case 'FinalReview':
          setSelectedTab('fnr');
          break;
        case 'Final Review':
          setSelectedTab('fnr');
          break;
        default:
          setSelectedTab('int');
          break;
      }
      setDisplayNotes(complaintInfo[0].Notes);
      setHasMDR(complaintInfo[0].MDRIsRequired);
      setShowPanel(true);
    }
  }, [fetchComplete]);

  /**
   * If note is saved, displays notification showing whether it was
   * saved successfully or not.
   */
  useEffect(() => {
    if (notesUpdated === 'true') {
      // eslint-disable-next-line no-undef
      // setShowDrawer(false);
      editComplaintToaster.show({
        intent: Intent.SUCCESS,
        icon: 'tick',
        message: 'Note successfully saved!',
      });
      dispatch(resetNotes());
    } else if (notesUpdated === 'false') {
      // eslint-disable-next-line no-undef
      editComplaintToaster.show({
        intent: Intent.DANGER,
        icon: 'cross',
        message: 'Error: Failed to save note.',
      });
      dispatch(resetNotes());
    }
  }, [notesUpdated]);

  useEffect(() => {
    if (fetchComplete) {
      const values = {};
      values.Notes = `${localNotes}${displayNotes}`;
      values.issueID = complaintInfo[0].ComplaintID;

      // TODO: FINISH THIS
      const complaintFields = {
        issueID: path.split('/').slice(-2)[0],
        userID: storedUserInfo.UserID,
        notes: displayNotes,
      };
      const updatedNotes = localNotes + displayNotes;
      const changes = parseChanges(complaintFields, { notes: updatedNotes }, storedUserInfo);

      dispatch(writeChange(changes));
      dispatch(updateNotesToDB(values));
    }
  }, [localNotes]);

  const handleSaveNotes = (values) => {
    setLocalNotes(`[${userName} - ${new Date().toString().substring(0, 24)}]\n\n${values.rawNotes || ''}\n\n\n${localNotes || ''}`);
    // dispatch(updateNotesToDB(values));
    // setLocalNotes(`[${userName} - ${new Date().toString().substring(0, 24)}]\n\n${values.rawNotes}\n\n\n${localNotes}`);
  };

  return (
    <>
      {showPanel ? (
        <div className="padding-20 edit-complaint-wrapper">
          <div className="row-wrapper-5">
            <a href={`${window.location.origin}/home`}>
              <img src={backArrow} alt="back arrow" width="24" height="24" />
              <h3 className="back-text">Back</h3>
            </a>
          </div>
          <div className="display-flex  align-items-center">
            <h1 className="main-header-text margin-0">
              Issue ID:
              {' '}
              { complaintInfo[0].ComplaintID }
            </h1>
            <div className="noti-button-wrapper margin-left-4p">
              <Button
                outlined
                icon="export"
                type="button"
                className="bp3-button height-min-content margin-3px"
                disabled={!fetchComplete}
                onClick={() => createPDF({
                  complaint: complaintInfo[0],
                  investigation: investigationInfo[0],
                  mdr: mdrInfo[0],
                  products: userProducts,
                })}
              >
                Export
              </Button>
            </div>
            <div className="noti-button-wrapper margin-left-4p">
              <Button
                outlined
                icon="annotation"
                type="button"
                className="bp3-button height-min-content margin-3px"
                onClick={() => setShowDrawer(true)}
              >
                <div className={complaintInfo[0].Notes && complaintInfo[0].Notes.length > 1 ? 'noti-badge' : 'display-none'} />
                Notes
              </Button>
            </div>
            <p className="truncate margin-left-1p">{complaintInfo[0].Notes ? complaintInfo[0].Notes : ''}</p>
          </div>
          {selectedTab === null ? (
            <>
            </>
          ) : (
            <div>
              <Drawer
                id="notes-drawer"
                icon="info-sign"
                autoFocus
                canEscapeKeyClose
                canOutsideClickClose
                enforceFocus
                hasBackdrop
                isOpen={showDrawer}
                position={Position.RIGHT}
                usePortal
                onClose={() => setShowDrawer(false)}
                title={`Issue #${path.split('/').slice(-2)[0]} Notes`}
              >
                <div className={Classes.DRAWER_BODY}>
                  <div className={Classes.DIALOG_BODY}>
                    <Form
                      onSubmit={handleSaveNotes}
                      render={({
                        handleSubmit, submitting, values,
                      }) => (
                        <>
                          <form onSubmit={handleSubmit} autoComplete="off" className="width-100p">
                            <div>
                              <label>
                                <p><b>New Note</b></p>
                                <Field
                                  name="rawNotes"
                                  validate={validators.required}
                                >
                                  {({ input, meta }) => (
                                    <div>
                                      <TextArea
                                        {...input}
                                        className="bp3-input bp3-fill"
                                        growVertically
                                        large
                                        fill
                                        disabled={submitting || isViewOnly}
                                      />
                                      {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                    </div>
                                  )}
                                </Field>
                              </label>
                            </div>
                            <br />
                            <br />
                            <div className="row-wrapper-5">
                              <button
                                type="button"
                                className="bp3-button .modifier"
                                disabled={submitting || isViewOnly}
                                onClick={() => setShowDrawer(false)}
                              >
                                Cancel
                              </button>
                              <button
                                type="button"
                                className="bp3-button .modifier"
                                disabled={submitting || isViewOnly}
                                onClick={() => handleSaveNotes(values)}
                              >
                                Save
                              </button>
                            </div>
                          </form>
                        </>
                      )}
                    />
                  </div>
                  <div className="info-container">
                    {localNotes.length > 1 ? (
                      <>
                        <b>Most Recent Notes:</b>
                        <br />
                      </>
                    ) : (
                      ''
                    )}
                    {localNotes || ''}
                    <b>Notes:</b>
                    <br />
                    {displayNotes || ''}
                  </div>
                </div>
                {/* <div className={Classes.DRAWER_FOOTER}>Footer</div> */}
              </Drawer>
              <h2>
                Phase:
                {' '}
                { complaintInfo[0].Phase }
              </h2>
              <Tabs className="edit-complaint-tablist" defaultSelectedTabId={selectedTab} large>
                <Tab
                  className="outline-none"
                  id="int"
                  title="Issue Intake"
                  panel={intakeViewOnly || assignedPhase === 'Final Review' || assignedPhase === 'Closed' ? (<ViewIntake />) : (<EditIntake />)}
                />
                <Tab
                  className="outline-none"
                  id="fsr"
                  title="First Review"
                  disabled={assignedPhase === 'Intake'}
                  panel={firstRevMDRFinalRevViewOnly || assignedPhase === 'Final Review' || assignedPhase === 'Closed' ? (<ViewFirstReview />) : (<FirstReview />)}
                />
                <Tab
                  className="outline-none"
                  id="inv"
                  title="Investigation"
                  disabled={assignedPhase === 'Intake' || assignedPhase === 'First Review'}
                  panel={investigationsViewOnly || assignedPhase === 'Final Review' || assignedPhase === 'Closed' ? (<ViewInvestigation />) : (<Investigation />)}
                />
                <Tab
                  className="outline-none"
                  id="mdr"
                  title="MDR"
                  disabled={(assignedPhase === 'Intake' || assignedPhase === 'First Review') && !hasMDR}
                  panel={firstRevMDRFinalRevViewOnly || assignedPhase === 'Closed' ? (<ViewMDR />) : (<MDR />)}
                />
                <Tab
                  className="outline-none"
                  id="cfu"
                  title="Customer Follow-up"
                  disabled={assignedPhase === 'Intake'}
                  panel={custFollowupViewOnly || assignedPhase === 'Closed' ? (<ViewCustomerFollowup />) : (<CustomerFollowup />)}
                />
                <Tab
                  className="outline-none"
                  id="fnr"
                  title="Final Review and Closure"
                  disabled={assignedPhase !== 'Final Review' && assignedPhase !== 'Closed'}
                  panel={firstRevMDRFinalRevViewOnly || assignedPhase === 'Closed' ? (<ViewFinalReview />) : (<FinalReview />)}
                />
              </Tabs>

              {/* <Tabs className="edit-complaint-tablist" large>
                <Tab className="outline-none" id="intake" title="Intake" panel={intakeViewOnly ? (<ViewIntake />) : (<EditIntake />)} />
                <Tab className="outline-none" id="firstrev" title="First Review" panel={firstRevViewOnly ? (<ViewFirstReview />) : (<FirstReview />)} />
                <Tab className="outline-none" id="investigation" title="Investigation" panel={investigationsViewOnly ? (<Investigation />) : (<Investigation />)} />
                <Tab className="outline-none" id="mdr" title="MDR" panel={mdrViewOnly ? (<MDR />) : (<MDR />)} />
                <Tab className="outline-none" id="custfollowup" title="Customer Follow-up" panel={custFollowupViewOnly ? (<CustomerFollowup />) : (<CustomerFollowup />)} />
                <Tab className="outline-none" id="finalrev" title="Final Review and Closure" panel={finalRevViewOnly ? (<FinalReview />) : (<FinalReview />)} />
              </Tabs> */}
            </div>
          )}
        </div>
      ) : (
        <>
          <div className="spinner">
            <Spinner
              intent="primary"
              size={80}
            />
          </div>
        </>
      )}
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  notesUpdated: state.viewComplaint.notesUpdated,
  fetchComplete: state.viewComplaint.fetchComplete,
  investigationInfo: state.investigation.investigationInfo,
  mdrInfo: state.viewComplaint.mdrInfo,
  userProducts: state.viewComplaint.userProducts,
});
export default connect(mapStateToProps)(EditComplaint);
