/* eslint-disable react/prop-types */
import React, { useEffect } from 'react';
import { useDispatch, connect } from 'react-redux';
import companyLogo from '../../VTCLogo.png';
import backArrow from '../../back-arrow.png';
import { fetchSingleComplaint } from './ViewComplaintSlice';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './ViewComplaint.css';
import '../../App.css';

// eslint-disable-next-line no-unused-vars
function ViewComplaint(props) {
  // eslint-disable-next-line react/prop-types
  const dispatch = useDispatch();

  useEffect(() => {
    const path = window.location.pathname;
    dispatch(fetchSingleComplaint(path.split('/').pop()));
  }, []);

  return (
    <>
      <div className="wrapper">
        <div className="main-header">
          <div className="left-pane">
            <a href="https://vtc-complaint-system.herokuapp.com/home">
              <img src={backArrow} alt="back arrow" width="24" height="24" />
              <h3>Back</h3>
            </a>
            <h1 className="main-header-text">Complaint System</h1>
          </div>
          <div className="right-pane">
            <img src={companyLogo} alt="test" width="72" height="60" />
          </div>
        </div>

        <div className="new-complaint-wrapper">
          {/* <Tabs className="new-complaint-tablist">
            <TabList>

              <Tab disabled={false}>Complaint Intake</Tab>
              <Tab disabled={false}>First Review</Tab>
              <Tab disabled={false}>Investigations</Tab>
              <Tab disabled={false}>MDR</Tab>
              <Tab disabled={false}>Customer Followup</Tab>
              <Tab disabled={false}>Final Review and Closure</Tab>
            </TabList>

            <TabPanel>
              <ViewIntake />
            </TabPanel>
            <TabPanel>
              <ViewFirstReview />
            </TabPanel>
            <TabPanel>
              <InvestigationsPanel complaintInfo={complaint} />
            </TabPanel>
            <TabPanel>
              <MDR complaintInfo={complaint} />
            </TabPanel>
            <TabPanel>
              <CustomerFollowup complaintInfo={complaint} />
            </TabPanel>
            <TabPanel>
              <FinalReview complaintInfo={complaint} />
            </TabPanel>
          </Tabs> */}
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
});
export default connect(mapStateToProps)(ViewComplaint);
