/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-alert */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Radio, HTMLSelect, TextArea, Alert, Intent, Collapse, Button, Toaster, FileInput,
} from '@blueprintjs/core';
import Creatable from 'react-select/creatable';
import { DateInput } from '@blueprintjs/datetime';
import { useDispatch, connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import IntakeFormToRedux from '../../IntakeForm/IntakeFormToRedux';
import * as validators from '../../Validation';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './ViewIntake.css';
import '../../../App.css';
import FileUpload from '../../FileUpload/FileUpload';

const maxDate = new Date();

const custFollowupToaster = Toaster.create({});

function ViewIntake(props) {
  const [collapse, setCollapse] = useState(true);
  const [intakeFiles, setIntakeFiles] = useState(JSON.parse(window.sessionStorage.getItem('complaintFiles')));

  const {
    complaintInfo,
    userProducts,
  } = props;

  const {
    ComplaintID,
    CustomerID,
    PatientID,
    DateAware,
    DateEvent,
    PersonIntake,
    IssueDescription,
    CustFollowUpNeeded,
    ProdReturnRequested,
    RMANumber,
  } = complaintInfo[0];

  const products = JSON.parse(window.sessionStorage.getItem('products'));

  useEffect(() => {
    if (intakeFiles && intakeFiles.length > 0) {
      setIntakeFiles(intakeFiles.filter((file) => file && file.AssociatedPhase === 'Intake'));
    }
  }, []);

  const customerIDs = JSON.parse(window.sessionStorage.getItem('custIDs'));
  const patientIDs = JSON.parse(window.sessionStorage.getItem('patIDs'));
  const custArray = Object.values(customerIDs).map((obj) => obj);
  const patArray = Object.values(patientIDs).map((obj) => obj);

  const customerIDsList = (() => {
    const temp = custArray.map((i) => ({ value: i.CustomerID, label: `${i.CustomerID} ${i.CustomerName}` }));
    // temp.unshift({});
    return temp;
  })();

  const defaultCust = customerIDsList.filter((customer) => customer.value === CustomerID)[0];

  const patientIDsList = (() => {
    const temp = patArray.map((i) => ({ value: i.PatientID, label: `${i.PatientID} ${i.PatientName}` }));
    // temp.unshift({});
    return temp;
  })();

  const defaultPat = patientIDsList.filter((customer) => customer.value === PatientID)[0];

  const prodArray = Object.values(products).map((obj) => obj);

  const productList = (() => {
    const temp = prodArray.map((i) => ({ value: `${i.ProductName} ${i.ProductCodeREF}` }));
    temp.unshift({});
    return temp;
  })();

  /**
   * Handles submission of form data.
   *
   * Takes in Object containing data from all fields of the form. Parses this
   * data then writes it to the database.
   *
   * @param {Object} values                  Object containing data from user-submitted form
   * @param {String} values.customerID       ID of customer associated with complaint
   * @param {String} values.patientID        ID of patient associated with complaint
   * @param {Object} values.dateEvent        Date object representing date event occurred
   * @param {Object} values.dateAware        Date object representing date user was aware of issue
   * @param {String} values.issueDescription Description of issue
   * @param {String} values.custFollowupReq  Either 'YES' or 'NO' for whether customer follow-up is needed
   * @param {String} values.prodReturn       Either 'YES' or 'NO' for whether product is being returned
   * @param {String} values.rmaNum           RMA Number for any product(s) being returned
   */
  const handleSubmitIntake = async (values) => { };

  /**
   * Handles saving of form data.
   *
   * Takes in Object containing data from all fields of the form. Parses this
   * data then writes it to the database. Identical in behavior to handleSubmitIntake
   * but assigns the value of 'Intake' not 'First Review' to values.phase.
   *
   * @param {Object} values                  Object containing data from user-submitted form
   * @param {String} values.customerID       ID of customer associated with complaint
   * @param {String} values.patientID        ID of patient associated with complaint
   * @param {Object} values.dateEvent        Date object representing date event occurred
   * @param {Object} values.dateAware        Date object representing date user was aware of issue
   * @param {String} values.issueDescription Description of issue
   * @param {String} values.custFollowupReq  Either 'YES' or 'NO' for whether customer follow-up is needed
   * @param {String} values.prodReturn       Either 'YES' or 'NO' for whether product is being returned
   * @param {String} values.rmaNum           RMA Number for any product(s) being returned
   */
  const handleSaveIntake = async (values) => { };

  /**
   * Form element containing all fields for Issue Intake.
   */
  const FormIntake = ({ subscription }) => (
    <>
      <Form
        onSubmit={handleSubmitIntake}
        subscription={subscription}
        initialValues={{
          customerID: defaultCust,
          patientID: defaultPat,
          dateEvent: DateEvent ? new Date(DateEvent) : null,
          dateAware: DateAware ? new Date(DateAware) : null,
          custFollowupReq: CustFollowUpNeeded ? 'TRUE' : 'FALSE',
          prodReturn: ProdReturnRequested ? 'TRUE' : 'FALSE',
          issueDescription: '',
          rmaNum: RMANumber,
        }}
        render={({
          handleSubmit, form, submitting, values,
        }) => (
          <>
            <form onSubmit={handleSubmit} autoComplete="off">
              <IntakeFormToRedux form="intakeForm" />
              <table className="form-table">
                <tbody className="text-top">
                  <tr>
                    <td>
                      <label>
                        <p><b>Customer ID</b></p>
                        <Field
                          name="customerID"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div className="width-80p">
                              <Creatable
                                {...input}
                                placeholder="Search for customer..."
                                // defaultInputValue={defaultCust.label || ''}
                                options={customerIDsList}
                                isDisabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                    {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                    <td>
                      <label>
                        <p><b>Patient ID</b></p>
                        <Field
                          name="patientID"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div className="width-80p">
                              <Creatable
                                {...input}
                                placeholder="Search for patient..."
                                // defaultInputValue={defaultPat.label || ''}
                                options={patientIDsList}
                                isDisabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                    <td rowSpan={3}>
                      <FileUpload issueID={ComplaintID} phase="Intake" disable />
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <label className="width-max-content">
                        <p><b>Date of Event</b></p>
                        <Field
                          name="dateEvent"
                          // validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <>
                              <DateInput
                                {...input}
                                formatDate={(date) => date.toLocaleDateString()}
                                dateFormat="MM/DD/YYYY"
                                maxDate={maxDate}
                                parseDate={(str) => new Date(str).toLocaleDateString()}
                                onChange={input.onChange}
                                disabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                    {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                    <td>
                      <label className="width-max-content">
                        <p><b>Date First Aware</b></p>
                        <Field
                          name="dateAware"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <DateInput
                                {...input}
                                formatDate={(date) => date.toLocaleDateString()}
                                dateFormat="MM/DD/YYYY"
                                maxDate={maxDate}
                                parseDate={(str) => new Date(str).toLocaleDateString()}
                                onChange={input.onChange}
                                disabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <label>
                        <p><b>Is customer follow-up required?</b></p>
                        <Field
                          name="custFollowupReq"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <>
                                <Radio
                                  {...input}
                                  label="Yes"
                                  type="radio"
                                  value="TRUE"
                                  defaultChecked={CustFollowUpNeeded}
                                  disabled
                                />
                                <Radio
                                  {...input}
                                  label="No"
                                  type="radio"
                                  value="FALSE"
                                  defaultChecked={!CustFollowUpNeeded}
                                  disabled
                                />
                              </>
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                    {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                    <td>
                      <label>
                        <p><b>Is product being returned?</b></p>
                        <Field
                          name="prodReturn"
                          // validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <>
                                <Radio
                                  {...input}
                                  label="Yes"
                                  type="radio"
                                  value="TRUE"
                                  defaultChecked={ProdReturnRequested}
                                  disabled
                                />
                                <Radio
                                  {...input}
                                  label="No"
                                  type="radio"
                                  value="FALSE"
                                  defaultChecked={!ProdReturnRequested}
                                  disabled
                                />
                              </>
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <label>
                        <Field name="rmaNum">
                          {({ input, meta }) => (
                            <>
                              <label><p><b>RMA Number</b></p></label>
                              <input {...input} disabled className="bp3-input" placeholder="RMA Number" />
                              {meta.touched && meta.error && <span>{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td colSpan="1">
                      {userProducts && userProducts.length > 0 ? (
                        <>
                          <br />
                          <div>
                            <b>
                              Previously added products:
                            </b>
                          </div>
                          <ul className="list-group">
                            {userProducts.map((prod) => (
                              <>
                                <li className="list-group-item list-group-item-primary">
                                  <b>{'Product: '}</b>
                                  {prod.ProductName}
                                  <br />
                                  <b>{'Serial Number: '}</b>
                                  {prod.ProductSN}
                                </li>
                                <br />
                              </>
                            ))}
                          </ul>
                        </>
                      ) : (
                        <>
                        </>
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td colSpan="3">
                      <label className="issue-desc">
                        <p><b>Additional Description</b></p>
                        <Field
                          name="issueDescription"
                          validate={IssueDescription ? validators.doNothing : validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <TextArea
                                {...input}
                                cols="500"
                                className="bp3-input bp3-fill"
                                growVertically
                                large
                                fill
                                disabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
              {/* <pre>{JSON.stringify(values)}</pre> */}
              <br />
              <br />
              <br />
              <div className="row-wrapper-20">
                <button
                  type="button"
                  className="bp3-button .modifier"
                  disabled
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="bp3-button .modifier"
                  disabled
                  onClick={() => handleSaveIntake(values)}
                >
                  Save
                </button>
                <button
                  type="submit"
                  className="bp3-button .modifier"
                  disabled
                >
                  Submit for Review
                </button>
              </div>
            </form>
          </>
        )}
      />
    </>
  );

  return (
    <>
      <h1>Issue Intake</h1>
      <div className="info-card info-container">
        <Button
          minimal
          className="info-button"
          onClick={() => setCollapse(!collapse)}
        >
          {collapse ? 'Hide Information ▲' : 'Show Information ▼'}
        </Button>
        <Collapse isOpen={collapse}>
          <div className="row-wrapper-10">
            <label>
              <b>Intake submitted by:</b>
              <div>
                { PersonIntake }
              </div>
            </label>
          </div>
          <div>
            <b>Issue Description:</b>
          </div>
          <div>
            { IssueDescription }
          </div>
        </Collapse>
      </div>
      <div className="edit-intake-wrapper">
        <div className="width-100p">
          <FormIntake />
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  userProducts: state.viewComplaint.userProducts,
});

export default connect(mapStateToProps)(ViewIntake);
