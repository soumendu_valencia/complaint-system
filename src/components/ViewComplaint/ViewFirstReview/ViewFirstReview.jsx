/* eslint-disable no-nested-ternary */
/* eslint-disable no-restricted-globals */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import {
  Radio, TextArea, HTMLSelect, Collapse, Button, Classes,
} from '@blueprintjs/core';
import { Popover2, Tooltip2 } from '@blueprintjs/popover2';
import { connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import 'js-loading-overlay';
import FirstReviewToRedux from '../../FirstReview/FirstReviewToRedux';
import { dateParser, addDays } from '../../../actions/actionsIndex';
import * as validators from '../../Validation';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './ViewFirstReview.css';
import '../../../App.css';

function ViewFirstReview(props) {
  const [collapse, setCollapse] = useState(true);

  // eslint-disable-next-line react/prop-types
  const {
    complaintInfo,
    mdrInfo,
    // mdrId,
  } = props;

  const {
    IssueDescription,
    IssueIsComplaint,
    IssueNotComplaintRationale,
    ImmediateSafetyIssue,
    AsReportedCode,
    MDRIsRequired,
    FirstRevComments,
    PersonIntake,
    DateAware,
    MDRQ1,
    MDRQ2,
    MDRQ3,
  } = complaintInfo[0];

  const {
    Type,
    Classification,
    DueDate,
  } = mdrInfo[0];

  const prompt1String = 'Does the information reasonably suggest that the device may have caused or contributed to a Death or a Serious Injury?';
  const prompt2String = 'Does the information reasonably suggest that the device malfunctioned, and if the malfunction were to recur, the device would be LIKELY to cause or contribute to a death or serious injury?';
  const prompt3String = 'Is remedial action necessary due to public health risk?';

  const mdrClassificationList = [
    { value: '' },
    { value: 'Serious Injury' },
    { value: 'Death' },
    { value: 'Malfuntion' },
  ];

  /* Parsing of asReportedCodes JSON object into array */
  const asReportedCodesArray = Object.values(JSON.parse(window.sessionStorage.getItem('asReportedCodes'))).map((obj) => obj);
  const asReportedCodesList = (() => {
    // eslint-disable-next-line consistent-return
    const temp = asReportedCodesArray.map((i) => {
      if (i.AsReportedCode !== '?') {
        return { value: i.AsReportedCode, label: i.AsReportedCode };
      }
    });
    temp.unshift({ value: '?', label: '?' });
    return temp;
  })();

  const handleFirstRevSubmit = async () => { };

  const helpText = (
    <div className="help-text-wrapper">
      <p className="help-text">
        {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
        If the issue presents a possible <b>immediate</b> and <b>serious</b> safety concern to the population of eCoin patients, promptly escalate the issue to the head of QA.
      </p>
    </div>
  );
  const FirstRevForm = ({ subscription }) => (
    <Form
      onSubmit={handleFirstRevSubmit}
      subscription={subscription}
      // TODO: add MDRQ1, 2, 3 values to initialValues. Maybe try integration with form via Field tag
      initialValues={{
        isComplaint: IssueIsComplaint ? 'TRUE' : IssueIsComplaint === null ? '' : 'FALSE',
        issueRationale: IssueNotComplaintRationale,
        immediateSafetyIssue: ImmediateSafetyIssue ? 'TRUE' : ImmediateSafetyIssue === null ? '' : 'FALSE',
        asReportedCode: AsReportedCode,
        isMDRReq: MDRIsRequired ? 'TRUE' : 'FALSE',
        mdrq1: MDRQ1 ? 'TRUE' : MDRQ1 === null ? '' : 'FALSE',
        mdrq2: MDRQ2 ? 'TRUE' : MDRQ2 === null ? '' : 'FALSE',
        mdrq3: MDRQ3 ? 'TRUE' : MDRQ3 === null ? '' : 'FALSE',
        typeMDR: Type ? 'TRUE' : 'FALSE',
        mdrClassification: Classification || '',
        firstRevComments: FirstRevComments,
      }}
      render={({
        handleSubmit, values,
      }) => (
        <>
          <form onSubmit={handleSubmit} autoComplete="off">
            <FirstReviewToRedux form="firstReviewForm" />
            <table className="form-table">
              <tbody>
                <tr>
                  <td>
                    <label>
                      <p>
                        <b>
                          Is this an urgent safety issue?
                          { '  ' }
                        </b>
                        <Popover2
                          content={helpText}
                          placement="right"
                          intent="primary"
                          popoverClassName={Classes.POPOVER_BACKDROP}
                        >
                          <Tooltip2
                            content={helpText}
                            openOnTargetFocus={false}
                            placement="right"
                            usePortal={false}
                          >
                            <a><em>Help</em></a>
                          </Tooltip2>
                        </Popover2>
                      </p>
                      <Field
                        name="immediateSafetyIssue"
                        validate={validators.required}
                      >
                        {({ input, meta }) => (
                          <div>
                            <>
                              <Radio
                                {...input}
                                label="Yes"
                                type="radio"
                                value="TRUE"
                                disabled
                                defaultChecked={values.immediateSafetyIssue === 'TRUE'}
                              />
                              <Radio
                                {...input}
                                label="No"
                                type="radio"
                                value="FALSE"
                                disabled
                                defaultChecked={values.immediateSafetyIssue === 'FALSE'}
                              />
                            </>
                            {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                          </div>
                        )}
                      </Field>
                    </label>
                  </td>
                  {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                  <td>
                    <label>
                      <b>Is the issue a complaint?</b>
                      <div className="row-wrapper-10 issue-rationale-wrapper">
                        <Field
                          name="isComplaint"
                          // type="radio"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <Radio
                                {...input}
                                label="Yes"
                                value="TRUE"
                                disabled
                                defaultChecked={IssueIsComplaint !== null && IssueIsComplaint !== false}
                              />
                              <Radio
                                {...input}
                                label="No"
                                value="FALSE"
                                disabled
                                defaultChecked={IssueIsComplaint !== null && IssueIsComplaint === false}
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </div>
                    </label>
                  </td>
                  <td>
                    {values.isComplaint === 'FALSE' ? (
                      <>
                        <div className="row-wrapper-10 issue-rationale-wrapper">
                          <label>
                            <p><b>Rationale if not a complaint:</b></p>
                            <Field
                              name="issueRationale"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <TextArea
                                    {...input}
                                    className="bp3-input bp3-fill"
                                    growVertically
                                    large
                                    fill
                                    disabled
                                  />
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                      </>
                    ) : (
                      <>
                      </>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colSpan={3}>
                    <label>
                      <p><b>As-Reported Code:</b></p>
                      <Field
                        name="asReportedCode"
                        validate={validators.required}
                      >
                        {({ input, meta }) => (
                          <div>
                            <HTMLSelect
                              {...input}
                              options={asReportedCodesList}
                              onChange={input.onChange}
                              disabled
                            />
                            {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                          </div>
                        )}
                      </Field>
                    </label>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colSpan={2}>
                    {values.isComplaint === 'TRUE' ? (
                      <div>
                        <div>
                          <label>
                            <b>{prompt1String}</b>
                            <Field
                              name="mdrq1"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      {...input}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      disabled
                                      defaultChecked={values.mdrq1 === 'TRUE'}
                                    />
                                    <Radio
                                      {...input}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      disabled
                                      defaultChecked={values.mdrq1 === 'FALSE'}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                        <br />
                        <div>
                          <label>
                            <b>{prompt2String}</b>
                            <Field
                              name="mdrq2"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      {...input}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      disabled
                                      defaultChecked={values.mdrq2 === 'TRUE'}
                                    />
                                    <Radio
                                      {...input}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      disabled
                                      defaultChecked={values.mdrq2 === 'FALSE'}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                        <br />
                        <div>
                          <label>
                            <b>{prompt3String}</b>
                            <Field
                              name="mdrq3"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      {...input}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      disabled
                                      defaultChecked={values.mdrq3 === 'TRUE'}
                                    />
                                    <Radio
                                      {...input}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      disabled
                                      defaultChecked={values.mdrq3 === 'FALSE'}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </div>
                        <br />
                        {values.mdrq1 === 'TRUE' || values.mdrq2 === 'TRUE' || values.mdrq3 === 'TRUE' ? (
                          <div className="mdr-info-text">
                            <div>
                              <b>MDR Needed:</b>
                              {' '}
                              {values.mdrq3 === 'TRUE' ? '5 Day' : '30 Day'}
                            </div>
                            <div>
                              <b>MDR Due Date:</b>
                              {' '}
                              {!DueDate ? (
                                <>
                                  {addDays(DateAware, values.mdrq3 === 'TRUE' ? 5 : 30).toLocaleDateString()}
                                </>
                              ) : (
                                <>
                                  {DueDate}
                                </>
                              )}
                            </div>
                            <label>
                              <b>Type of Reportable Event:</b>
                              <Field
                                name="mdrClassification"
                                validate={validators.required}
                              >
                                {({ input, meta }) => (
                                  <div>
                                    <HTMLSelect
                                      {...input}
                                      intent="primary"
                                      options={mdrClassificationList}
                                      onChange={input.onChange}
                                      disabled
                                    />
                                    {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                  </div>
                                )}
                              </Field>
                            </label>
                          </div>
                        ) : (
                          <div className="mdr-info-text">
                            <b>MDR NOT Needed</b>
                          </div>
                        )}
                      </div>
                    ) : (
                      <></>
                    )}
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td colSpan="3">
                    <label className="first-rev-comments">
                      <p><b>Comments</b></p>
                      <Field
                        name="firstRevComments"
                      >
                        {({ input, meta }) => (
                          <div>
                            <TextArea
                              {...input}
                              className="bp3-input bp3-fill"
                              growVertically
                              large
                              fill
                              disabled
                            />
                            {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                          </div>
                        )}
                      </Field>
                    </label>
                  </td>
                </tr>
              </tbody>
            </table>
            <div className="row-wrapper-80">
              <button
                type="button"
                className="bp3-button .modifier"
                disabled
              >
                Cancel
              </button>
              <button
                type="submit"
                className="bp3-button .modifier"
                disabled
              >
                Complete First Review
              </button>
            </div>
          </form>
        </>
      )}
    />
  );

  return (
    <>
      <h1>First Review</h1>
      <div className="info-card info-container">
        <Button
          minimal
          className="info-button"
          onClick={() => setCollapse(!collapse)}
        >
          {collapse ? 'Hide Information ▲' : 'Show Information ▼'}
        </Button>
        <Collapse isOpen={collapse}>
          <table className="form-table">
            <tbody>
              <tr>
                <td>
                  <label>
                    <b>Intake submitted by:</b>
                    <div>
                      { PersonIntake || 'N/A' }
                    </div>
                  </label>
                </td>
                <td>
                  <label>
                    <b>Date First Aware:</b>
                    <div>
                      { DateAware ? dateParser(DateAware) : 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td colSpan={2}>
                  <label>
                    <b>Issue Description</b>
                    <div>
                      { IssueDescription }
                    </div>
                  </label>
                </td>
              </tr>
            </tbody>
          </table>
        </Collapse>
      </div>
      <div className="first-review-wrapper">
        <div className="width-100p">
          <FirstRevForm />
        </div>
        {/* <div className="right-pane-first-rev">

        </div> */}
      </div>
    </>
  );
}

const mapStateToProps = (state) => (
  {
    intakeData: state.intakeForm.formData,
    mdrInfo: state.viewComplaint.mdrInfo,
    complaintInfo: state.viewComplaint.complaintInfo,
  });

export default connect(mapStateToProps)(ViewFirstReview);
