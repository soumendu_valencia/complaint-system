/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const fetchSingleComplaint = createAsyncThunk(
  'testComplaints/getSingleComplaint',
  async (issueID) => {
    const response = await TestDataService.getSingleComplaint(issueID);
    return response.data.result;
  },
);

export const fetchMDRInfo = createAsyncThunk(
  'testComplaints/fetchMDRInfo',
  async (issueID) => {
    const response = await TestDataService.fetchMDRInfo(issueID);
    return response.data.result;
  },
);

export const fetchAllMDRInfo = createAsyncThunk(
  'testComplaints/fetchAllMDRInfo',
  async () => {
    const response = await TestDataService.fetchAllMDRInfo();
    return response.data.result;
  },
);

export const updateNotesToDB = createAsyncThunk(
  'api/updateNotesToDB',
  async (values) => {
    const response = await TestDataService.updateNotesToDB(values);
    return response.data;
  },
);

export const fetchUserProducts = createAsyncThunk(
  'api/fetchUserProducts',
  async (issueID) => {
    const response = await TestDataService.fetchUserProducts(issueID);
    return response.data;
  },
);

export const viewComplaintSlice = createSlice({
  name: 'viewComplaint',
  initialState: {
    complaintInfo: {
      0: {
        ComplaintID: 'placeholder',
        CustomerID: 'placeholder',
        PatientID: 'placeholder',
        DateAware: '-----------------------------',
        DateReported: '-----------------------------',
        DateEvent: '-----------------------------',
        IssueDescription: 'placeholder',
        ImmediateSafetyIssue: 'placeholder',
      },
    },
    mdrInfo: {
      0: {
        IssueID: '',
        Type: '',
        DueDate: '',
        MDRNumber: '',
        Classification: '',
        Status: '',
        ReviewDate: '',
      },
    },
    allMDRInfo: {},
    formDisabled: true,
    fetchComplete: false,
    notesUpdated: null,
    userProducts: {},
  },
  reducers: {
    resetNotes: (state) => {
      state.notesUpdated = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchSingleComplaint.fulfilled, (state, action) => {
      state.complaintInfo = action.payload;
      state.fetchComplete = true;
    });
    builder.addCase(fetchMDRInfo.fulfilled, (state, action) => {
      if (action.payload.length === 0) {
        const tempMDRInfo = {
          0: {
            IssueID: '',
            Type: '',
            DueDate: '',
            MDRNumber: '',
            Classification: '',
            Status: '',
            ReviewDate: '',
          },
        };
        window.sessionStorage.setItem('mdrInfo', JSON.stringify(tempMDRInfo));
        state.mdrInfo = tempMDRInfo;
      } else {
        window.sessionStorage.setItem('mdrInfo', JSON.stringify(action.payload));
        state.mdrInfo = action.payload;
      }
    });
    builder.addCase(fetchAllMDRInfo.fulfilled, (state, action) => {
      window.sessionStorage.setItem('allMDRInfo', JSON.stringify(action.payload));
      state.allMDRInfo = action.payload;
    });
    builder.addCase(updateNotesToDB.fulfilled, (state, action) => {
      state.notesUpdated = action.payload.toString();
    });
    builder.addCase(fetchUserProducts.fulfilled, (state, action) => {
      state.userProducts = action.payload;
    });
  },
});

// export const selectLoginData = (state) => state.viewComplaint.loginCredentialsClient;

// export const { updateInfo } = viewComplaintSlice.actions;

export const { resetNotes } = viewComplaintSlice.actions;

export default viewComplaintSlice.reducer;
