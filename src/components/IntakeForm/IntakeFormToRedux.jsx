/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { FormSpy } from 'react-final-form';
import { updateInfo } from './intakeFormSlice';

const IntakeFormToRedux = ({ form }) => (
  <FormSpy onChange={(state) => updateInfo(form, state)} />
);

export default connect(undefined, { updateInfo })(IntakeFormToRedux);
