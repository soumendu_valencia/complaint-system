/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const fetchAllCustomersAndPatients = createAsyncThunk(
  'testComplaints/fetchAllComplaintsStatus',
  async () => {
    const response = await TestDataService.getCustAndPat();
    return response.data;
  },
);

export const fetchAllProducts = createAsyncThunk(
  'testComplaints/fetchAllProducts',
  async () => {
    const response = await TestDataService.getProducts();
    return response.data;
  },
);

export const writeIntakeToDb = createAsyncThunk(
  'testComplaints/writeIntakeToDb',
  async (values) => {
    const response = await TestDataService.writeIntake(values);
    return response.data;
  },
);

export const updateIntakeToDb = createAsyncThunk(
  'testComplaints/updateIntakeToDb',
  async (values) => {
    const response = await TestDataService.updateIntake(values);
    return response.data;
  },
);

export const fetchNewestComplaint = createAsyncThunk(
  'testComplaints/fetchNewestComplaint',
  async (userID) => {
    const response = await TestDataService.fetchNewestComplaint(userID);
    return response.data;
  },
);

export const updateCustFollowup = createAsyncThunk(
  'testComplaints/updateCustFollowup',
  async (values) => {
    const response = await TestDataService.updateCustFollowup(values);
    return response.data.result;
  },
);

export const writeProducts = createAsyncThunk(
  'api/writeProducts',
  async (values) => {
    const response = await TestDataService.writeProducts(values);
    return response.data;
  },
);

export const intakeFormSlice = createSlice({
  name: 'intakeForm',
  initialState: {
    intakeSubmitted: false,
    intakeUpdateSubmitted: null,
    scopeID: null,
    formData: {},
    custIDs: {},
    patIDs: {},
    products: {},
    tabIndex: 0,
    recentComplaint: '',
    custFollowupSuccess: null,
    custPatFetched: null,
    prodsUploaded: null,
  },
  reducers: {
    updateInfo: (state, action) => {
      state.formData = action.payload;
    },
    resetCustPat: (state) => {
      state.custPatFetched = null;
    },
    resetIntSub: (state) => {
      state.intakeUpdateSubmitted = null;
    },
    resetProdsUploaded: (state) => {
      state.prodsUploaded = null;
    },
    setProdsUploadedTrue: (state) => {
      state.prodsUploaded = 'true';
    },
    resetCustFollowupSuccess: (state) => {
      state.custFollowupSuccess = null;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(fetchAllCustomersAndPatients.fulfilled, (state, action) => {
      state.custIDs = action.payload.custResult;
      state.patIDs = action.payload.patResult;
      window.sessionStorage.setItem('custIDs', JSON.stringify(action.payload.custResult));
      window.sessionStorage.setItem('patIDs', JSON.stringify(action.payload.patResult));
      state.custPatFetched = 'true';
    });
    builder.addCase(fetchAllProducts.fulfilled, (state, action) => {
      state.products = action.payload.result;
      window.sessionStorage.setItem('products', JSON.stringify(action.payload.result));
    });
    builder.addCase(writeIntakeToDb.fulfilled, (state, action) => {
      state.intakeSubmitted = action.payload.response;
      state.scopeID = action.payload.scopeID;
      window.sessionStorage.setItem('rcID', JSON.stringify(action.payload.scopeID));
    });
    builder.addCase(updateIntakeToDb.fulfilled, (state, action) => {
      state.intakeUpdateSubmitted = action.payload.result.toString();
    });
    builder.addCase(fetchNewestComplaint.fulfilled, (state, action) => {
      state.intakeSubmitted = action.payload.result;
    });
    builder.addCase(updateCustFollowup.fulfilled, (state, action) => {
      state.custFollowupSuccess = action.payload.toString();
    });
    builder.addCase(writeProducts.fulfilled, (state, action) => {
      state.prodsUploaded = action.payload.toString();
    });
  },
});

export const selectIntakeInfo = (state) => state.intakeForm.formData;

export const {
  updateInfo,
  resetIntSub,
  resetProdsUploaded,
  setProdsUploadedTrue,
  resetCustFollowupSuccess,
} = intakeFormSlice.actions;

export default intakeFormSlice.reducer;
