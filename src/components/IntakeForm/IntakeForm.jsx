/* eslint-disable max-len */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-param-reassign */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import {
  Radio, Alert, Intent, TextArea,
} from '@blueprintjs/core';
import { DateInput } from '@blueprintjs/datetime';
import { useDispatch, connect } from 'react-redux';
import { Form, Field } from 'react-final-form';
import Creatable from 'react-select/creatable';
import * as intakeFormSlice from './intakeFormSlice';
import { writeChange, uploadFiles, resetWriteChangeSuccessful } from '../../app/changesSlice';
import { parseChanges } from '../../actions/actionsIndex';
import * as validators from '../Validation';
import AddProduct from '../NewComplaint/AddProduct';
// import FileUploadIntake from '../FileUpload/FileUploadIntake';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './IntakeForm.css';
import '../../App.css';

const selectedFiles = [];

function IntakeForm(props) {
  const [formValues, setFormValues] = useState();
  const [showAlert, setShowAlert] = useState(false);
  const [hideCustPat, setHideCustPat] = useState(true);
  const [custList, setCustList] = useState([]);
  const [patList, setPatList] = useState([]);
  const [saveProdList, setSaveProdList] = useState(false);
  const [scopeID, setScopeID] = useState(null);
  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`;

  const history = useHistory();
  const dispatch = useDispatch();
  const {
    products,
    // filesUploadSuccessful,
    prodsUploaded,
    formSubmitted,
    custPatFetched,
    writeChangeSuccessful,
  } = props;

  let custArray;
  let patArray;
  let customerIDsList;
  let patientIDsList;

  /* Retrieve list of all customers & patients, and list of all products on page load. */
  useEffect(() => {
    dispatch(intakeFormSlice.fetchAllCustomersAndPatients());
    dispatch(intakeFormSlice.fetchAllProducts());
  }, []);

  /* Parse customer/patient list into an array (from JSON format). */
  useEffect(() => {
    // eslint-disable-next-line prefer-template
    if (custPatFetched === 'true') {
      const customerIDs = JSON.parse(window.sessionStorage.getItem('custIDs'));
      const patientIDs = JSON.parse(window.sessionStorage.getItem('patIDs'));
      custArray = Object.values(customerIDs).map((obj) => obj);
      patArray = Object.values(patientIDs).map((obj) => obj);

      customerIDsList = (() => {
        const temp = custArray.map((i) => ({ value: i.CustomerID, label: `${i.CustomerID} ${i.CustomerName}` }));
        return temp;
      })();

      patientIDsList = (() => {
        const temp = patArray.map((i) => ({ value: i.PatientID, label: `${i.PatientID} ${i.PatientName}` }));
        return temp;
      })();

      setCustList(customerIDsList);
      setPatList(patientIDsList);
      setHideCustPat(false);
    }
  }, [custPatFetched]);

  const prodArray = Object.values(products).map((obj) => obj);

  const productList = (() => {
    const temp = prodArray.map((i) => ({ value: `${i.ProductName} ${i.ProductCodeREF}` }));
    temp.unshift({});
    return temp;
  })();

  /**
   * Handles submission of form data.
   *
   * Takes in Object containing data from all fields of the form. Parses this
   * data then writes it to the database.
   *
   * @param {Object} values                  Object containing data from user-submitted form
   * @param {String} values.customerID       ID of customer associated with complaint
   * @param {String} values.patientID        ID of patient associated with complaint
   * @param {Object} values.dateEvent        Date object representing date event occurred
   * @param {Object} values.dateAware        Date object representing date user was aware of issue
   * @param {String} values.issueDescription Description of issue
   * @param {String} values.custFollowupReq  Either 'YES' or 'NO' for whether customer follow-up is needed
   * @param {String} values.prodReturn       Either 'YES' or 'NO' for whether product is being returned
   * @param {String} values.rmaNum           RMA Number for any product(s) being returned
   */
  const handleSubmitIntake = async (values) => {
    values.dateReported = new Date().toLocaleDateString();
    values.personIntake = userName;
    values.phase = 'First Review';
    values.customerID = values.customerID.value;
    values.patientID = values.patientID.value;

    setSaveProdList(true);
    setFormValues({ ...values });
    dispatch(intakeFormSlice.updateInfo(values));
    dispatch(intakeFormSlice.writeIntakeToDb(values));
  };

  /**
   * Handles saving of form data.
   *
   * Takes in Object containing data from all fields of the form. Parses this
   * data then writes it to the database. Identical in behavior to handleSubmitIntake
   * but assigns the value of 'Intake' not 'First Review' to values.phase.
   *
   * @param {Object} values                  Object containing data from user-submitted form
   * @param {String} values.customerID       ID of customer associated with complaint
   * @param {String} values.patientID        ID of patient associated with complaint
   * @param {Object} values.dateEvent        Date object representing date event occurred
   * @param {Object} values.dateAware        Date object representing date user was aware of issue
   * @param {String} values.issueDescription Description of issue
   * @param {String} values.custFollowupReq  Either 'YES' or 'NO' for whether customer follow-up is needed
   * @param {String} values.prodReturn       Either 'YES' or 'NO' for whether product is being returned
   * @param {String} values.rmaNum           RMA Number for any product(s) being returned
   */
  const handleSave = async (values) => {
    values.dateReported = new Date().toLocaleDateString();
    values.personIntake = userName;
    values.phase = 'Intake';
    values.customerID = values.customerID ? values.customerID.value : null;
    values.patientID = values.patientID ? values.patientID.value : null;

    setSaveProdList(true);
    setFormValues({ ...values });
    dispatch(intakeFormSlice.writeIntakeToDb(values));
    dispatch(intakeFormSlice.updateInfo(values));
  };

  useEffect(() => {
    if (formSubmitted) {
      const scopeId = JSON.parse(window.sessionStorage.getItem('rcID'));
      setScopeID(scopeId);
      // save values to sessionstorage onsubmit then JSON.parse them here?
      const rcIDs = JSON.parse(window.sessionStorage.getItem('rcIDs'));
      rcIDs.push({ scopeId });
      window.sessionStorage.setItem('rcIDs', JSON.stringify(rcIDs));

      const complaintFields = {
        issueID: scopeId,
        userID: storedUserInfo.UserID,
        dateReported: '',
        personIntake: '',
        phase: '',
        customerID: '',
        patientID: '',
        // products: JSON.stringify(selectedProductList),
        // files: JSON.stringify(uploadedFiles),
        dateEvent: '',
        dateAware: '',
        issueDescription: '',
        custFollowUpReq: '',
        prodReturn: '',
        rmaNum: '',
      };

      formValues.issueID = scopeId;
      const changes = parseChanges(complaintFields, formValues, storedUserInfo);

      if (selectedFiles.length > 0) {
        selectedFiles.forEach((file) => { file.issueID = scopeId; });
        dispatch(uploadFiles(selectedFiles));
        selectedFiles.length = 0;
      }
      dispatch(writeChange(changes));
    }
  }, [formSubmitted]);

  useEffect(() => {
    if (writeChangeSuccessful === 'true' && prodsUploaded === 'true') {
      dispatch(intakeFormSlice.resetProdsUploaded());
      dispatch(intakeFormSlice.resetIntSub());
      dispatch(resetWriteChangeSuccessful());
      history.push('/home');
      history.go(0);
    }
  }, [writeChangeSuccessful, prodsUploaded]);

  /* Handles cancelling of "delete intake" pop-up. */
  const handleCancel = () => {
    setShowAlert(false);
  };

  /* Handles confirming of "delete intake" pop-up. */
  const handleConfirm = () => {
    history.go(0);
  };

  const maxDate = new Date();

  const FormIntake = ({ subscription }) => (
    <>
      <Form
        onSubmit={handleSubmitIntake}
        subscription={subscription}
        render={({
          handleSubmit, form, submitting, values, pristine,
        }) => {
          if (!window.setFormValue) window.setFormValue = form.mutators.setVal;
          return (
            <>
              <h1>Issue Intake</h1>
              <form onSubmit={handleSubmit} autoComplete="off" className="width-100p">
                <div>
                  <table className="form-table">
                    <tbody>
                      <tr>
                        <td>
                          <label className="width-80p">
                            <p><b>Customer ID</b></p>
                            {hideCustPat ? (
                              <p>LOADING...</p>
                            ) : (
                              <Field
                                name="customerID"
                                validate={validators.required}
                              >
                                {({ input, meta }) => (
                                  <div className="width-100p">
                                    <Creatable
                                      {...input}
                                      placeholder="Search for customer..."
                                      options={custList}
                                    />
                                    {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                  </div>
                                )}
                              </Field>
                            )}
                          </label>
                        </td>
                        {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                        <td>
                          <label className="width-80p">
                            <p><b>Patient ID</b></p>
                            {hideCustPat ? (
                              <p>LOADING...</p>
                            ) : (
                              <Field
                                name="patientID"
                                validate={validators.required}
                              >
                                {({ input, meta }) => (
                                  <div>
                                    <Creatable
                                      {...input}
                                      placeholder="Search for patient..."
                                      options={patList}
                                    />
                                    {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                  </div>
                                )}
                              </Field>
                            )}
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          <label className="width-max-content">
                            <p><b>Date of Event</b></p>
                            <Field
                              name="dateEvent"
                              // validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <DateInput
                                    {...input}
                                    formatDate={(date) => date.toLocaleDateString()}
                                    dateFormat="MM/DD/YYYY"
                                    parseDate={(str) => new Date(str).toLocaleDateString()}
                                    maxDate={maxDate}
                                    onChange={input.onChange}
                                  />
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </td>
                        {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                        <td>
                          <label className="width-max-content">
                            <p><b>Date First Aware</b></p>
                            <Field
                              name="dateAware"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <DateInput
                                    {...input}
                                    formatDate={(date) => date.toLocaleDateString()}
                                    dateFormat="MM/DD/YYYY"
                                    parseDate={(str) => new Date(str).toLocaleDateString()}
                                    maxDate={maxDate}
                                    onChange={input.onChange}
                                  />
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          <label className="width-fit-content">
                            <p><b>Is customer follow-up required?</b></p>
                            <Field
                              name="custFollowupReq"
                              type="radio"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      name={input.name}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      onChange={(event) => input.onChange(event.target.value)}
                                    />
                                    <Radio
                                      name={input.name}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      onChange={(event) => input.onChange(event.target.value)}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </td>
                        {/* <td>&nbsp;&nbsp;&nbsp;&nbsp;</td> */}
                        <td>
                          <label className="width-fit-content">
                            <p><b>Is product being returned?</b></p>
                            <Field
                              name="prodReturn"
                              type="radio"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <>
                                    <Radio
                                      name={input.name}
                                      label="Yes"
                                      type="radio"
                                      value="TRUE"
                                      onChange={(event) => input.onChange(event.target.value)}
                                    />
                                    <Radio
                                      name={input.name}
                                      label="No"
                                      type="radio"
                                      value="FALSE"
                                      onChange={(event) => input.onChange(event.target.value)}
                                    />
                                  </>
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </td>
                        <td>
                          <label className="width-fit-content">
                            <Field name="rmaNum">
                              {({ input, meta }) => (
                                <>
                                  <label><p><b>RMA Number</b></p></label>
                                  <input {...input} className="bp3-input" placeholder="RMA Number" />
                                  {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                                </>
                              )}
                            </Field>
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                      </tr>
                      <tr>
                        <td colSpan="3">
                          <label className="issue-desc">
                            <p><b>Issue Description</b></p>
                            <Field
                              name="issueDescription"
                              validate={validators.required}
                            >
                              {({ input, meta }) => (
                                <div>
                                  <TextArea
                                    {...input}
                                    className="bp3-input bp3-fill"
                                    growVertically
                                    large
                                    fill
                                  />
                                  {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                </div>
                              )}
                            </Field>
                          </label>
                        </td>
                      </tr>
                      <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                      </tr>
                      <tr>
                        <td colSpan={2} className="text-top">
                          <b>Leave blank if no associated product:</b>
                          <br />
                          <br />
                          <AddProduct products={productList} scopeID={scopeID} save={saveProdList} />
                        </td>
                        <td>
                          {/* <FileUploadIntake issueID={scopeID} phase="Intake" /> */}
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  <br />
                  <div>
                    <Alert
                      canEscapeKeyCancel
                      canOutsideClickCancel
                      isOpen={showAlert}
                      intent={Intent.DANGER}
                      onCancel={handleCancel}
                      onConfirm={handleConfirm}
                      icon="trash"
                      cancelButtonText="Cancel"
                      confirmButtonText="Delete Intake"
                    >
                      <p>Are you sure you want to cancel? Inputted data won&apos;t be saved.</p>
                    </Alert>
                    <div className="row-wrapper-5">
                      <button
                        type="button"
                        className="bp3-button .modifier"
                        disabled={submitting}
                        onClick={() => setShowAlert(true)}
                      >
                        Cancel
                      </button>
                      <button
                        type="button"
                        className="bp3-button .modifier"
                        disabled={submitting || pristine}
                        onClick={() => handleSave(values)}
                      >
                        Save
                      </button>
                      <button
                        type="submit"
                        className="bp3-button .modifier"
                        disabled={submitting || pristine}
                      >
                        Submit for Review
                      </button>
                    </div>
                  </div>
                </div>
              </form>
            </>
          );
        }}
      />
    </>
  );

  return (
    <>
      <div className="width-vw-75">
        <FormIntake />
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  scopeID: state.intakeForm.scope_id,
  products: state.intakeForm.products,
  filesUploadSuccessful: state.changes.filesUploadSuccessful,
  formSubmitted: state.intakeForm.intakeSubmitted,
  custPatFetched: state.intakeForm.custPatFetched,
  prodsUploaded: state.intakeForm.prodsUploaded,
  writeChangeSuccessful: state.changes.writeChangeSuccessful,
});

export default connect(mapStateToProps)(IntakeForm);
