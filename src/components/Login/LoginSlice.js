/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const writeLoginInfo = createAsyncThunk(
  'api/writeLoginInfo',
  async (loginInfo) => {
    const response = await TestDataService.writeLoginInfo(loginInfo);
    return response.data;
  },
);

export const loginSlice = createSlice({
  name: 'login',
  initialState: {
    loginSuccess: null,
    userInfo: {},
    userInfoReady: null,
  },
  reducers: {
    updateInfo: (state, action) => {
      state.userInfo = action.payload;
    },
    resetLogin: (state) => {
      state.loginSuccess = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(writeLoginInfo.fulfilled, (state, action) => {
      state.loginSuccess = action.payload.isLoginSuccessful.toString();
      state.userInfo = action.payload.userInfo;
      state.userInfoReady = action.payload.isLoginSuccessful.toString();
    });
  },
});

export const selectLoginData = (state) => state.loginData.loginCredentialsClient;

export const { updateInfo, resetLogin } = loginSlice.actions;

export default loginSlice.reducer;
