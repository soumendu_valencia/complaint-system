/* eslint-disable max-len */
/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Form, Field, FormSpy } from 'react-final-form';
import {
  Button, Dialog, Callout, Toaster, Intent,
} from '@blueprintjs/core';
// import { useHistory } from 'react-router-dom';
import { writeLoginInfo, resetLogin } from './LoginSlice';
import { fetchAllUsers } from '../InvestigationsPanel/investigationSlice';
import { fetchAsReportedCodes } from '../FirstReview/firstReviewSlice';
import { fetchAllCustomersAndPatients } from '../IntakeForm/intakeFormSlice';
import { updateAccount, resetUASuccess } from '../CreateAccount/CreateAccountSlice';
import { daysElapsed } from '../../actions/actionsIndex';
import logoName from '../../logo_icon_name.jpg';
import * as validators from '../Validation';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './Login.css';
import '../../App.css';

const updateToaster = Toaster.create({});

function Login(props) {
  const dispatch = useDispatch();
  const history = useHistory();
  const [showPwdUpdate, setShowPwdUpdate] = useState(false);
  // const [showIncorrectLogin, setShowIncorrectLogin] = useState(false);
  // const history = useHistory();
  const {
    userInfo,
    loginSuccess,
    updateAccountSuccess,
  } = props;

  // function sleep(ms) {
  //   return new Promise((resolve) => setTimeout(resolve, ms));
  // }

  const completeLogin = () => {
    window.sessionStorage.setItem('userLoggedIn', JSON.stringify(true));
    dispatch(resetUASuccess());
    history.push('/home');
    history.go(0);
  };

  useEffect(() => {
    if (loginSuccess === 'true') {
      window.sessionStorage.setItem('rcIDs', JSON.stringify([]));

      // while (!userInfo) {
      //   sleep(500);
      // }

      window.sessionStorage.setItem('userInfo', JSON.stringify(userInfo));
      const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
      const today = new Date();

      if (daysElapsed(storedUserInfo.PassLastUpdated, today) >= 365) {
        dispatch(resetLogin());
        setShowPwdUpdate(true);
      } else {
        completeLogin();
      }
    }
  }, [loginSuccess]);

  useEffect(() => {
    if (updateAccountSuccess === 'true') {
      updateToaster.show({
        intent: Intent.SUCCESS,
        icon: 'tick',
        message: 'Password updated',
      });
      setShowPwdUpdate(false);
      completeLogin();
    }
  }, [updateAccountSuccess]);

  const handleLoginSubmit = async (loginInfo) => {
    dispatch(writeLoginInfo(loginInfo));
    dispatch(fetchAllUsers());
    dispatch(fetchAllCustomersAndPatients());
    dispatch(fetchAsReportedCodes());
  };

  /**
   * onSubmit for password update form.
   *
   * Updates the user's password and password expiry date in the database.
   *
   * @param {Object} values         data from "update password" form
   * @param {String} values.newPass the user's new password
   */
  const handleUpdatePwd = (values) => {
    values.username = userInfo.Username;
    values.dateUpdated = new Date();
    dispatch(updateAccount(values));
  };

  /* Handles closing of "update password" overlay. */
  const handleOverlayClose = () => {
    dispatch(resetUASuccess());
    setShowPwdUpdate(false);
  };

  /**
   * Validates new password.
   *
   * Takes in a password string and returns a warning if the password does not
   * meet the specified security requirements.
   *
   * @param {String} value User-entered password
   */
  const password = (value) => (value && /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/i.test(value)
    ? undefined
    : (
      <Callout
        className="width-fit-content"
        intent="warning"
        icon="warning-sign"
      >
        Password must have at least: 8 characters, one uppercase letter, one lowercase letter, one number, one special character
      </Callout>
    )
  );

  const LoginForm = ({ subscription }) => (
    <Form
      onSubmit={handleLoginSubmit}
      subscription={subscription}
      initialValues={{
        username: '',
        password: '',
      }}
      render={({
        // eslint-disable-next-line no-unused-vars
        form, values, handleSubmit, submitting,
      }) => (
        <>
          <FormSpy subscription={{ values: true }}>
            {({ vals }) => (
              <pre>
                {JSON.stringify(vals, 0, 2)}
              </pre>
            )}
          </FormSpy>
          <form onSubmit={handleSubmit} autoComplete="off" className="login-form">
            {/* <h1 className="margin-top-0">Login:</h1> */}
            <img src={logoName} alt="VTC Logo" width={200} height={50} />
            <div className="input-fields">
              <div className="input-label">
                <label>Username:</label>
              </div>
              <div>
                <Field
                  name="username"
                  validate={validators.required}
                >
                  {({ input, meta }) => (
                    <>
                      <input {...input} className="bp3-input" placeholder="Username" />
                      {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                    </>
                  )}
                </Field>
              </div>
              <br />
              <div className="input-label">
                <label className="input-label">Password:</label>
              </div>
              <div>
                <Field
                  name="password"
                  validate={validators.required}
                >
                  {({ input, meta }) => (
                    <>
                      <input {...input} type="password" className="bp3-input login-input" placeholder="Password" autoComplete="off" />
                      {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                    </>
                  )}
                </Field>
              </div>
              {loginSuccess === 'false' ? (
                <span className="warning-text">
                  Incorrect username or password.
                </span>
              ) : (
                <></>
              )}
              <div className="submit-wrapper">
                <Button
                  type="submit"
                  intent="primary"
                  className="bp3-button .modifier submit-button"
                  disabled={submitting}
                  // onClick={() => handleLoginSubmit(values)}
                >
                  Log In
                </Button>
              </div>
            </div>
          </form>
        </>
      )}
    />
  );

  return (
    <>
      <div className="login-wrapper">
        <Dialog
          isOpen={showPwdUpdate}
          icon="warning-sign"
          title="Update Password"
          canEscapeKeyClose={false}
          canOutsideClickClose={false}
          onClose={handleOverlayClose}
        >
          <div className="margin-10">
            <b><p>Your password has expired. Please enter a new one:</p></b>
            <br />
            <Form
              onSubmit={handleUpdatePwd}
              initialValues={{
                newPass: '',
                newPassConfirm: '',
              }}
              render={({
                // eslint-disable-next-line no-unused-vars
                handleSubmit, form, values, submitting,
              }) => (
                <>
                  <form onSubmit={handleSubmit} autoComplete="off">
                    <div className="row-wrapper-5 input-label">
                      <label>
                        <b>Enter Current Password:</b>
                        <div>
                          <Field
                            name="password"
                            className="login-input"
                            validate={validators.required}
                          >
                            {({ input, meta }) => (
                              <>
                                <input
                                  {...input}
                                  type="password"
                                  className="bp3-input"
                                  placeholder="Confirm Password"
                                  autoComplete="off"
                                />
                                <div>
                                  {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                                </div>
                              </>
                            )}
                          </Field>
                        </div>
                      </label>
                    </div>
                    <div className="row-wrapper-5 input-label">
                      <label>
                        <b>Enter New Password:</b>
                        <div>
                          <Field
                            name="newPass"
                            className="login-input"
                            validate={validators.required}
                          >
                            {({ input, meta }) => (
                              <>
                                <input
                                  {...input}
                                  type="password"
                                  className="bp3-input"
                                  placeholder="Confirm Password"
                                  autoComplete="off"
                                />
                                <div>
                                  {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                                </div>
                              </>
                            )}
                          </Field>
                        </div>
                      </label>
                      <label>
                        <b>Confirm New Password:</b>
                        <div>
                          <Field
                            name="newPassConfirm"
                            className="login-input"
                            validate={validators.required}
                          >
                            {({ input, meta }) => (
                              <>
                                <input
                                  {...input}
                                  type="password"
                                  className="bp3-input"
                                  placeholder="Confirm Password"
                                  autoComplete="off"
                                />
                                <div>
                                  {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                                </div>
                              </>
                            )}
                          </Field>
                        </div>
                      </label>
                    </div>
                    <span>
                      {values.newPass === values.newPassConfirm ? (
                        <></>
                      ) : (
                        <p className="warning-text">Passwords must match</p>
                      )}
                    </span>
                    <span>
                      {values.password === values.newPass ? (
                        <p className="warning-text margin-0">New password cannot be same as old password.</p>
                      ) : (
                        <></>
                      )}
                    </span>
                    <span>
                      {password(values.password)}
                    </span>
                    <br />
                    <div className="row-wrapper-5">
                      <button
                        type="submit"
                        intent="primary"
                        disabled={submitting || values.newPass !== values.newPassConfirm || !values.newPass || !values.password || values.password === values.newPass}
                        className="bp3-button bp3-intent-primary submit-button"
                      >
                        Submit
                      </button>
                    </div>
                  </form>
                </>
              )}
            />
          </div>
        </Dialog>
        <LoginForm />
      </div>
    </>
  );
}
const mapStateToProps = (state) => ({
  userInfo: state.login.userInfo,
  loginSuccess: state.login.loginSuccess,
  updateAccountSuccess: state.createAccount.updateAccountSuccess,
});

export default connect(mapStateToProps)(Login);
