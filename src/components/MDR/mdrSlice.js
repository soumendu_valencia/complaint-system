/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const writeMDRToDb = createAsyncThunk(
  'testComplaints/writeMDRToDb',
  async (values) => {
    const response = await TestDataService.writeMDRToDb(values);
    return response.data;
  },
);

export const updateMDRToDb = createAsyncThunk(
  'testComplaints/updateMDRToDb',
  async (values) => {
    const response = await TestDataService.updateMDRToDb(values);
    return response.data;
  },
);

export const writeMDRNum = createAsyncThunk(
  'api/writeMDrNum',
  async (mdrNum) => {
    const response = await TestDataService.writeMDRNum(mdrNum);
    return response.data;
  },
);

export const mdrSlice = createSlice({
  name: 'mdr',
  initialState: {
    mdrDataSubmitted: false,
    mdrUpdated: null,
    mdrId: null,
    formData: {},
  },
  reducers: {
    updateInfo: (state, action) => {
      state.formData = action.payload;
    },
    resetMDRUpdated: (state) => {
      state.mdrUpdated = null;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(writeMDRToDb.fulfilled, (state, action) => {
      state.mdrDataSubmitted = action.payload.response;
      state.mdrId = action.payload.scopeID;
    });
    builder.addCase(updateMDRToDb.fulfilled, (state, action) => {
      state.mdrUpdated = action.payload.toString();
    });
  },
});

export const selectAssignInfo = (state) => state.investigation.formData;

export const { updateInfo, resetMDRUpdated } = mdrSlice.actions;

export default mdrSlice.reducer;
