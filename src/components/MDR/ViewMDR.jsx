/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch, connect } from 'react-redux';
import { Form, Field, FormSpy } from 'react-final-form';
import {
  FileInput, Radio, Toaster, Intent, Callout, Button,
} from '@blueprintjs/core';
import { DateInput } from '@blueprintjs/datetime';
import { updateMDRToDb, resetMDRUpdated } from './mdrSlice';
import { writeChange, fetchAllFiles } from '../../app/changesSlice';
import { parseChanges, dateParser, daysElapsed } from '../../actions/actionsIndex';
import * as validators from '../Validation';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import '../../App.css';
import './MDR.css';

const mdrToaster = Toaster.create({});

function ViewMDR(props) {
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [uploadedFiles, setUploadedFiles] = useState({});
  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    complaintInfo,
    mdrInfo,
    mdrUpdated,
  } = props;

  const {
    ComplaintID,
    MDRIsRequired,
    DateAware,
    DateReported,
    DateFirstReview,
  } = complaintInfo[0];

  const {
    Id,
    IssueID,
    Type,
    DueDate,
    MDRNumber,
    Classification,
    Status,
    ReviewDate,
    DateSubmitted,
    SupplementalNeeded,
    DateSuppSubmitted,
    MDRSubmitted,
    SuppMDRSubmitted,
  } = mdrInfo[0];

  const renderUploadedFiles = (files) => {
    <>
      {Object.keys(files).map((fileName) => (
        <div key={fileName}>{fileName}</div>
      ))}
    </>;
  };

  const prepMDRNum = (mdrId) => (
    `3010878085-${new Date().getFullYear()}-${String(mdrId).padStart(4, '0')}`
  );

  const uploadedFilesArray = Object.values(uploadedFiles).map((obj) => obj);
  window.sessionStorage.setItem('uploadedFilesMDR', JSON.stringify(uploadedFilesArray));

  const renderDataTable = (filesList) => {
    let result = `
    <table border=0 style=none>
      <tbody>
        <tr>
          <th>Name</th>
          <th>Date Last Modified</th>
          <th>Comments</th>
        </tr>`;
    result += filesList.map((file) => {
      const picked = (({
        name,
        lastModifiedDate,
        comment,
      }) => ({
        name,
        lastModifiedDate,
        comment,
      }))(file);
      return (
        `<tr>
          <td>${picked.name}</td>
          <td>${picked.lastModifiedDate.toLocaleDateString()}</td>
          <td>${picked.comment}</td>
        </tr>`
      );
    });
    result += '</tbody></table>';
    return result.replaceAll(',', '');
  };

  const handleSaveMDR = async (values) => { };

  const today = new Date();

  const maxDate = new Date();

  const typeDays = Type ? 5 : 30;

  const MDRForm = ({ subscription }) => (
    <div className="font-med">
      {!MDRIsRequired ? (
        <h1>No MDR associated with this complaint.</h1>
      ) : (
        <>
          {MDRSubmitted ? (
            <>
              <Callout
                className="padding-top-2 border-radius-10"
                intent="success"
              >
                <div className="font-x-large">
                  <b>
                    MDR Submitted
                  </b>
                </div>
                <div className="font-med">
                  <b>
                    Submitted on:
                    {' '}
                    { dateParser(DateSubmitted) }
                  </b>
                </div>
              </Callout>
            </>
          ) : (
            <>
              {/* TODO: fix */}
              {daysElapsed(DateFirstReview, today) <= typeDays ? (
                <Callout
                  className="padding-top-2 border-radius-10"
                  intent={daysElapsed(DateFirstReview, DueDate) <= 5 ? 'danger' : 'warning'}
                >
                  <div className="font-x-large">
                    <b>
                      Date MDR due:
                      {' '}
                      { dateParser(DueDate) }
                    </b>
                  </div>
                  <div className="font-large">
                    <b>
                      Days Open:
                      {' '}
                      { daysElapsed(DateFirstReview, today) }
                    </b>
                  </div>
                </Callout>
              ) : (
                <Callout
                  className="padding-top-2 border-radius-10"
                  intent="danger"
                >
                  <div className="font-x-large">
                    <b>
                      Days Overdue:
                      {' '}
                      { daysElapsed(DueDate, today) }
                    </b>
                  </div>
                  <div className="font-x-large">
                    <b>
                      Date MDR Due:
                      {' '}
                      { dateParser(DueDate) }
                    </b>
                  </div>
                </Callout>
              )}
            </>
          )}
          <br />
          <div>
            {/* <div className="font-large row-wrapper-10">
              <b>
                Date MDR submitted:
                {' '}
                {dateParser(DateSubmitted)}
              </b>
            </div> */}
            <table className="form-table">
              <tbody>
                <tr>
                  <td>
                    <label>
                      <b>Date First Aware:</b>
                      <div>
                        { dateParser(DateAware) }
                      </div>
                    </label>
                  </td>
                  <td>
                    <label>
                      <b>Date Reported:</b>
                      <div>
                        { dateParser(DateReported) }
                      </div>
                    </label>
                  </td>
                  <td>
                    <label>
                      <b>Date First Review:</b>
                      <div>
                        { dateParser(DateFirstReview) }
                      </div>
                    </label>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                  <td>
                    <label>
                      <b>Type of Report:</b>
                      <div>
                        { Type ? '5 Day' : '30 Day' }
                      </div>
                    </label>
                  </td>
                  <td>
                    <label>
                      <b>Type of Reportable Event:</b>
                      <div>
                        { Classification }
                      </div>
                    </label>
                  </td>
                  <td>
                    <label>
                      <b>
                        MDR Number:
                      </b>
                      <div>
                        {MDRNumber || 'ERR'}
                        {' '}
                        {MDRNumber ? (
                          <Button
                            small
                            onClick={() => { navigator.clipboard.writeText(prepMDRNum(Id)); }}
                          >
                            Copy
                          </Button>
                        ) : (
                          <></>
                        )}
                      </div>
                    </label>
                  </td>
                </tr>
                <tr>
                  <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </div>
          <h2 className="padding-10">
            Please Upload any relevant documents.
          </h2>
          <Form
            onSubmit={handleSaveMDR}
            subscription={subscription}
            initialValues={{
              mdrSubmitted: MDRSubmitted ? 'TRUE' : 'FALSE',
              dateMDRSubmitted: DateSubmitted ? new Date(DateSubmitted) : '',
              suppMDRNeeded: SupplementalNeeded ? 'TRUE' : 'FALSE',
              suppMDRSubmitted: SuppMDRSubmitted ? 'TRUE' : 'FALSE',
            }}
            render={({
              handleSubmit, form, values,
            }) => (
              <>
                <FormSpy subscription={{ values: true }}>
                  {({ vals }) => (
                    <pre>
                      {JSON.stringify(vals, 0, 2)}
                    </pre>
                  )}
                </FormSpy>
                <div className="row-wrapper-5">
                  <form onSubmit={handleSubmit} autoComplete="off">
                    <div>
                      <FileInput
                        className="bp3-button .modifier"
                        multiple
                        text="Choose file..."
                        disabled
                      />
                      {isFilePicked ? (
                        <div>
                          <h4>File Information:</h4>
                          <p>
                            <b>File Name:</b>
                            {' '}
                            {selectedFile.name}
                          </p>
                          <p>
                            <b>File Type:</b>
                            {' '}
                            {selectedFile.type}
                          </p>
                          <p>
                            <b>Size in bytes:</b>
                            {' '}
                            {selectedFile.size}
                          </p>
                          <p>
                            <b>Last Modified:</b>
                            {' '}
                            {selectedFile.lastModifiedDate.toLocaleDateString()}
                          </p>
                          <br />
                          <p><b>Add Comments:</b></p>
                          <Field name="fileComments">
                            {({ input, meta }) => (
                              <>
                                <input
                                  className="bp3-input"
                                  id="comment-input"
                                  placeholder="Comments..."
                                  disabled
                                />
                                {meta.touched && meta.error && <span>{meta.error}</span>}
                              </>
                            )}
                          </Field>
                        </div>
                      ) : (
                        <p>Select a file to show details</p>
                      )}
                    </div>
                    <div className="padding-top-20">
                      <button
                        type="button"
                        className="bp3-button .modifier"
                        disabled
                      >
                        Upload
                      </button>
                    </div>
                    <br />
                    <table className="form-table">
                      <tbody>
                        <tr>
                          <td>
                            <label>
                              <b>Has MDR been submitted?</b>
                              <div className="row-wrapper-10">
                                <Field
                                  name="mdrSubmitted"
                                  // type="radio"
                                  validate={validators.required}
                                >
                                  {({ input, meta }) => (
                                    <div>
                                      <Radio
                                        {...input}
                                        label="Yes"
                                        value="TRUE"
                                        // disabled={MDRSubmitted}
                                        // type="radio"
                                        defaultChecked={MDRSubmitted}
                                        disabled
                                      />
                                      <Radio
                                        {...input}
                                        label="No"
                                        value="FALSE"
                                        // disabled={MDRSubmitted}
                                        // type="radio"
                                        defaultChecked={!MDRSubmitted}
                                        disabled
                                      />
                                      {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                    </div>
                                  )}
                                </Field>
                              </div>
                            </label>
                          </td>
                          <td>
                            {values.mdrSubmitted === 'TRUE' ? (
                              <label>
                                <p><b>Date MDR Submitted:</b></p>
                                <Field
                                  name="dateMDRSubmitted"
                                  validate={validators.required}
                                >
                                  {({ input, meta }) => (
                                    <div>
                                      <DateInput
                                        {...input}
                                        formatDate={(date) => date.toLocaleDateString()}
                                        dateFormat="MM/DD/YYYY"
                                        maxDate={maxDate}
                                        parseDate={(str) => new Date(str).toLocaleDateString()}
                                        onChange={input.onChange}
                                        disabled
                                      />
                                      {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                    </div>
                                  )}
                                </Field>
                              </label>
                            ) : (
                              <></>
                            )}
                          </td>
                        </tr>
                        <tr>
                          <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        </tr>
                        <tr>
                          <td>
                            <label>
                              <b>Supplemental MDR required?</b>
                              <div className="row-wrapper-10">
                                <Field
                                  name="suppMDRNeeded"
                                  // type="radio"
                                  validate={validators.required}
                                >
                                  {({ input, meta }) => (
                                    <div>
                                      <Radio
                                        {...input}
                                        label="Yes"
                                        value="TRUE"
                                        disabled
                                        // type="radio"
                                        defaultChecked={SupplementalNeeded}
                                      />
                                      <Radio
                                        {...input}
                                        label="No"
                                        value="FALSE"
                                        disabled
                                        // type="radio"
                                        defaultChecked={!SupplementalNeeded}
                                      />
                                      {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                    </div>
                                  )}
                                </Field>
                              </div>
                            </label>
                          </td>
                          <td>
                            {values.suppMDRNeeded === 'TRUE' ? (
                              <>
                                <label>
                                  <b>Has supplemental MDR been submitted?</b>
                                  <div className="row-wrapper-10">
                                    <Field
                                      name="suppMDRSubmitted"
                                      // type="radio"
                                      validate={validators.composeValidators(validators.required, validators.mdrSubmitted)}
                                    >
                                      {({ input, meta }) => (
                                        <div>
                                          <Radio
                                            {...input}
                                            label="Yes"
                                            value="TRUE"
                                            disabled
                                            // type="radio"
                                            defaultChecked={SuppMDRSubmitted}
                                          />
                                          <Radio
                                            {...input}
                                            label="No"
                                            value="FALSE"
                                            disabled
                                            // type="radio"
                                            defaultChecked={!SuppMDRSubmitted}
                                          />
                                          {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                        </div>
                                      )}
                                    </Field>
                                  </div>
                                </label>
                              </>
                            ) : (
                              <>
                              </>
                            )}
                          </td>
                          <td>
                            {values.suppMDRSubmitted === 'TRUE' ? (
                              <label>
                                <p><b>Date Supplemental MDR Submitted:</b></p>
                                <Field
                                  name="dateSuppMDRSubmitted"
                                  validate={validators.required}
                                >
                                  {({ input, meta }) => (
                                    <div>
                                      <DateInput
                                        {...input}
                                        formatDate={(date) => date.toLocaleDateString()}
                                        dateFormat="MM/DD/YYYY"
                                        maxDate={maxDate}
                                        parseDate={(str) => new Date(str).toLocaleDateString()}
                                        onChange={input.onChange}
                                        disabled
                                      />
                                      {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                                    </div>
                                  )}
                                </Field>
                              </label>
                            ) : (
                              <></>
                            )}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <div className="row-wrapper-5">
                      <button
                        type="button"
                        className="bp3-button .modifier"
                        onClick={() => handleSaveMDR(values, false)}
                        disabled
                      >
                        Save
                      </button>
                    </div>
                  </form>
                </div>
              </>
            )}
          />
        </>
      )}
    </div>
  );

  return (
    <>
      <h1>MDR</h1>
      <MDRForm />
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  mdrInfo: state.viewComplaint.mdrInfo,
  formSubmitted: state.intakeForm.intakeSubmitted,
  mdrUpdated: state.mdr.mdrUpdated,
});

export default connect(mapStateToProps)(ViewMDR);
