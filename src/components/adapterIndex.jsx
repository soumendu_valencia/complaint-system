/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from 'react';
import { DateInput } from '@blueprintjs/datetime';
import {
  HTMLSelect, TextArea, Alignment, Checkbox, FileInput,
} from '@blueprintjs/core';

const DateInputAdapter = ({ input: { onChange, value }, ...rest }) => (
  <div>
    <DateInput
      formatDate={(date) => date.toLocaleDateString()}
      dateFormat="MM/DD/YYYY"
      parseDate={(str) => new Date(str).toLocaleDateString()}
      selected={value}
      onChange={(date) => onChange(date)}
      {...rest}
    />
  </div>
);

const TextAreaAdapter = ({ input: { onChange, value }, meta: { touched, error, warning }, ...rest }) => (
  <div>
    <TextArea
      className="bp3-input bp3-fill"
      growVertically
      large
      fill
      value={value}
      onChange={(date) => onChange(date)}
      {...rest}
    />
    {touched
    && ((error
      && (
      <span>
        {error}
      </span>
      ))
      || (warning
        && (
        <span>
          {warning}
        </span>
        )))}
  </div>
);

const SelectAdapter = ({ input, ...rest }) => (
  <HTMLSelect {...input} {...rest} />
);

const CheckboxAdapter = ({ input, ...rest }) => (
  <Checkbox
    {...input}
    {...rest}
    alignIndicator={Alignment.RIGHT}
    checked={input.checked}
    defaultChecked
    onChange={input.onChange}
  />
);

const CheckboxRightAdapter = ({ input, ...rest }) => (
  <Checkbox
    {...input}
    {...rest}
    alignIndicator={Alignment.RIGHT}
    checked={input.checked}
    onChange={input.onChange}
  />
);

const CheckboxLeftAdapter = ({ input, ...rest }) => (
  <Checkbox
    {...input}
    {...rest}
    alignIndicator={Alignment.LEFT}
    checked={input.checked}
    onChange={input.onChange}
  />
);

const FileInputAdapter = ({ input, ...rest }) => (
  <FileInput
    {...input}
    {...rest}
    className="bp3-button .modifier"
    text="Choose file..."
  />
);

export {
  DateInputAdapter,
  TextAreaAdapter,
  SelectAdapter,
  CheckboxAdapter,
  CheckboxRightAdapter,
  CheckboxLeftAdapter,
  FileInputAdapter,
};
