/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Form, Field } from 'react-final-form';
import { useHistory } from 'react-router-dom';
import { Buffer } from 'buffer';
import { connect, useDispatch } from 'react-redux';
import {
  FileInput, Radio, TextArea, Toaster, Intent, Collapse, Button, Spinner,
} from '@blueprintjs/core';
import { updateCustFollowup, resetCustFollowupSuccess } from '../IntakeForm/intakeFormSlice';
import { parseChanges } from '../../actions/actionsIndex';
import { writeChange, writeFile, resetFilesSuccessful } from '../../app/changesSlice';
import FileUpload from '../FileUpload/FileUpload';
import * as validators from '../Validation';

import './CustomerFollowup.css';
import '../../App.css';

const custFollowupToaster = Toaster.create({});

function CustomerFollowup(props) {
  const [collapse, setCollapse] = useState(true);
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [showSpinner, setShowSpinner] = useState(false);
  const [cfFiles, setCFFiles] = useState(JSON.parse(window.sessionStorage.getItem('complaintFiles')));
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    complaintInfo,
    custFollowupSuccess,
    uploadFilesSuccessful,
    writeFilesSuccessful,
  } = props;

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`;
  // const cfFiles = ;
  // console.table(cfFiles);

  const {
    ComplaintID,
    CustomerID,
    CustFollowUpNeeded,
    ProdReturnRequested,
    ProductReturned,
    CustomerUpdates,
    RMANumber,
    DateFirstReview,
  } = complaintInfo[0];

  const renderUploadedFiles = (files) => {
    <>
      {Object.keys(files).map((fileName) => (
        <div key={fileName}>{fileName}</div>
      ))}
    </>;
  };

  useEffect(() => {
    if (cfFiles && cfFiles.length > 0) {
      setCFFiles(cfFiles.filter((file) => file && file.AssociatedPhase === 'Customer Follow-up'));
    }
  }, []);

  useEffect(() => {
    if (custFollowupSuccess === 'true') {
      setShowSpinner(false);
      history.go(0);
      custFollowupToaster.show({
        intent: Intent.SUCCESS,
        icon: 'tick',
        message: 'Customer Follow-up successfully saved!',
      });
      dispatch(resetCustFollowupSuccess());
    } else if (custFollowupSuccess === 'false') {
      setShowSpinner(false);
      history.go(0);
      custFollowupToaster.show({
        intent: Intent.DANGER,
        icon: 'tick',
        message: 'ERROR: Issue saving Customer Follow-up',
      });
      dispatch(resetCustFollowupSuccess());
    }
  }, [custFollowupSuccess]);

  useEffect(() => {
    if (uploadedFiles) {
      renderUploadedFiles(uploadedFiles);
    }
  }, [uploadedFiles]);

  useEffect(() => {
    if (uploadFilesSuccessful && writeFilesSuccessful) {
      // TODO: write this
    }
  }, [uploadFilesSuccessful, writeFilesSuccessful]);

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  const onUpload = async (file) => {
    const u8 = await file.arrayBuffer();
    const fileBuffer = Buffer.from(u8).toString('base64');

    setUploadedFiles(
      {
        ...uploadedFiles, [file.name]: file,
      },
      dispatch(writeFile({
        issueID: ComplaintID,
        fileBuffer,
        fileName: file.name,
        fileType: file.type,
        dateUploaded: new Date().toDateString(),
        personUpload: userName,
        phase: 'Customer Follow-up',
        // TODO: add fileLink
      })),
    );
  };

  const handleSaveCustFollowup = async (values) => {
    setShowSpinner(true);
    values.issueID = ComplaintID;
    values.customerUpdates = values.customerUpdates === undefined || values.customerUpdates === null ? CustomerUpdates : `${CustomerUpdates}\n\n${userName} - ${new Date().toString().substring(0, 24)}\n${values.customerUpdates}`;

    const complaintFields = {
      issueID: ComplaintID,
      custFollowupReq: CustFollowUpNeeded ? 'TRUE' : 'FALSE',
      isProdReturned: ProductReturned ? 'TRUE' : 'FALSE',
      customerUpdates: CustomerUpdates,
      rmaNum: RMANumber,
    };

    // if (isFilePicked) {
    //   const updatedFileList = [];

    //   uploadedFiles.forEach((file) => {
    //     updatedFileList.push({
    //       issueID: ComplaintID,
    //       fileName: file.name,
    //       fileSize: file.size,
    //       fileType: file.type,
    //       dateUploaded: new Date().toLocaleDateString,
    //       personUpload: userName,
    //       associatedPhase: 'Customer Follow-up',
    //       // TODO: GET FILE URL
    //       // fileLink:
    //     });
    //   });

    //   dispatch(uploadedFiles(uploadedFiles));
    //   dispatch(writeFile(updatedFileList(updatedFileList)));
    // }

    const changes = parseChanges(complaintFields, values, storedUserInfo);
    if (changes.valuesDiff.length > 0) {
      dispatch(writeChange(changes));
    }

    dispatch(updateCustFollowup(values));
  };

  const handleCancel = () => {
    history.go(0);
  };

  const uploadedFilesArray = uploadedFiles ? Object.values(uploadedFiles).map((obj) => obj) : [];
  window.sessionStorage.setItem('uploadedFiles', JSON.stringify(uploadedFilesArray));

  const renderDataTable = (filesList) => {
    let result = `
    <table border=0 style=none>
      <tbody>
        <tr>
          <th>Name</th>
        </tr>`;
    result += filesList.map((file) => {
      const picked = (({
        FileName,
      }) => ({
        FileName,
      }))(file);
      return (
        `<tr>
          <td>${picked.FileName}</td>
        </tr>`
      );
    });
    result += '</tbody></table>';
    return result.replaceAll(',', '');
  };

  let table;
  useEffect(() => {
    if (cfFiles) {
      table = renderDataTable(cfFiles);
    }
  }, [cfFiles]);

  const FormCustFollowup = () => (
    <Form
      onSubmit={handleSaveCustFollowup}
      initialValues={{
        isProdReturned: ProdReturnRequested ? 'TRUE' : 'FALSE',
        custFollowupReq: CustFollowUpNeeded ? 'TRUE' : 'FALSE',
        rmaNum: RMANumber,
      }}
      render={({
        handleSubmit, form, values,
      }) => (
        <>
          <table className="form-table">
            <tbody className="text-top">
              <tr>
                <td>
                  <label>
                    <p><b>Is customer follow-up needed?</b></p>
                    <Field
                      name="custFollowupReq"
                      // type="radio"
                      validate={validators.required}
                    >
                      {({ input, meta }) => (
                        <div className="width-min-content">
                          <Radio
                            {...input}
                            label="Yes"
                            value="TRUE"
                            defaultChecked={CustFollowUpNeeded}
                          />
                          <Radio
                            {...input}
                            label="No"
                            value="FALSE"
                            defaultChecked={!CustFollowUpNeeded}
                          />
                          {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                        </div>
                      )}
                    </Field>
                  </label>
                </td>
                <td>
                  <label>
                    <p><b>Has product return been requested?</b></p>
                    <Field
                      name="isProdReturned"
                      // type="radio"
                      validate={validators.required}
                    >
                      {({ input, meta }) => (
                        <div className="width-min-content">
                          <Radio
                            {...input}
                            label="Yes"
                            value="TRUE"
                            defaultChecked={ProdReturnRequested}
                          />
                          <Radio
                            {...input}
                            label="No"
                            value="FALSE"
                            defaultChecked={!ProdReturnRequested}
                          />
                          {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                        </div>
                      )}
                    </Field>
                  </label>
                </td>
                <td rowSpan="2">
                  <b>Upload Files:</b>
                  <br />
                  <br />
                  <FileUpload issueID={ComplaintID} phase="Customer Follow-up" />
                  {/* <button
                    type="button"
                    className="bp3-button .modifier"
                    // disabled={selectedFile && selectedFile.size > 4000000}
                    onClick={() => onUpload(selectedFile)}
                  >
                    Upload File
                  </button> */}
                  {selectedFile && selectedFile.size > 400000 ? (
                    <span className="warning-text">File size must be less than 4 MB.</span>
                  ) : (
                    <></>
                  )}
                </td>
                <td rowSpan="2">
                  <div className="padding-top-10 width-max-content text-align-left">
                    {/* TODO: FIX */}
                    <h3>Uploaded Files:</h3>
                    {/* eslint-disable-next-line react/no-danger */}
                    <div dangerouslySetInnerHTML={{ __html: table }} />
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <label>
                    <Field name="rmaNum">
                      {({ input, meta }) => (
                        <>
                          <label><p><b>RMA Number</b></p></label>
                          <input {...input} className="bp3-input" placeholder="RMA Number" />
                          {meta.touched && meta.error && <span>{meta.error}</span>}
                        </>
                      )}
                    </Field>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td colSpan="4">
                  <label className="issue-desc">
                    <p><b>Customer Updates</b></p>
                    <p><em>Detail all attempts to retrieve product</em></p>
                    <Field
                      name="customerUpdates"
                    >
                      {({ input, meta }) => (
                        <div>
                          <TextArea
                            {...input}
                            className="bp3-input bp3-fill"
                            growVertically
                            large
                            fill
                          />
                          {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                        </div>
                      )}
                    </Field>
                  </label>
                </td>
              </tr>
              <tr>
                <td>
                  <button
                    type="button"
                    className="bp3-button .modifier"
                    onClick={() => handleCancel()}
                  >
                    Cancel
                  </button>
                </td>
                <td>
                  <button
                    type="submit"
                    className="bp3-button .modifier"
                    // onClick={() => handleSaveCustFollowup(values)}
                  >
                    Save
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </>
      )}
    />
  );

  return (
    <>
      {showSpinner ? (
        <div className="width-100p">
          <Spinner className="bp3-intent-primary center" size={80} />
        </div>
      ) : (
        <>
          <h1>Customer Follow-up</h1>
          <div className="info-card info-container">
            <Button
              minimal
              className="info-button"
              onClick={() => setCollapse(!collapse)}
            >
              {collapse ? 'Hide Information ▲' : 'Show Information ▼'}
            </Button>
            <Collapse isOpen={collapse}>
              <div>
                <b>
                  Customer ID:
                  {' '}
                  {CustomerID}
                </b>
              </div>
              <br />
              <div>
                <b>
                  Customer has
                  {CustFollowUpNeeded ? ' ' : ' NOT '}
                  requested follow-up.
                </b>
              </div>
              <br />
              <div>
                <b>
                  Product return has
                  {ProdReturnRequested ? ' ' : ' NOT '}
                  been requested.
                </b>
              </div>
              <br />
              <div>
                <b>Customer Updates:</b>
                <br />
                {CustomerUpdates}
              </div>
            </Collapse>
          </div>
          <div className="font-med">
            <br />
            <FormCustFollowup />
          </div>
        </>
      )}
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  custFollowupSuccess: state.intakeForm.custFollowupSuccess,
  uploadFilesSuccessful: state.changes.uploadFilesSuccessful,
  writeFilesSuccessful: state.changes.writeFilesSuccessful,
});

export default connect(mapStateToProps)(CustomerFollowup);
