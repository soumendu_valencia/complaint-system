/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-globals */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import {
  Radio, TextArea, Alert, Intent, Button, Collapse, FileInput, Toaster,
} from '@blueprintjs/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, connect } from 'react-redux';
import { Form, Field, FormSpy } from 'react-final-form';
import 'js-loading-overlay';
import { updateInfo, updateInvestigationToDb, resetInvUpdated } from './investigationSlice';
import { changePhase, writeChange } from '../../app/changesSlice';
import { parseChanges } from '../../actions/actionsIndex';
import * as validators from '../Validation';
import FileUpload from '../FileUpload/FileUpload';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './InvestigationsPanel.css';
import '../../App.css';

const selectedFiles = [];

const loadingConfigs = {
  overlayBackgroundColor: '#FFFDFD',
  overlayOpacity: 0.6,
  spinnerIcon: 'ball-spin-clockwise',
  spinnerColor: '#4BA3F3',
  spinnerSize: '3x',
  overlayIDName: 'overlay',
  spinnerIDName: 'spinner',
  offsetX: 0,
  offsetY: 0,
  containerID: null,
  lockScroll: false,
  overlayZIndex: 9998,
  spinnerZIndex: 9999,
};

function Investigation(props) {
  const [collapse, setCollapse] = useState(true);
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [uploadedFiles, setUploadedFiles] = useState([{}]);
  const [showAlert, setShowAlert] = useState(false);

  const history = useHistory();
  const dispatch = useDispatch();

  const userInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${userInfo.FirstName} ${userInfo.LastName}`;

  const {
    // eslint-disable-next-line no-unused-vars
    complaintInfo,
    investigationInfo,
    formSubmitted,
    invUpdated,
  } = props;

  const {
    InvestigationStatus,
    ComplaintConfirmed,
    RootCauseCode,
    CAPAFileReviewResults,
    DHRReviewResults,
    RiskFileReviewResults,
    InvestigationSummary,
    InvestigationNarrative,
    DateInvClosed,
    PersonInvClose,
  } = investigationInfo[0];

  const {
    ComplaintID,
    IssueDescription,
    AsReportedCode,
    Phase,
  } = complaintInfo[0];

  useEffect(() => {
    if (invUpdated === 'true') {
      dispatch(resetInvUpdated());
      history.push(`/editComplaint/${ComplaintID}/Investigation`);
      history.go(0);
    }
  }, [invUpdated]);

  const handleSubmitInvestigation = async (values) => {
    values.dateInvestigation = new Date().toLocaleDateString();
    values.status = 'CLOSED';
    values.issueID = ComplaintID;
    values.phase = 'Final Review';
    values.investigator = `${userInfo.FirstName} ${userInfo.LastName}`;

    const complaintFields = {
      issueID: ComplaintID,
      invStatus: InvestigationStatus,
      dateInvestigation: DateInvClosed,
      phase: Phase,
      investigator: PersonInvClose,
      files: JSON.stringify(uploadedFiles),
      complaintConfirmed: ComplaintConfirmed ? 'TRUE' : ComplaintConfirmed === null ? '' : 'FALSE',
      rootCauseCode: RootCauseCode,
      capaResults: CAPAFileReviewResults,
      dhrRevResults: DHRReviewResults,
      riskResults: RiskFileReviewResults,
      investigationSummary: InvestigationSummary,
      investigationNarrative: InvestigationNarrative,
    };

    const changes = parseChanges(complaintFields, values, userInfo);

    if (changes.valuesDiff.length > 0) {
      dispatch(writeChange(changes));
    }

    dispatch(updateInfo(values));
    dispatch(updateInvestigationToDb(values));
    const phaseInfo = {
      issueId: ComplaintID,
      status: 'CLOSED',
      phase: 'Final Review',
    };
    dispatch(changePhase(phaseInfo));
  };

  const handleSaveInvestigation = async (values) => {
    values.invStatus = 'OPEN';
    values.issueID = ComplaintID;
    values.phase = 'Investigation';
    values.investigator = `${userInfo.FirstName} ${userInfo.LastName}`;
    values.dateInvestigation = new Date().toLocaleDateString();

    const complaintFields = {
      issueID: ComplaintID,
      status: InvestigationStatus,
      dateInvestigation: DateInvClosed,
      phase: Phase,
      investigator: PersonInvClose,
      files: JSON.stringify(uploadedFiles),
      complaintConfirmed: ComplaintConfirmed === null ? ComplaintConfirmed : ComplaintConfirmed ? 'TRUE' : 'FALSE',
      rootCauseCode: RootCauseCode,
      capaResults: CAPAFileReviewResults,
      dhrRevResults: DHRReviewResults,
      riskResults: RiskFileReviewResults,
      investigationSummary: InvestigationSummary,
      investigationNarrative: InvestigationNarrative,
    };

    const changes = parseChanges(complaintFields, values, userInfo);

    dispatch(writeChange(changes));
    dispatch(updateInvestigationToDb(values));
  };

  const handleCancel = () => {
    history.go(0);
  };

  const handleConfirm = () => {
    setShowAlert(false);
  };

  function readFileDataAsBinary(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = (event) => {
        resolve(event.target.result);
      };

      reader.onerror = (err) => {
        reject(err);
      };

      reader.readAsArrayBuffer(file);
    });
  }

  const handleFileUpload = async (file) => {
    if (file) {
      const newFile = {
        fileName: file.name,
        uploadDate: new Date().toLocaleDateString(),
        personUpload: userName,
        phase: 'Intake',
        fileType: file.type,
        fileBuffer: await readFileDataAsBinary(file),
      };

      selectedFiles.push(newFile);
      setUploadedFiles([...selectedFiles]);
    }
  };

  const FormInvestigation = ({ subscription }) => (
    <>
      <Form
        onSubmit={handleSubmitInvestigation}
        subscription={subscription}
        initialValues={{
          complaintConfirmed: ComplaintConfirmed ? 'TRUE' : ComplaintConfirmed === null ? '' : 'FALSE',
          rootCauseCode: RootCauseCode,
          dhrRevResults: DHRReviewResults,
          capaResults: CAPAFileReviewResults,
          riskResults: RiskFileReviewResults,
          investigationSummary: InvestigationSummary,
          investigationNarrative: InvestigationNarrative,
        }}
        render={({
          handleSubmit, form, submitting, values,
        }) => (
          <>
            <FormSpy subscription={{ values: true }}>
              {({ vals }) => (
                <pre>
                  {JSON.stringify(vals, 0, 2)}
                </pre>
              )}
            </FormSpy>

            <form onSubmit={handleSubmit} autoComplete="off">
              <table className="form-table">
                <tbody>
                  <tr>
                    <td>
                      <label className="width-max-content">
                        <p><b>Was the complaint confirmed?</b></p>
                        <div className="row-wrapper-10">
                          <Field
                            name="complaintConfirmed"
                            // type="radio"
                            validate={validators.required}
                          >
                            {({ input, meta }) => (
                              <div>
                                <Radio
                                  {...input}
                                  label="Yes"
                                  value="TRUE"
                                  defaultChecked={values.complaintConfirmed === 'TRUE'}
                                />
                                <Radio
                                  {...input}
                                  label="No"
                                  value="FALSE"
                                  defaultChecked={values.complaintConfirmed === 'FALSE'}
                                />
                                {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                              </div>
                            )}
                          </Field>
                        </div>
                      </label>
                    </td>
                    <td>
                      <label className="width-max-content">
                        <p><b>Root Cause Code:</b></p>
                        <Field name="rootCauseCode" validate={validators.required}>
                          {({ input, meta }) => (
                            <>
                              <input {...input} className="bp3-input" placeholder="Root Cause" />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                    <td>
                      <FileUpload issueID={ComplaintID} phase="Investigation" />
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <label className="width-max-content">
                        <p><b>CAPA File Review Results</b></p>
                        <Field name="capaResults" validate={validators.required}>
                          {({ input, meta }) => (
                            <>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                maxLength={255}
                                large
                                fill
                              />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                    <td>
                      <label className="width-max-content">
                        <p><b>DHR Review Results</b></p>
                        <Field
                          name="dhrRevResults"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                maxLength={255}
                                large
                                fill
                              />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                    <td>
                      <label className="width-max-content">
                        <p><b>Risk File Review Results</b></p>
                        <Field
                          name="riskResults"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                maxLength={255}
                                large
                                fill
                              />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td colSpan={3}>
                      <label>
                        <p><b>Investigation Summary</b></p>
                        <Field
                          name="investigationSummary"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill text-area-inv"
                                growVertically
                                large
                                fill
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td colSpan={3}>
                      <label>
                        <p><b>Investigation Narrative</b></p>
                        <Field
                          name="investigationNarrative"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                large
                                fill
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br />
              <br />
              <div className="row-wrapper-20">
                <Alert
                  cancelButtonText="Cancel"
                  confirmButtonText="Confirm"
                  icon="trash"
                  intent={Intent.DANGER}
                  isOpen={showAlert}
                  onCancel={handleConfirm}
                  onConfirm={handleCancel}
                >
                  <p>Are you sure you want to cancel? Inputted data won&apos;t be saved.</p>
                </Alert>
                <button
                  type="button"
                  className="bp3-button .modifier"
                  disabled={submitting}
                  onClick={() => setShowAlert(true)}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="bp3-button .modifier"
                  disabled={submitting}
                  onClick={() => handleSaveInvestigation(values)}
                >
                  Save
                </button>
                <button
                  type="submit"
                  className="bp3-button .modifier"
                >
                  Close Investigation
                </button>
              </div>
            </form>
          </>
        )}
      />
    </>
  );

  return (
    <>
      <h1>Investigation</h1>
      <div className="info-card info-container">
        <Button
          minimal
          className="info-button"
          onClick={() => setCollapse(!collapse)}
        >
          {collapse ? 'Hide Information ▲' : 'Show Information ▼'}
        </Button>
        <Collapse isOpen={collapse}>
          <div className="row-wrapper-10">
            <label>
              <b>As-Reported Code:</b>
              <div>
                { AsReportedCode }
              </div>
            </label>
          </div>
          <div>
            <b>Issue Description:</b>
          </div>
          <div>
            { IssueDescription }
          </div>
        </Collapse>
      </div>
      <div className="intake-wrapper">
        <div className="width-100p">
          <FormInvestigation />
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  investigationInfo: state.investigation.investigationInfo,
  invUpdated: state.investigation.invUpdated,
});

export default connect(mapStateToProps)(Investigation);
