/* eslint-disable no-nested-ternary */
/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable no-restricted-globals */
/* eslint-disable array-callback-return */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import {
  Radio, TextArea, Alert, Intent, Button, Collapse, FileInput, Toaster,
} from '@blueprintjs/core';
import { useHistory } from 'react-router-dom';
import { useDispatch, connect } from 'react-redux';
import { Form, Field, FormSpy } from 'react-final-form';
import * as validators from '../Validation';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './InvestigationsPanel.css';
import '../../App.css';

const selectedFiles = [];

function ViewInvestigation(props) {
  const [collapse, setCollapse] = useState(true);
  const [uploadedFiles, setUploadedFiles] = useState([{}]);
  const [showAlert, setShowAlert] = useState(false);

  const history = useHistory();
  const dispatch = useDispatch();

  const userInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${userInfo.FirstName} ${userInfo.LastName}`;

  const {
    // eslint-disable-next-line no-unused-vars
    complaintInfo,
    investigationInfo,
    formSubmitted,
    invUpdated,
  } = props;

  const {
    InvestigationStatus,
    ComplaintConfirmed,
    RootCauseCode,
    CAPAFileReviewResults,
    DHRReviewResults,
    RiskFileReviewResults,
    InvestigationSummary,
    InvestigationNarrative,
    DateInvClosed,
    PersonInvClose,
  } = investigationInfo[0];

  const {
    ComplaintID,
    IssueDescription,
    AsReportedCode,
    Phase,
  } = complaintInfo[0];

  const handleSubmitInvestigation = async (values) => { };

  const handleSaveInvestigation = async (values) => { };

  const FormInvestigation = ({ subscription }) => (
    <>
      <Form
        onSubmit={handleSubmitInvestigation}
        subscription={subscription}
        initialValues={{
          complaintConfirmed: ComplaintConfirmed ? 'TRUE' : ComplaintConfirmed === null ? '' : 'FALSE',
          rootCauseCode: RootCauseCode,
          dhrRevResults: DHRReviewResults,
          capaResults: CAPAFileReviewResults,
          riskResults: RiskFileReviewResults,
          investigationSummary: InvestigationSummary,
          investigationNarrative: InvestigationNarrative,
        }}
        render={({
          handleSubmit, form, submitting, values,
        }) => (
          <>
            <FormSpy subscription={{ values: true }}>
              {({ vals }) => (
                <pre>
                  {JSON.stringify(vals, 0, 2)}
                </pre>
              )}
            </FormSpy>

            <form onSubmit={handleSubmit} autoComplete="off">
              <table className="form-table">
                <tbody>
                  <tr>
                    <td>
                      <label className="width-max-content">
                        <p><b>Was the complaint confirmed?</b></p>
                        <div className="row-wrapper-10">
                          <Field
                            name="complaintConfirmed"
                            // type="radio"
                            validate={validators.required}
                          >
                            {({ input, meta }) => (
                              <div>
                                <Radio
                                  {...input}
                                  label="Yes"
                                  value="TRUE"
                                  disabled
                                  defaultChecked={values.complaintConfirmed === 'TRUE'}
                                />
                                <Radio
                                  {...input}
                                  label="No"
                                  value="FALSE"
                                  disabled
                                  defaultChecked={values.complaintConfirmed === 'FALSE'}
                                />
                                {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                              </div>
                            )}
                          </Field>
                        </div>
                      </label>
                    </td>
                    <td>
                      <label className="width-max-content">
                        <p><b>Root Cause Code:</b></p>
                        <Field name="rootCauseCode" validate={validators.required}>
                          {({ input, meta }) => (
                            <>
                              <input {...input} disabled className="bp3-input" placeholder="Root Cause" />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td>
                      <label className="width-max-content">
                        <p><b>CAPA File Review Results</b></p>
                        <Field name="capaResults" validate={validators.required}>
                          {({ input, meta }) => (
                            <>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                maxLength={255}
                                large
                                fill
                                disabled
                              />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                    <td>
                      <label className="width-max-content">
                        <p><b>DHR Review Results</b></p>
                        <Field
                          name="dhrRevResults"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                maxLength={255}
                                large
                                fill
                                disabled
                              />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                    <td>
                      <label className="width-max-content">
                        <p><b>Risk File Review Results</b></p>
                        <Field
                          name="riskResults"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                maxLength={255}
                                large
                                fill
                                disabled
                              />
                              {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                            </>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td colSpan={3}>
                      <label>
                        <p><b>Investigation Summary</b></p>
                        <Field
                          name="investigationSummary"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill text-area-inv"
                                growVertically
                                large
                                fill
                                disabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                  <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                  </tr>
                  <tr>
                    <td colSpan={3}>
                      <label>
                        <p><b>Investigation Narrative</b></p>
                        <Field
                          name="investigationNarrative"
                          validate={validators.required}
                        >
                          {({ input, meta }) => (
                            <div>
                              <TextArea
                                {...input}
                                className="bp3-input bp3-fill"
                                growVertically
                                large
                                fill
                                disabled
                              />
                              {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                            </div>
                          )}
                        </Field>
                      </label>
                    </td>
                  </tr>
                </tbody>
              </table>
              <br />
              <br />
              <div className="row-wrapper-20">
                <button
                  type="button"
                  className="bp3-button .modifier"
                  disabled
                  onClick={() => setShowAlert(true)}
                >
                  Cancel
                </button>
                <button
                  type="button"
                  className="bp3-button .modifier"
                  disabled
                  onClick={() => handleSaveInvestigation(values)}
                >
                  Save
                </button>
                <button
                  type="submit"
                  className="bp3-button .modifier"
                  disabled
                >
                  Close Investigation
                </button>
              </div>
            </form>
          </>
        )}
      />
    </>
  );

  return (
    <>
      <h1>Investigation</h1>
      <div className="info-card info-container">
        <Button
          minimal
          className="info-button"
          onClick={() => setCollapse(!collapse)}
        >
          {collapse ? 'Hide Information ▲' : 'Show Information ▼'}
        </Button>
        <Collapse isOpen={collapse}>
          <div className="row-wrapper-10">
            <label>
              <b>As-Reported Code:</b>
              <div>
                { AsReportedCode }
              </div>
            </label>
          </div>
          <div>
            <b>Issue Description:</b>
          </div>
          <div>
            { IssueDescription }
          </div>
        </Collapse>
      </div>
      <div className="intake-wrapper">
        <div className="width-100p">
          <FormInvestigation />
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  investigationInfo: state.investigation.investigationInfo,
  invUpdated: state.investigation.invUpdated,
});

export default connect(mapStateToProps)(ViewInvestigation);
