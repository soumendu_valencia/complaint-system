/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const fetchAllUsers = createAsyncThunk(
  'testComplaints/fetchAllUsers',
  async () => {
    const response = await TestDataService.getAllUsers();
    return response.data;
  },
);

export const fetchInvestigation = createAsyncThunk(
  'testComplaints/fetchInvestigation',
  async (issueID) => {
    const response = await TestDataService.fetchInvestigation(issueID);
    return response.data.result;
  },
);

export const fetchAllInvestigations = createAsyncThunk(
  'testComplaints/fetchAllInvestigations',
  async () => {
    const response = await TestDataService.fetchAllInvestigations();
    return response.data.result;
  },
);

export const writeInvestigationToDb = createAsyncThunk(
  'testComplaints/writeInvestigationToDb',
  async (values) => {
    const response = await TestDataService.writeInvestigationToDb(values);
    return response.data;
  },
);

export const updateInvestigationToDb = createAsyncThunk(
  'api/updateInvestigationToDb',
  async (values) => {
    const response = await TestDataService.updateInvestigationToDb(values);
    return response.data;
  },
);

export const investigationSlice = createSlice({
  name: 'investigation',
  initialState: {
    investigationSubmitted: false,
    investigationInfo: {
      0: {
        IssueID: 'N/A',
        InvestigationStatus: 'N/A',
        CAPAFileReviewCompleted: false,
        AssociatedCAPA: 'N/A',
        ComplaintConfirmed: false,
        RootCauseIdentified: false,
        RootCause: 'N/A',
        DHRReviewCompleted: false,
        RiskFileReviewCompleted: false,
        DHRReviewConclusion: 'N/A',
        InvestigationSummary: 'N/A',
        InvestigationNarrative: 'N/A',
        RootCauseExplanation: 'N/A',
      },
    },
    investigations: {
      0: {
        IssueID: 'N/A',
        InvestigationStatus: 'N/A',
        CAPAFileReviewCompleted: 'N/A',
        AssociatedCAPA: 'N/A',
        ComplaintConfirmed: 'N/A',
        RootCauseIdentified: 'N/A',
        RootCause: 'N/A',
        DHRReviewCompleted: 'N/A',
        RiskFileReviewCompleted: 'N/A',
        DHRReviewConclusion: 'N/A',
        InvestigationSummary: 'N/A',
        InvestigationNarrative: 'N/A',
        RootCauseExplanation: 'N/A',
      },
    },
    formData: {},
    users: {},
    invUpdated: null,
  },
  reducers: {
    updateInfo: (state, action) => {
      state.formData = action.payload;
    },
    resetInvUpdated: (state) => {
      state.invUpdated = null;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(fetchAllUsers.fulfilled, (state, action) => {
      state.users = Object.values(action.payload.result);
      window.sessionStorage.setItem('users', JSON.stringify(action.payload.result));
    });
    builder.addCase(writeInvestigationToDb.fulfilled, (state, action) => {
      state.investigationSubmitted = action.payload.result;
    });
    builder.addCase(fetchInvestigation.fulfilled, (state, action) => {
      // window.sessionStorage.setItem('investigationInfo', JSON.stringify(action.payload));
      if (action.payload.length === 0) {
        state.investigationInfo = {
          0: {
            IssueID: 'N/A',
            InvestigationStatus: 'N/A',
            CAPAFileReviewCompleted: false,
            AssociatedCAPA: 'N/A',
            ComplaintConfirmed: false,
            RootCauseIdentified: false,
            RootCause: 'N/A',
            DHRReviewCompleted: 'N/A',
            RiskFileReviewCompleted: false,
            DHRReviewConclusion: 'N/A',
            InvestigationSummary: 'N/A',
            InvestigationNarrative: 'N/A',
            RootCauseExplanation: 'N/A',
          },
        };
      } else {
        state.investigationInfo = action.payload;
      }
      window.sessionStorage.setItem('investigationInfo', JSON.stringify(action.payload));
    });
    builder.addCase(fetchAllInvestigations.fulfilled, (state, action) => {
      window.sessionStorage.setItem('allInvestigationsInfo', JSON.stringify(action.payload));
      state.investigations = action.payload;
    });
    builder.addCase(updateInvestigationToDb.fulfilled, (state, action) => {
      state.invUpdated = action.payload.toString();
    });
  },
});

export const selectAssignInfo = (state) => state.investigation.formData;

export const { updateInfo, resetInvUpdated } = investigationSlice.actions;

export default investigationSlice.reducer;
