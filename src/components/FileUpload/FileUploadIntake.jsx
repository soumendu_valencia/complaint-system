/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Buffer } from 'buffer';
import { connect, useDispatch } from 'react-redux';
import { FileInput, Spinner } from '@blueprintjs/core';
import { parseChanges } from '../../actions/actionsIndex';
import {
  writeChange,
  writeFile,
  getFile,
  resetFilesSuccessful,
} from '../../app/changesSlice';

import '../../App.css';

function FileUploadIntake(props) {
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  // Delete?
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [fileUploading, setFileUploading] = useState(false);
  // Possibly store as object?
  const [fileList, setFileList] = useState([]);
  const dispatch = useDispatch();

  const {
    issueID,
    phase,
    uploadFilesSuccessful,
    writeFilesSuccessful,
    disable,
  } = props;

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`;

  const renderUploadedFiles = (files) => {
    <>
      {Object.keys(files).map((fileName) => (
        <div key={fileName}>{fileName}</div>
      ))}
    </>;
  };

  useEffect(async () => {
    if (issueID) {
      const parsedArr = [];
      const restoredFileList = JSON.parse(window.sessionStorage.getItem('storedFiles'));
      if (restoredFileList && restoredFileList.length > 0) {
        fileList.forEach(async (file) => {
          const u8 = await file.arrayBuffer();

          parsedArr.push({
            issueID,
            fileBuffer: Buffer.from(u8).toString('base64'),
            fileName: file.name,
            fileType: file.type,
            dateUploaded: new Date().toDateString(),
            personUpload: userName,
            phase,
          });
        });

        parsedArr.forEach(async (file) => {
          dispatch(writeFile(file));
        });
      }
    }
  }, [issueID]);

  useEffect(() => {
    renderUploadedFiles(fileList);
  }, [fileList]);

  useEffect(() => {
    if (uploadFilesSuccessful && writeFilesSuccessful) {
      setFileUploading(false);
      dispatch(resetFilesSuccessful());
      // TODO: write this
    }
  }, [uploadFilesSuccessful, writeFilesSuccessful]);

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  const onUpload = async (file) => {
    setFileList(
      {
        ...fileList, [file.name]: file,
      },
    );
    console.table(fileList);
  };

  window.sessionStorage.setItem('fileList', JSON.stringify(fileList));

  const renderDataTable = (filesList) => {
    let result = `
    <table border=0 style=none>
      <tbody>
        <tr>
          <th>Name</th>
        </tr>`;
    result += filesList.map((file) => {
      const picked = (({
        FileName,
      }) => ({
        FileName,
      }))(file);
      return (
        `<tr>
          <td>${picked.FileName}</td>
        </tr>`
      );
    });
    result += '</tbody></table>';
    return result.replaceAll(',', '');
  };

  let table;
  useEffect(() => {
    if (fileList && fileList.length > 1) {
      table = renderDataTable(fileList);
    }
  }, [fileList]);

  return (
    <>
      <b>Upload Files:</b>
      <br />
      <br />
      <FileInput
        className="bp3-button .modifier"
        multiple
        text="Choose file..."
        onInputChange={changeHandler}
        disabled={disable}
      />
      {isFilePicked ? (
        <>
          {fileUploading ? (
            <Spinner className="bp3-intent-primary center" size={40} />
          ) : (
            <div>
              <h4>File Information:</h4>
              <p>
                <b>File Name:</b>
                {' '}
                {selectedFile.name}
              </p>
              <p>
                <b>Size:</b>
                {' '}
                {`${(selectedFile.size * 0.000001).toFixed(2)} MB`}
              </p>
              <p>
                <b>Last Modified:</b>
                {' '}
                {selectedFile.lastModifiedDate.toLocaleDateString()}
              </p>
              <br />
            </div>
          )}
        </>
      ) : (
        <p>Select a file to show details</p>
      )}
      <button
        type="button"
        className="bp3-button .modifier"
        disabled={(selectedFile && selectedFile.size > 400000) || disable}
        onClick={() => onUpload(selectedFile)}
      >
        Upload File
      </button>
      {selectedFile && selectedFile.size > 400000 ? (
        <span className="warning-text">File size must be less than 4 MB.</span>
      ) : (
        <></>
      )}
      <div className="padding-top-10 width-max-content text-align-left">
        {/* TODO: FIX */}
        <h3>Uploaded Files:</h3>
        {/* eslint-disable-next-line react/no-danger */}
        <div dangerouslySetInnerHTML={{ __html: table }} />
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  uploadFilesSuccessful: state.changes.uploadFilesSuccessful,
  writeFilesSuccessful: state.changes.writeFilesSuccessful,
});

export default connect(mapStateToProps)(FileUploadIntake);
