/* eslint-disable no-param-reassign */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { Buffer } from 'buffer';
import { connect, useDispatch } from 'react-redux';
import { FileInput, Spinner } from '@blueprintjs/core';
import { parseChanges } from '../../actions/actionsIndex';
import {
  writeChange,
  writeFile,
  getFile,
  resetFilesSuccessful,
} from '../../app/changesSlice';

import '../../App.css';

function FileUpload(props) {
  const [selectedFile, setSelectedFile] = useState();
  const [isFilePicked, setIsFilePicked] = useState(false);
  const [uploadedFiles, setUploadedFiles] = useState([]);
  const [fileUploading, setFileUploading] = useState(false);
  const [asscFiles, setAsscFiles] = useState(JSON.parse(window.sessionStorage.getItem('complaintFiles')));
  const dispatch = useDispatch();

  const {
    issueID,
    phase,
    uploadFilesSuccessful,
    writeFilesSuccessful,
    disable,
  } = props;

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const userName = `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`;

  const renderUploadedFiles = (files) => {
    <>
      {Object.keys(files).map((fileName) => (
        <div key={fileName}>{fileName}</div>
      ))}
    </>;
  };

  useEffect(() => {
    if (asscFiles && asscFiles.length > 0) {
      setAsscFiles(asscFiles.filter((file) => file && file.AssociatedPhase === phase));
    }
  }, []);
  useEffect(() => {
    renderUploadedFiles(uploadedFiles);
  }, [uploadedFiles]);

  useEffect(() => {
    if (uploadFilesSuccessful && writeFilesSuccessful) {
      setFileUploading(false);
      dispatch(resetFilesSuccessful());
      // TODO: write this
    }
  }, [uploadFilesSuccessful, writeFilesSuccessful]);

  const changeHandler = (event) => {
    setSelectedFile(event.target.files[0]);
    setIsFilePicked(true);
  };

  const onUpload = async (file) => {
    const u8 = await file.arrayBuffer();
    const fileBuffer = Buffer.from(u8).toString('base64');

    setUploadedFiles(
      {
        ...uploadedFiles, [file.name]: file,
      },
    );
    setFileUploading(true);
    const fileData = {
      issueID,
      fileBuffer,
      fileName: file.name,
      fileType: file.type,
      dateUploaded: new Date().toDateString(),
      personUpload: userName,
      phase,
    };
    dispatch(writeFile(fileData));
    // dispatch(getFile({ issueID, fileName: 'PDFTest2.pdf' }));
  };

  const uploadedFilesArray = Object.values(uploadedFiles).map((obj) => obj);
  window.sessionStorage.setItem('uploadedFiles', JSON.stringify(uploadedFilesArray));

  const renderDataTable = (filesList) => {
    let result = `
    <table border=0 style=none>
      <tbody>
        <tr>
          <th>Name</th>
        </tr>`;
    result += filesList.map((file) => {
      const picked = (({
        FileName,
      }) => ({
        FileName,
      }))(file);
      return (
        `<tr>
          <td>${picked.FileName}</td>
        </tr>`
      );
    });
    result += '</tbody></table>';
    return result.replaceAll(',', '');
  };

  let table;
  useEffect(() => {
    if (asscFiles) {
      table = renderDataTable(asscFiles);
    }
  }, [asscFiles]);

  return (
    <>
      <b>Upload Files:</b>
      <br />
      <br />
      <FileInput
        className="bp3-button .modifier"
        multiple
        text="Choose file..."
        onInputChange={changeHandler}
        // disabled={disable}
      />
      {isFilePicked ? (
        <>
          {fileUploading ? (
            <Spinner className="bp3-intent-primary center" size={40} />
          ) : (
            <div>
              <h4>File Information:</h4>
              <p>
                <b>File Name:</b>
                {' '}
                {selectedFile.name}
              </p>
              <p>
                <b>Size:</b>
                {' '}
                {`${(selectedFile.size * 0.000001).toFixed(2)} MB`}
              </p>
              <p>
                <b>Last Modified:</b>
                {' '}
                {selectedFile.lastModifiedDate.toLocaleDateString()}
              </p>
              <br />
            </div>
          )}
        </>
      ) : (
        <p>Select a file to show details</p>
      )}
      <button
        type="button"
        className="bp3-button .modifier"
        // disabled={(selectedFile && selectedFile.size > 400000) || disable}
        onClick={() => onUpload(selectedFile)}
      >
        Upload File
      </button>
      {selectedFile && selectedFile.size > 400000 ? (
        <span className="warning-text">File size must be less than 4 MB.</span>
      ) : (
        <></>
      )}
      <div className="padding-top-10 width-max-content text-align-left">
        {/* TODO: FIX */}
        <h3>Uploaded Files:</h3>
        {/* eslint-disable-next-line react/no-danger */}
        <div dangerouslySetInnerHTML={{ __html: table }} />
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  uploadFilesSuccessful: state.changes.uploadFilesSuccessful,
  writeFilesSuccessful: state.changes.writeFilesSuccessful,
});

export default connect(mapStateToProps)(FileUpload);
