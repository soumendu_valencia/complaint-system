/* eslint-disable react/prop-types */
/* eslint-disable array-callback-return */
/* eslint-disable max-len */
/* eslint-disable consistent-return */
import React, { useEffect, useState } from 'react';
import { useDispatch, connect } from 'react-redux';
import CustomTable from '../CustomTable/CustomTable';
import { fetchAllMDRInfo } from '../ViewComplaint/ViewComplaintSlice';
// import { fetchAllInvestigations } from '../InvestigationsPanel/investigationSlice';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './UserDashboard.css';
import '../../App.css';

function UserDashboard(props) {
  const dispatch = useDispatch();
  const [showTables, setShowTables] = useState('bp3-skeleton');

  const {
    // userComplaints,
    // allMDRInfo,
    // allComplaints,
    intakeComplaints,
    firstRevComplaints,
    investigationComplaints,
    mdrComplaints,
    finalRevComplaints,
    complaintsFetched,
  } = props;

  // let storedComplaints = {};
  let storedIntakeComplaints = {};
  let storedFirstRevComplaints = {};
  let storedInvestigationComplaints = {};
  let storedMDRComplaints = {};
  let storedFinalRevComplaints = {};
  // let storedUserComplaints = {};
  // let storedAllMDRInfo = {};

  /* Fetch all complaints and all MDRs on page load. */
  useEffect(() => {
    dispatch(fetchAllMDRInfo());
  }, []);

  /* Display tables once all complaints are retrieved. */
  useEffect(() => {
    if (complaintsFetched) {
      setShowTables('');
      // const createdByList = allComplaints.map((iss) => iss.PersonIntake);
      // setUCBList(Array.from(new Set(createdByList)));
      // window.sessionStorage.setItem('ucbList', JSON.stringify(ucbList));
    }
  }, [complaintsFetched]);

  window.sessionStorage.setItem('intakeComplaints', JSON.stringify(intakeComplaints));
  window.sessionStorage.setItem('firstRevComplaints', JSON.stringify(firstRevComplaints));
  window.sessionStorage.setItem('investigationComplaints', JSON.stringify(investigationComplaints));
  window.sessionStorage.setItem('mdrComplaints', JSON.stringify(mdrComplaints));
  window.sessionStorage.setItem('finalRevComplaints', JSON.stringify(finalRevComplaints));

  storedIntakeComplaints = JSON.parse(window.sessionStorage.getItem('intakeComplaints'));
  storedFirstRevComplaints = JSON.parse(window.sessionStorage.getItem('firstRevComplaints'));
  storedInvestigationComplaints = JSON.parse(window.sessionStorage.getItem('investigationComplaints'));
  storedMDRComplaints = JSON.parse(window.sessionStorage.getItem('mdrComplaints'));
  storedFinalRevComplaints = JSON.parse(window.sessionStorage.getItem('finalRevComplaints'));

  return (
    <>
      <div className="dashboard-wrapper">
        <div className="user-dashboard-wrapper">
          <div>
            <div className="complaints-table-wrapper">
              <div className="complaints-table-wrapper">
                <h2 className={showTables}>Issue Intake:</h2>
                <div className={showTables}>
                  <CustomTable data={storedIntakeComplaints} customWidth={0.8} />
                </div>
              </div>
              <div>
                <div className="complaints-table-wrapper">
                  <h2 className={showTables}>First Review:</h2>
                  <div className={showTables}>
                    <CustomTable data={storedFirstRevComplaints} customWidth={0.8} />
                  </div>
                </div>
              </div>
              <div>
                <div className="complaints-table-wrapper">
                  <h2 className={showTables}>Investigation:</h2>
                  <div className={showTables}>
                    <CustomTable data={storedInvestigationComplaints} customWidth={0.8} />
                  </div>
                </div>
              </div>
              <div>
                <div className="complaints-table-wrapper">
                  <h2 className={showTables}>Open MDRs:</h2>
                  <div className={showTables}>
                    <CustomTable data={storedMDRComplaints} customWidth={0.8} />
                  </div>
                </div>
              </div>
              <div>
                <div className="complaints-table-wrapper">
                  <h2 className={showTables}>Final Review:</h2>
                  <div className={showTables}>
                    <CustomTable data={storedFinalRevComplaints} customWidth={0.8} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  userInfo: state.login.userInfo,
  // complaints: state.existingComplaints.Complaints,
  allMDRInfo: state.viewComplaint.allMDRInfo,
  userComplaints: state.existingComplaints.userComplaints,
  allComplaints: state.existingComplaints.complaints,
  intakeComplaints: state.existingComplaints.intakeComplaints,
  firstRevComplaints: state.existingComplaints.firstRevComplaints,
  investigationComplaints: state.existingComplaints.investigationComplaints,
  mdrComplaints: state.existingComplaints.mdrComplaints,
  finalRevComplaints: state.existingComplaints.finalRevComplaints,
  closedComplaints: state.existingComplaints.investigationComplaints,
  investigations: state.investigation.investgiations,
  complaintsFetched: state.existingComplaints.complaintsFetched,
});

export default connect(mapStateToProps)(UserDashboard);
