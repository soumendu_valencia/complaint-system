/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useEffect, useState } from 'react';
import { Button, Toaster, Intent } from '@blueprintjs/core';
import { useDispatch, connect } from 'react-redux';
import CreateAccount from '../CreateAccount/CreateAccount';
import { resetCASuccess } from '../CreateAccount/CreateAccountSlice';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './Settings.css';
import '../../App.css';

const createAccountToaster = Toaster.create({});

function Settings(props) {
  const { createAccountSuccess } = props;
  const [showCreateAccount, setShowCreateAccount] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    if (createAccountSuccess === 'true') {
      createAccountToaster.show({
        intent: Intent.SUCCESS,
        icon: 'tick',
        message: 'Account created!',
      });
      setShowCreateAccount(false);
    } else if (createAccountSuccess === 'false') {
      createAccountToaster.show({
        intent: Intent.DANGER,
        icon: 'cross',
        message: 'Error: Failed to create account. Please try again.',
      });
    }
    dispatch(resetCASuccess());
  }, [createAccountSuccess]);

  const getRole = (roleNum) => {
    switch (roleNum) {
      case 0:
        return 'Administrator';
      case 1:
        return 'Investigtor';
      case 2:
        return 'Intake User';
      case 3:
        return 'Viewer';
      case 4:
        return 'Compliance User';
      default:
        return roleNum;
    }
  };

  const cancelFunc = () => {
    setShowCreateAccount(false);
  };

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));
  const role = getRole(storedUserInfo.Role);

  const SettingsForm = () => (
    <>
      <div className="settings-wrapper">
        <div>
          {storedUserInfo.Role === 0 ? (
            <>
              <h2>Create new account:</h2>
              {showCreateAccount ? (
                <div>
                  <CreateAccount cancelFunc={cancelFunc} />
                </div>
              ) : (
                <>
                  <div className="button-wrapper">
                    <Button
                      type="button"
                      intent="primary"
                      large="true"
                      className="bp3-button .modifier button-1"
                      onClick={() => setShowCreateAccount(true)}
                    >
                      Create New Account
                    </Button>
                  </div>
                </>
              )}
            </>
          ) : (
            <>
            </>
          )}
        </div>
      </div>
    </>
  );

  return (
    <>
      <h2>Current Role:</h2>
      <div className="button-wrapper">
        <h3>{role}</h3>
      </div>
      <SettingsForm />
    </>
  );
}

const mapStateToProps = (state) => ({
  createAccountSuccess: state.createAccount.createAccountSuccess,
});

export default connect(mapStateToProps)(Settings);
