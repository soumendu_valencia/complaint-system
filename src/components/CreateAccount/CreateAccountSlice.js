/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const createAccount = createAsyncThunk(
  'api/createAccount',
  async (accountInfo) => {
    const response = await TestDataService.createAccount(accountInfo);
    return response.data;
  },
);

export const updateAccount = createAsyncThunk(
  'api/updateAccount',
  async (accountInfo) => {
    const response = await TestDataService.updateAccount(accountInfo);
    return response.data;
  },
);

export const createAccountSlice = createSlice({
  name: 'createAccount',
  initialState: {
    createAccountSuccess: null,
    updateAccountSuccess: null,
  },
  reducers: {
    resetCASuccess: (state) => {
      state.createAccountSuccess = null;
    },
    resetUASuccess: (state) => {
      state.updateAccountSuccess = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(createAccount.fulfilled, (state, action) => {
      state.createAccountSuccess = action.payload.result.toString();
    });
    builder.addCase(updateAccount.fulfilled, (state, action) => {
      state.updateAccountSuccess = action.payload.toString();
    });
  },
});

export const selectCreateAccountData = (state) => state.createAccount.accountCredentials;

export const { updateInfo, resetCASuccess, resetUASuccess } = createAccountSlice.actions;

export default createAccountSlice.reducer;
