/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
import React, { useState } from 'react';
import { useDispatch, connect } from 'react-redux';
// import { useHistory } from 'react-router-dom';
import { Form, Field, FormSpy } from 'react-final-form';
import { Callout, HTMLSelect } from '@blueprintjs/core';
// eslint-disable-next-line no-unused-vars
import { createAccount } from './CreateAccountSlice';
import * as validators from '../Validation';
// import CreateAccountToRedux from './CreateAccountToRedux';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './CreateAccount.css';
import '../../App.css';

// eslint-disable-next-line no-unused-vars
function CreateAccount(props) {
  const [usrWrn, setUsrWrn] = useState(false);
  const dispatch = useDispatch();

  const { cancelFunc } = props;

  let formVals = {
    firstName: '',
    lastName: '',
    username: '',
    email: '',
    role: '',
  };
  // const history = useHistory();
  // const { createAccountSuccess } = props;

  // useEffect(() => {
  //   if (createAccountSuccess) {
  //     history.push('/');
  //   }
  // });

  const roleList = [
    { label: '', value: '' },
    { label: 'Administrator', value: 0 },
    { label: 'Investigator', value: 1 },
    { label: 'Intaker', value: 2 },
    { label: 'Viewer', value: 3 },
    { label: 'Compliance User', value: 4 },
  ];

  const handleCreateAccountSubmit = async (accountInfo) => {
    const accountCreationDate = new Date().toLocaleString();
    accountInfo.dateAccountCreated = accountCreationDate.toLocaleString('en-US', { timeZone: 'America/Los_Angeles' });
    // eslint-disable-next-line no-param-reassign
    // accountInfo.dateAccountCreated = accountInfo.dateAccountCreated.toString();
    const userList = JSON.parse(window.sessionStorage.getItem('users'));
    const matchingUser = userList.filter((user) => user.Username === accountInfo.username);
    if (matchingUser.length === 0) {
      setUsrWrn(false);
      dispatch(createAccount(accountInfo));
      formVals = {};
    } else {
      formVals = accountInfo;
      // window.sessionStorage.setItem('cavls', JSON.stringify(accountInfo));
      setUsrWrn(true);
    }
  };

  const password = (value) => (value && /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/i.test(value)
    ? undefined
    : (
      <Callout
        className="width-fit-content"
        intent="warning"
        icon="warning-sign"
      >
        Password must have at least: 8 characters, one uppercase letter, one lowercase letter, one number, one special character
      </Callout>
    )
  );

  const CreateAccountForm = ({ subscription }) => (
    <Form
      onSubmit={handleCreateAccountSubmit}
      subscription={subscription}
      initialValues={formVals}
      render={({
        // eslint-disable-next-line no-unused-vars
        handleSubmit, form, values, submitting,
      }) => (
        <>
          <FormSpy subscription={{ values: true }}>
            {({ vals }) => (
              <pre>
                {JSON.stringify(vals, 0, 2)}
              </pre>
            )}
          </FormSpy>

          <form onSubmit={handleSubmit} autoComplete="off">
            {/* <CreateAccountToRedux form="createAccountForm" /> */}
            <div className="input-fields">
              <div className="input-label">
                <label>
                  <b>Full Name:</b>
                </label>
              </div>
              <div className="row-wrapper-5">
                <Field
                  name="firstName"
                  className="login-input"
                  validate={validators.required}
                >
                  {({ input, meta }) => (
                    <>
                      <input {...input} className="bp3-input" placeholder="First Name" />
                      <div>
                        {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                      </div>
                    </>
                  )}
                </Field>
                <Field
                  name="lastName"
                  className="login-input"
                  validate={validators.required}
                >
                  {({ input, meta }) => (
                    <>
                      <input {...input} className="bp3-input" placeholder="Last Name" />
                      <div>
                        {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                      </div>
                    </>
                  )}
                </Field>
              </div>
              <br />
              <div>
                <div className="input-label">
                  <label>
                    <b>Username:</b>
                  </label>
                </div>
                <Field
                  name="username"
                  className="login-input"
                  validate={validators.composeValidators(validators.required, validators.alphaNumeric)}
                >
                  {({ input, meta }) => (
                    <>
                      <input {...input} className="bp3-input" placeholder="Username" />
                      <div>
                        {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                      </div>
                    </>
                  )}
                </Field>
              </div>
              {usrWrn ? (
                <>
                  <br />
                  <Callout
                    className="width-max-content"
                    intent="danger"
                    icon="warning-sign"
                  >
                    This username is already taken.
                  </Callout>
                </>
              ) : (
                <></>
              )}
              <br />
              <div className="row-wrapper-5">
                <div className="input-label">
                  <label>
                    <b>Password:</b>
                    <Field
                      name="password"
                      className="login-input"
                      validate={validators.required}
                    >
                      {({ input, meta }) => (
                        <>
                          <input {...input} type="password" className="bp3-input" placeholder="Password" autoComplete="off" />
                          <div>
                            {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                          </div>
                        </>
                      )}
                    </Field>
                  </label>
                </div>
                <div className="input-label">
                  <label>
                    <b>Confirm Password:</b>
                    <Field
                      name="passwordConfirm"
                      className="login-input"
                      validate={validators.required}
                    >
                      {({ input, meta }) => (
                        <>
                          <input
                            {...input}
                            type="password"
                            className="bp3-input"
                            placeholder="Confirm Password"
                            autoComplete="off"
                            // onKeyUp={() => checkPwdMatch(values.password, values.passwordConfirm)}
                          />
                          <div>
                            {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                          </div>
                        </>
                      )}
                    </Field>
                  </label>
                </div>
              </div>
              <span className="warning-text">
                {values.password === values.passwordConfirm ? (
                  <></>
                ) : (
                  <span className="warning-text">Passwords must match</span>
                )}
              </span>
              <span>
                {password(values.password)}
              </span>
              <div className="input-label">
                <label>
                  <b>Email:</b>
                </label>
              </div>
              <div className="input-wrapper">
                <Field
                  name="email"
                  className="login-input"
                  validate={validators.composeValidators(validators.required, validators.customDomainEmail)}
                >
                  {({ input, meta }) => (
                    <>
                      <input {...input} className="bp3-input" placeholder="Email" />
                      <div>
                        {meta.touched && meta.error && <span className="warning-text">{meta.error}</span>}
                      </div>
                    </>
                  )}
                </Field>
              </div>
              <br />
              <div className="input-label">
                <label>
                  <b>Role:</b>
                </label>
              </div>
              <div className="input-wrapper role-wrapper">
                <Field
                  name="role"
                  className="width-100p"
                  validate={validators.required}
                >
                  {({ input, meta }) => (
                    <div>
                      <HTMLSelect
                        {...input}
                        options={roleList}
                        onChange={input.onChange}
                      />
                      <div>
                        {meta.error && meta.touched && <span className="warning-text">{meta.error}</span>}
                      </div>
                    </div>
                  )}
                </Field>
              </div>
              <br />
              <br />
              <div className="row-wrapper-5">
                <button
                  type="button"
                  intent="danger"
                  disabled={submitting}
                  className="bp3-button bp3-intent-danger"
                  onClick={() => cancelFunc()}
                >
                  Cancel
                </button>
                <button
                  type="submit"
                  intent="primary"
                  disabled={submitting || values.password !== values.passwordConfirm}
                  className="bp3-button bp3-intent-primary submit-button"
                  // onClick={() => handleCreateAccountSubmit(values)}
                >
                  Submit
                </button>
              </div>
            </div>
          </form>
        </>
      )}
    />
  );

  return (
    <>
      <CreateAccountForm />
    </>
  );
}
const mapStateToProps = (state) => ({
  createAccountSuccess: state.createAccount.createAccountSuccess,
});

export default connect(mapStateToProps)(CreateAccount);
