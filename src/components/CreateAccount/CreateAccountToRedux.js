/* eslint-disable react/jsx-filename-extension */
/* eslint-disable react/prop-types */
import React from 'react';
import { connect } from 'react-redux';
import { FormSpy } from 'react-final-form';
import { updateInfo } from './CreateAccountSlice';

const CreateAccountToRedux = ({ form }) => (
  <FormSpy onChange={(state) => updateInfo(form, state)} />
);

export default connect(undefined, { updateInfo })(CreateAccountToRedux);
