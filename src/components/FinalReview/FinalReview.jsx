/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import {
  Alert, Intent, Collapse, Button, Toaster,
} from '@blueprintjs/core';
import 'js-loading-overlay';
import { changePhase, writeChange, resetPhaseChangeSuccessful } from '../../app/changesSlice';
import { parseChanges } from '../../actions/actionsIndex';

import './FinalReview.css';

const loadingConfigs = {
  overlayBackgroundColor: '#FFFDFD',
  overlayOpacity: 0.6,
  spinnerIcon: 'ball-spin-clockwise',
  spinnerColor: '#4BA3F3',
  spinnerSize: '3x',
  overlayIDName: 'overlay',
  spinnerIDName: 'spinner',
  offsetX: 0,
  offsetY: 0,
  containerID: null,
  lockScroll: false,
  overlayZIndex: 9998,
  spinnerZIndex: 9999,
};

function FinalReview(props) {
  const [collapse, setCollapse] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [showPhaseAlert, setShowPhaseAlert] = useState(false);
  const [ovrlyInfo, setOvrlyInfo] = useState('');
  const [phase, setPhase] = useState('');
  const [showCloseOvrly, setShowCloseOvrly] = useState(false);
  // const [phaseInfo, setPhaseInfo] = useState({});
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    complaintInfo,
    mdrInfo,
    investigationInfo,
    phaseChangeSuccessful,
  } = props;

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));

  const {
    CustomerID,
    PatientID,
    ComplaintID,
    IssueDescription,
    StatusOpenClosed,
    DateEvent,
    DateAware,
    DateReported,
    ImmediateSafetyIssue,
    IssueIsComplaint,
    PersonIntake,
    IssueNotComplaintRationale,
    AsReportedCode,
    MDRConclusion1,
    MDRIsRequired,
    InvestigationConclusion,
    ProdReturnRequested,
    CustFollowUpNeeded,
    Phase,
    RMANumber,
    DateClosed,
  } = complaintInfo[0];

  const {
    Id,
    IssueID,
    Type,
    DueDate,
    MDRNumber,
    Classification,
    Status,
    ReviewDate,
    DateSubmitted,
    SupplementalNeeded,
    DateSuppSubmitted,
    MDRSubmitted,
    SuppMDRSubmitted,
  } = mdrInfo[0];

  const {
    InvestigationStatus,
    CAPAFileReviewResults,
    AssociatedCAPA,
    ComplaintConfirmed,
    RootCauseIdentified,
    RootCause,
    DHRReviewCompleted,
    RiskFileReviewCompleted,
    DHRReviewConclusion,
    InvestigationSummary,
    InvestigationNarrative,
    RootCauseExplanation,
  } = investigationInfo[0];

  useEffect(() => {
    if (phaseChangeSuccessful) {
      dispatch(resetPhaseChangeSuccessful());
      history.go(0);
    }
  }, [phaseChangeSuccessful]);

  const handlePhaseChange = (phaseInfo) => {
    // eslint-disable-next-line no-undef
    // const { issueId, phase } = phaseInfo;
    phaseInfo.status = 'OPEN';
    phaseInfo.dateClosed = null;
    if (phaseInfo.phase === 'Closed') {
      phaseInfo.status = 'CLOSED';
      phaseInfo.dateClosed = new Date();
    }
    const values = {
      phase: phaseInfo.phase,
      status: phaseInfo.status,
    };

    const complaintFields = {
      issueID: ComplaintID,
      phase: Phase,
      status: StatusOpenClosed,
      dateClosed: DateClosed,
    };

    const changes = parseChanges(complaintFields, values, storedUserInfo);
    if (changes.valuesDiff.length > 0) {
      dispatch(writeChange(changes));
    }
    dispatch(changePhase(phaseInfo));
    let parsedPhase = phaseInfo.phase;
    parsedPhase = parsedPhase.replace(' ', '%20');
    history.push(`/editComplaint/${ComplaintID}/${parsedPhase}`);
  };

  const composeOvrly = (errs) => {
    let ovrlyMsg = '<div>';
    errs.forEach((err, ind) => {
      ovrlyMsg += `<div><b>${ind + 1}. ${err}</b>\n\n</div>`;
    });
    ovrlyMsg += '</div>';
    return ovrlyMsg;
  };

  const handleCloseComplaint = () => {
    const errList = [];

    if (MDRIsRequired) {
      if (!MDRSubmitted) { errList.push('MDR must be submitted before closing complaint.'); }
      if (!DateSubmitted) { errList.push('MDR Submission Date cannot be empty.'); }
      if (SupplementalNeeded) {
        if (!SuppMDRSubmitted) {
          errList.push('Supplemental MDR must be submitted before closing complaint.');
        }
        if (!DateSuppSubmitted) {
          errList.push('Supplemental MDR Submission Date cannot be empty.');
        }
      }
    }

    if (CustFollowUpNeeded) {
      errList.push("'Is Customer Follow-up Needed?' cannot have a value of YES.");
    }

    if (errList.length > 0) {
      setOvrlyInfo(composeOvrly(errList));
      setShowCloseOvrly(true);
    } else {
      handlePhaseChange({ issueId: ComplaintID, phase: 'Closed' });
    }
  };

  const handleConfirm = () => {
    if (phase === 'Closed') {
      handleCloseComplaint();
    } else {
      handlePhaseChange({ issueId: ComplaintID, phase });
    }
    setShowAlert(false);
    setShowPhaseAlert(false);
  };

  const handleCancel = (status) => {
    if (status === 'closed') {
      setShowAlert(false);
    } else if (status === 'phase') {
      setShowPhaseAlert(false);
    }
  };

  return (
    <>
      <h1>Final Review</h1>
      <div>
        <div>
          <b>Issue Description</b>
          <div>
            <Button
              minimal
              intent="primary"
              className="info-button"
              onClick={() => setCollapse(!collapse)}
            >
              <b>
                {collapse ? '- Hide' : '+ Show'}
              </b>
            </Button>
            <Collapse isOpen={collapse}>
              <div className="pre-line">
                { IssueDescription }
              </div>
            </Collapse>
          </div>
          <br />
          <table className="font-med width-80p">
            <tbody>
              <tr>
                <td>
                  <label>
                    <b>
                      Is this an urgent safety issue?
                    </b>
                    <div>
                      {ImmediateSafetyIssue ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Is customer follow-up needed?
                    </b>
                    <div>
                      {CustFollowUpNeeded ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Is product being returned?
                    </b>
                    <div>
                      {ProdReturnRequested ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      RMA Number:
                    </b>
                    <div>
                      {RMANumber || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td rowSpan={8} className="text-top">
                  <label>
                    <b>
                      All files:
                    </b>
                    <div>
                      {}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <label>
                    <b>
                      As-Reported Code
                    </b>
                    <div>
                      {AsReportedCode}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      MDR is required:
                    </b>
                    <div>
                      {MDRIsRequired ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      MDR Number
                    </b>
                    <div>
                      {MDRNumber || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Date MDR Submitted:
                    </b>
                    <div>
                      {DateSubmitted || 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <label>
                    <b>
                      MDR Submitted:
                    </b>
                    <div>
                      {MDRSubmitted ? 'YES' : 'NO' || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Type of Report:
                    </b>
                    <div>
                      {Type ? '5 Day' : '30 Day' || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Type of Reportable Event:
                    </b>
                    <div>
                      {Classification}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <label>
                    <b>
                      CAPA File Review Results
                    </b>
                    <div>
                      {CAPAFileReviewResults || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      DHR Review Results:
                    </b>
                    <div>
                      {DHRReviewConclusion || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Risk File Review Results:
                    </b>
                    <div>
                      N/A
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Root Cause Code:
                    </b>
                    <div>
                      {RootCause || 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td colSpan="8">
                  <label>
                    <b>
                      Investigation Summary:
                    </b>
                    <div>
                      {InvestigationSummary || 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <button
                    type="button"
                    className="bp3-button .modifier"
                    onClick={() => {
                      setPhase('Intake');
                      setShowPhaseAlert(true);
                      // handlePhaseChange({ issueId: ComplaintID, phase: 'Intake' });
                    }}
                  >
                    Back to Issue Intake
                  </button>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <button
                    type="button"
                    className="bp3-button .modifier"
                    onClick={() => {
                      setPhase('First Review');
                      setShowPhaseAlert(true);
                      // handlePhaseChange({ issueId: ComplaintID, phase: 'First Review' });
                    }}
                  >
                    Back to First Review
                  </button>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <button
                    type="button"
                    className="bp3-button .modifier"
                    onClick={() => {
                      setPhase('Investigation');
                      setShowPhaseAlert(true);
                      // handlePhaseChange({ issueId: ComplaintID, phase: 'Investigation' });
                    }}
                  >
                    Back to Investigation
                  </button>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <button
                    type="button"
                    className="bp3-button .modifier"
                    onClick={() => {
                      setPhase('Closed');
                      setShowAlert(true);
                    }}
                  >
                    Close Complaint
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div>
        <Alert
          className="close-alert"
          confirmButtonText="Okay"
          onClose={() => setShowCloseOvrly(false)}
          icon="warning-sign"
          // intent={Intent.PRIMARY}
          isOpen={showCloseOvrly}
        >
          <>
            <h1 className="margin-top-0">Cannot close complaint:</h1>
            {/* eslint-disable-next-line react/no-danger */}
            <div className="pre-line" dangerouslySetInnerHTML={{ __html: ovrlyInfo }} />
          </>
          {/* {ovrlyInfo}
          </div> */}
        </Alert>
        <Alert
          cancelButtonText="Cancel"
          confirmButtonText="Close Complaint"
          icon="warning-sign"
          intent={Intent.DANGER}
          isOpen={showAlert}
          onCancel={() => handleCancel('closed')}
          onConfirm={() => handleConfirm('closed')}
        >
          <h2>Close Complaint</h2>
          <p>Are you sure you want to close this complaint?</p>
        </Alert>
        <Alert
          cancelButtonText="Cancel"
          confirmButtonText={`Send back to ${phase}`}
          icon="warning-sign"
          intent={Intent.DANGER}
          isOpen={showPhaseAlert}
          onCancel={() => handleCancel('phase')}
          onConfirm={() => handleConfirm('phase')}
        >
          <h2>
            Back to
            {' '}
            {phase}
          </h2>
          <p>
            Are you sure you want to send this complaint back to
            {' '}
            {phase}
            ?
          </p>
        </Alert>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  mdrInfo: state.viewComplaint.mdrInfo,
  investigationInfo: state.investigation.investigationInfo,
  phaseChangeSuccessful: state.changes.phaseChangeSuccessful,
});

export default connect(mapStateToProps)(FinalReview);
