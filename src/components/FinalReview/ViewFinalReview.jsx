/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { connect, useDispatch } from 'react-redux';
import {
  Alert, Intent, Collapse, Button, Toaster,
} from '@blueprintjs/core';
import 'js-loading-overlay';
import { changePhase, writeChange, resetPhaseChangeSuccessful } from '../../app/changesSlice';
import { parseChanges } from '../../actions/actionsIndex';

import './FinalReview.css';

const loadingConfigs = {
  overlayBackgroundColor: '#FFFDFD',
  overlayOpacity: 0.6,
  spinnerIcon: 'ball-spin-clockwise',
  spinnerColor: '#4BA3F3',
  spinnerSize: '3x',
  overlayIDName: 'overlay',
  spinnerIDName: 'spinner',
  offsetX: 0,
  offsetY: 0,
  containerID: null,
  lockScroll: false,
  overlayZIndex: 9998,
  spinnerZIndex: 9999,
};

function FinalReview(props) {
  const [collapse, setCollapse] = useState(true);
  const [showAlert, setShowAlert] = useState(false);
  const [showPhaseAlert, setShowPhaseAlert] = useState(false);
  const [ovrlyInfo, setOvrlyInfo] = useState('');
  const [phase, setPhase] = useState('');
  const [showCloseOvrly, setShowCloseOvrly] = useState(false);
  // const [phaseInfo, setPhaseInfo] = useState({});
  const dispatch = useDispatch();
  const history = useHistory();

  const {
    complaintInfo,
    mdrInfo,
    investigationInfo,
    phaseChangeSuccessful,
  } = props;

  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));

  const {
    CustomerID,
    PatientID,
    ComplaintID,
    IssueDescription,
    StatusOpenClosed,
    DateEvent,
    DateAware,
    DateReported,
    ImmediateSafetyIssue,
    IssueIsComplaint,
    PersonIntake,
    IssueNotComplaintRationale,
    AsReportedCode,
    MDRConclusion1,
    MDRIsRequired,
    InvestigationConclusion,
    ProdReturnRequested,
    CustFollowUpNeeded,
    Phase,
    RMANumber,
    DateClosed,
  } = complaintInfo[0];

  const {
    Id,
    IssueID,
    Type,
    DueDate,
    MDRNumber,
    Classification,
    Status,
    ReviewDate,
    DateSubmitted,
    SupplementalNeeded,
    DateSuppSubmitted,
    MDRSubmitted,
    SuppMDRSubmitted,
  } = mdrInfo[0];

  const {
    InvestigationStatus,
    CAPAFileReviewResults,
    AssociatedCAPA,
    ComplaintConfirmed,
    RootCauseIdentified,
    RootCause,
    DHRReviewCompleted,
    RiskFileReviewCompleted,
    DHRReviewConclusion,
    InvestigationSummary,
    InvestigationNarrative,
    RootCauseExplanation,
  } = investigationInfo[0];

  useEffect(() => {
    if (phaseChangeSuccessful) {
      dispatch(resetPhaseChangeSuccessful());
      history.push(`/editComplaint/${ComplaintID}/${'Intake'}`);
      history.go(0);
    }
  }, [phaseChangeSuccessful]);

  const handleCancel = () => {
    setShowAlert(false);
  };

  const handleConfirm = () => {
    dispatch(changePhase({ issueId: ComplaintID, phase: 'Intake', status: 'OPEN' }));
    const oldValues = 'status: CLOSED ||| ';
    const newValues = 'status: OPEN ||| ';

    const changes = {
      userID: storedUserInfo.UserID,
      userName: `${storedUserInfo.FirstName} ${storedUserInfo.LastName}`,
      timestamp: new Date().toLocaleString(),
      oldValues,
      newValues,
      complaintID: ComplaintID,
    };

    dispatch(writeChange(changes));
    setShowAlert(false);
  };

  return (
    <>
      <h1>Final Review</h1>
      <div>
        <div>
          <b>Issue Description</b>
          <div>
            <Button
              minimal
              intent="primary"
              className="info-button"
              onClick={() => setCollapse(!collapse)}
            >
              <b>
                {collapse ? '- Hide' : '+ Show'}
              </b>
            </Button>
            <Collapse isOpen={collapse}>
              <div className="pre-line">
                { IssueDescription }
              </div>
            </Collapse>
          </div>
          <br />
          <table className="font-med width-80p">
            <tbody>
              <tr>
                <td>
                  <label>
                    <b>
                      Is this an urgent safety issue?
                    </b>
                    <div>
                      {ImmediateSafetyIssue ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Is customer follow-up needed?
                    </b>
                    <div>
                      {CustFollowUpNeeded ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Is product being returned?
                    </b>
                    <div>
                      {ProdReturnRequested ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      RMA Number:
                    </b>
                    <div>
                      {RMANumber || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td rowSpan={8} className="text-top">
                  <label>
                    <b>
                      All files:
                    </b>
                    <div>
                      {}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <label>
                    <b>
                      As-Reported Code
                    </b>
                    <div>
                      {AsReportedCode}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      MDR is required:
                    </b>
                    <div>
                      {MDRIsRequired ? 'YES' : 'NO'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      MDR Number
                    </b>
                    <div>
                      {MDRNumber || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Date MDR Submitted:
                    </b>
                    <div>
                      {DateSubmitted || 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <label>
                    <b>
                      MDR Submitted:
                    </b>
                    <div>
                      {MDRSubmitted ? 'YES' : 'NO' || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Type of Report:
                    </b>
                    <div>
                      {Type ? '5 Day' : '30 Day' || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Type of Reportable Event:
                    </b>
                    <div>
                      {Classification}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <label>
                    <b>
                      CAPA File Review Results
                    </b>
                    <div>
                      {CAPAFileReviewResults || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      DHR Review Results:
                    </b>
                    <div>
                      {DHRReviewConclusion || 'N/A'}
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Risk File Review Results:
                    </b>
                    <div>
                      N/A
                    </div>
                  </label>
                </td>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td>
                  <label>
                    <b>
                      Root Cause Code:
                    </b>
                    <div>
                      {RootCause || 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
              <tr>
                <td colSpan="8">
                  <label>
                    <b>
                      Investigation Summary:
                    </b>
                    <div>
                      {InvestigationSummary || 'N/A'}
                    </div>
                  </label>
                </td>
              </tr>
              <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div>
        {storedUserInfo.Role === 0 && (Phase === 'Closed' || StatusOpenClosed === 'CLOSED') ? (
          <div>
            <Alert
              cancelButtonText="Cancel"
              confirmButtonText="Re-open Complaint"
              icon="high-priority"
              intent={Intent.DANGER}
              isOpen={showAlert}
              onCancel={handleCancel}
              onConfirm={handleConfirm}
            >
              <h2>
                Issue ID:
                {' '}
                {ComplaintID}
              </h2>
              <h3>
                Closed on:
                {' '}
                {DateClosed || 'N/A'}
              </h3>
              <p>Are you sure you want to re-open this complaint?</p>
            </Alert>
            <div className="row-wrapper-5">
              <Button
                type="button"
                // minimal
                // outlined
                intent="danger"
                className="bp3-button .modifier"
                onClick={() => setShowAlert(true)}
              >
                Re-open Complaint
              </Button>
            </div>
          </div>
        ) : (
          <>
          </>
        )}
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaintInfo: state.viewComplaint.complaintInfo,
  mdrInfo: state.viewComplaint.mdrInfo,
  investigationInfo: state.investigation.investigationInfo,
  phaseChangeSuccessful: state.changes.phaseChangeSuccessful,
});

export default connect(mapStateToProps)(FinalReview);
