/* eslint-disable max-len */
/* eslint-disable no-param-reassign */
import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import TestDataService from '../../services/test.service';

export const fetchAllComplaints = createAsyncThunk(
  'testComplaints/fetchAllComplaints',
  async () => {
    const response = await TestDataService.getAllComplaints();
    return response.data;
  },
);

export const existingComplaintsSlice = createSlice({
  name: 'existingComplaints',
  initialState: {
    complaints: {},
    intakeComplaints: [],
    firstRevComplaints: [],
    investigationComplaints: [],
    mdrComplaints: [],
    finalRevComplaints: [],
    openComplaints: [],
    closedComplaints: [],
    userComplaints: {},
    // intakeUsers: [],
    singleComplaint: {},
    createdByList: [{
      value: 'Created By',
    }],
    complaintsFetched: false,
  },
  reducers: {
    setCBList: (state, action) => {
      state.createdByList = action.payload;
    },
  },

  extraReducers: (builder) => {
    builder.addCase(fetchAllComplaints.fulfilled, (state, action) => {
      const complaintArray = Object.values(action.payload);
      const intakeArray = complaintArray.filter((complaint) => complaint.Phase === 'Intake');
      const firstRevArray = complaintArray.filter((complaint) => complaint.Phase === 'First Review' || complaint.Phase === 'FirstReview');
      const investigationArray = complaintArray.filter((complaint) => complaint.Phase === 'Investigation');
      const mdrArray = complaintArray.filter((complaint) => complaint.MDRIsRequired && complaint.MDRConclusion1 === null && complaint.Phase !== 'Closed');
      const finalRevArray = complaintArray.filter((complaint) => complaint.Phase === 'Final Review' || complaint.Phase === 'FinalReview');
      const closedArray = complaintArray.filter((complaint) => complaint.Phase === 'Closed');
      const openArray = intakeArray.concat(firstRevArray, investigationArray, finalRevArray);

      state.complaints = complaintArray;
      state.intakeComplaints = intakeArray;
      state.firstRevComplaints = firstRevArray;
      state.investigationComplaints = investigationArray;
      state.mdrComplaints = mdrArray;
      state.finalRevComplaints = finalRevArray;
      state.openComplaints = openArray;
      state.closedComplaints = closedArray;

      // TODO: Fix this
      // const intakeUsersArr = [];
      // complaintArray.forEach((complaint) => {
      //   if (!complaint.PersonIntake && !intakeUsersArr.includes(complaint.PersonIntake)) {
      //     intakeArray.push(complaint.PersonIntake);
      //   }
      // });
      // console.table(intakeUsersArr);
      // state.intakeUsers = intakeUsersArr;

      state.complaintsFetched = true;

      // window.sessionStorage.setItem('allComplaints', JSON.stringify(action.payload));
    });
  },
});

export const selectExistingComplaints = (state) => state.existingComplaints.complaints;

export const { updateInfo, setCBList } = existingComplaintsSlice.actions;

export default existingComplaintsSlice.reducer;
