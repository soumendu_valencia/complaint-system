/* eslint-disable react/prop-types */
/* eslint-disable max-len */
/* eslint-disable arrow-body-style */
import React from 'react';
import { connect } from 'react-redux';
import CustomTable from '../CustomTable/CustomTable';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import './ExistingComplaints.css';
import '../../App.css';

function ExistingComplaints(props) {
  const {
    complaints,
    // closedComplaints,
  } = props;

  return (
    <>
      <div className="dashboard-wrapper">
        <div className="user-dashboard-wrapper">
          <div className="dashboard-container-right">
            <div className="complaints-table-wrapper">
              <div className="complaints-table-wrapper">
                <h2>Closed:</h2>
                <CustomTable data={complaints} customHeight={0.8} customWidth={0.8} />
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const mapStateToProps = (state) => ({
  complaints: state.existingComplaints.complaints,
  closedComplaints: state.existingComplaints.closedComplaints,
  userInfo: state.login.userInfo,
});

export default connect(mapStateToProps)(ExistingComplaints);
