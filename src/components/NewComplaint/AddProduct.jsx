/* eslint-disable max-len */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react';
import { HTMLSelect } from '@blueprintjs/core';
import { useDispatch } from 'react-redux';
import { writeProducts, setProdsUploadedTrue } from '../IntakeForm/intakeFormSlice';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import '../../App.css';

export default function AddProduct(props) {
  const dispatch = useDispatch();

  const {
    products,
    scopeID,
    save,
  } = props;

  const inputArr = [{
    serialNum: '',
    id: 1,
  }];

  const inputProdArr = [{
    prodName: '',
    id: 1,
  }];

  const [arr, setArr] = useState(inputArr);
  const [prodArr, setProdArr] = useState(inputProdArr);

  useEffect(() => {
    if (save && prodArr.length >= 1) {
      const prodList = JSON.parse(window.sessionStorage.getItem('userProdArr'));
      if (prodList && Object.keys(prodList).length > 0) {
        const {
          prodInfoList,
          snArr,
        } = prodList;
        prodInfoList.forEach((p, i) => {
          p.serialNum = snArr[i].serialNum;
        });
        window.sessionStorage.setItem('finalArr', JSON.stringify(prodInfoList));
      }
    }
  }, [save]);

  useEffect(() => {
    if (scopeID) {
      const savedProdArr = JSON.parse(window.sessionStorage.getItem('finalArr'));
      if (savedProdArr && savedProdArr.length > 0) {
        const inputObj = { scopeID, savedProdArr };
        window.sessionStorage.removeItem('userProdArr');
        window.sessionStorage.removeItem('finalArr');
        dispatch(writeProducts(inputObj));
      } else {
        dispatch(setProdsUploadedTrue());
      }
    }
  }, [scopeID]);

  const addInput = () => {
    setProdArr((s) => [...s, { prodName: '', id: s.length + 1 }]);
    setArr((s) => [...s, { serialNum: '', id: s.length + 1 }]);
  };

  const handleSelectChange = (e) => {
    e.preventDefault();
    const index = e.target.id;

    setProdArr((s) => {
      const newArr = s.slice();
      newArr[index].prodName = e.target.value;
      window.sessionStorage.setItem('userProdArr', JSON.stringify({ prodInfoList: prodArr, snArr: arr }));
      return newArr;
    });
  };

  const handleChange = (e) => {
    e.preventDefault();
    const index = e.target.id;

    setArr((s) => {
      const newArr = s.slice();
      newArr[index].serialNum = e.target.value;
      window.sessionStorage.setItem('userProdArr', JSON.stringify({ prodInfoList: prodArr, snArr: arr }));
      return newArr;
    });
  };

  return (
    <>
      <div>
        {arr.map((item, i) => (
          <div key={`${item}-name`}>
            <div className="row-wrapper-5">
              <label htmlFor={i} key={`${item}-name-label`}>
                <b>Product Name</b>
                <div>
                  <HTMLSelect
                    // className="bp3-minimal"
                    intent="primary"
                    id={i}
                    key={`${item}-name-option`}
                    onChange={handleSelectChange}
                    options={products}
                  />
                </div>
              </label>
              <label htmlFor={i}>
                <b>Serial/Lot Number</b>
                <div>
                  <input
                    className="bp3-input"
                    onChange={handleChange}
                    value={item.serialNum}
                    id={i}
                    key={`${item}-number`}
                    type={item.type}
                    size="40"
                  />
                </div>
              </label>
              {/* <button type="button" className="bp3-button" onClick={handleRemove}>Remove</button> */}
            </div>
          </div>
        ))}
      </div>
      <br />
      <button type="button" className="bp3-button bp3-intent-primary" onClick={addInput}>Add Product</button>
    </>
  );
}
