import React from 'react';
import companyLogo from '../../VTCLogo.png';
import IntakeForm from '../IntakeForm/IntakeForm';
import backArrow from '../../back-arrow.png';

import 'normalize.css';
import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/datetime/lib/css/blueprint-datetime.css';
import './NewComplaint.css';
import '../../App.css';

export default function NewComplaint() {
  const storedUserInfo = JSON.parse(window.sessionStorage.getItem('userInfo'));

  return (
    <>
      <div className="wrapper">
        <div className="main-header">
          <div className="left-pane">
            <a href="https://vtc-complaint-system.herokuapp.com/dashboard">
              <img src={backArrow} alt="back arrow" width="24" height="24" />
              <h3>Back</h3>
            </a>
            <h1 className="main-header-text">Complaint System</h1>
          </div>
          <div className="right-pane">
            <img src={companyLogo} alt="test" width="72" height="60" />
          </div>
        </div>

        {storedUserInfo.Role === 0 || storedUserInfo.Role === 2 ? (
          <IntakeForm />
        ) : (
          <h1>You do not have permission to create a new issue.</h1>
        )}
        {/* </div> */}
      </div>
    </>
  );
}
