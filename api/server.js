/* eslint-disable no-await-in-loop */
/* eslint-disable no-plusplus */
/* eslint-disable no-console */
/* eslint-disable max-len */
const express = require('express');
const cors = require('cors');
const sql = require('mssql');
const path = require('path');
const fs = require('fs');
require('dotenv').config();
const {
  ShareServiceClient,
  StorageSharedKeyCredential,
  // ShareDirectoryClient,
} = require('@azure/storage-file-share');
const dbActions = require('../src/actions/dbActions');
const loginTool = require('../src/actions/loginActions/loginTools');

const app = express();

app.use(express.json({ limit: 200000000 }));
app.use(express.static(path.resolve(__dirname, './build')));
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

const sqlConfig = {
  user: process.env.SQL_USER || '',
  password: process.env.SQL_PASS || '',
  database: process.env.SQL_DB || '',
  server: process.env.SQL_SERVER || '',
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMillis: 30000,
  },
  options: {
    encrypt: true,
    trustServerCertificate: false,
  },
};

const connectionPool = new sql.ConnectionPool(sqlConfig)
  .connect()
  .then((pool) => {
    console.log('Connected to MSSQL...');
    return pool;
  })
  .catch((err) => console.log('Connection Pool ERROR: ', err));

app.use('/api/writeChange', async (req, res) => {
  const writeChangeResult = await dbActions.writeChange(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('WRITE CHANGE ERROR CATCH');
      console.log(e);
    });
  res.status(200).send({ result: writeChangeResult });
});

app.use('/api/testComplaints', async (req, res) => {
  const custResult = await dbActions.pullCustomers(sql, connectionPool)
    .catch((e) => {
      console.log('CUSTOMERS ERROR CATCH');
      console.log(e);
    });

  const patResult = await dbActions.pullPatients(sql, connectionPool)
    .catch((e) => {
      console.log('PATIENTS ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ custResult, patResult });
});

app.use('/api/getProducts', async (req, res) => {
  const prodResult = await dbActions.getProducts(sql, connectionPool)
    .catch((e) => {
      console.log('PRODUCTS ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ result: prodResult });
});

app.use('/api/writeIntakeToDb', async (req, res) => {
  // TODO: change on migration to new URL
  if (req.headers.referer === 'https://vtc-complaint-system.herokuapp.com/home' || req.headers.referer === 'http://localhost:3000/') {
    const writeResult = await dbActions.writeIntakeToDb(sql, connectionPool, req.body)
      .catch((e) => {
        console.log('WRITE INTAKE ERROR CATCH');
        console.log(e);
      });
    res.status(200).send(writeResult);
  } else {
    res.status(403).send('Invalid request URL');
  }
});

app.use('/api/updateIntakeToDb', async (req, res) => {
  const writeResult = await dbActions.updateIntakeToDb(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('UPDATE INTAKE ERROR CATCH');
      console.log(e);
    });
  res.status(200).send({ result: writeResult });
});

app.use('/api/getAllcomplaints', async (req, res) => {
  const complaintResult = await dbActions.pullAllComplaints(sql, connectionPool)
    .catch((e) => {
      console.log('ALL COMPLAINTS ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json(complaintResult);
});

app.use('/api/fetchAllUsers', async (req, res) => {
  const usersResult = await dbActions.fetchAllUsers(sql, connectionPool)
    .catch((e) => {
      console.log('ALL USERS ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ result: usersResult });
});

app.use('/api/getSingleComplaint', async (req, res) => {
  const { ComplaintID } = req.body;
  // eslint-disable-next-line max-len
  const singleComplaintResult = await dbActions.getSingleComplaint(sql, connectionPool, ComplaintID)
    .catch((e) => {
      console.log('SINGLE COMPLAINT ERROR CATCH:');
      console.log(e);
    });
  return res.status(200).json({ result: singleComplaintResult });
});

app.use('/api/getAsReportedCodes', async (req, res) => {
  const asReportedResults = await dbActions.pullAsReportedCodes(sql, connectionPool)
    .catch((e) => {
      console.log('AS REPORTED CODES ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json(asReportedResults);
});

app.use('/api/writeFirstRevToDb', async (req, res) => {
  const writeResult = await dbActions.writeFirstRevToDb(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('WRITE FIRST REV ERROR CATCH');
      console.log(e);
    });
  res.status(200).send({ result: writeResult });
});

app.use('/api/writeMDRToDb', async (req, res) => {
  const mdrResult = await dbActions.writeMDRToDb(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('WRITE MDR ERROR CATCH');
      console.log(e);
    });
  res.status(200).send(mdrResult);
});

app.use('/api/writeMDRNum', async (req, res) => {
  const mdrNumResult = await dbActions.writeMDRNum(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('WRITE MDR NUM ERROR CATCH');
      console.log(e);
    });
  res.status(200).send(mdrNumResult);
});

app.use('/api/writeInvestigationToDb', async (req, res) => {
  const investigationResult = await dbActions.writeInvestigationToDb(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('WRITE INVESTIGATION ERROR CATCH');
      console.log(e);
    });
  res.status(200).json(investigationResult);
});

app.use('/api/fetchInvestigation', async (req, res) => {
  const investigationResult = await dbActions.fetchInvestigation(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('FETCH INV ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ result: investigationResult });
});

app.use('/api/fetchAllInvestigations', async (req, res) => {
  const investigationsResult = await dbActions.fetchAllInvestigations(sql, connectionPool)
    .catch((e) => {
      console.log('ALL INVS ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ result: investigationsResult });
});

app.use('/api/writeLoginInfo', async (req, res) => {
  const { username, password } = req.body;
  const loginReponse = await dbActions.login(
    sql,
    connectionPool,
    username,
    password,
  )
    .catch((e) => {
      console.log('LOGIN ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ isLoginSuccessful: loginReponse[0], userInfo: loginReponse[1] });
});

app.use('/api/createAccount', async (req, res) => {
  const {
    dateAccountCreated,
    email,
    firstName,
    lastName,
    password,
    username,
    role,
  } = req.body;
  const userSalt = loginTool.generateSalt();
  const { hashedPass } = loginTool.hash(password, userSalt);
  const createAccountSuccess = await dbActions.createAccount(sql, connectionPool, {
    dateAccountCreated,
    email,
    firstName,
    lastName,
    hashedPass,
    userSalt,
    username,
    role,
  });
  return res.status(200).json({ result: createAccountSuccess });
});

app.use('/api/updateAccount', async (req, res) => {
  const { newPass, username, dateUpdated } = req.body;
  const userSalt = loginTool.generateSalt();
  const { hashedPass } = loginTool.hash(newPass, userSalt);

  const updateAccountSuccess = await dbActions.updateAccount(sql, connectionPool, {
    hashedPass,
    userSalt,
    username,
    dateUpdated,
  });
  return res.status(200).json(updateAccountSuccess);
});

app.use('/api/writeAssignToDb', async (req, res) => {
  const assignResponse = await dbActions.assignUserToComplaint(
    sql,
    connectionPool,
    req.body,
  )
    .catch((e) => {
      console.log('ASSIGN ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json({ result: assignResponse });
});

app.use('/api/fetchMDRInfo', async (req, res) => {
  const { ComplaintID } = req.body;
  const mdrInfo = await dbActions.fetchMDRInfo(sql, connectionPool, ComplaintID).catch((e) => {
    console.log('AS REPORTED CODES ERROR CATCH');
    console.log(e);
  });
  return res.status(200).json({ result: mdrInfo });
});

app.use('/api/fetchAllMDRInfo', async (req, res) => {
  const allMDRInfo = await dbActions.fetchAllMDRInfo(sql, connectionPool).catch((e) => {
    console.log('ALL MDR ERROR CATCH');
    console.log(e);
  });
  return res.status(200).json({ result: allMDRInfo });
});

app.use('/api/updateMDRToDb', async (req, res) => {
  const writeResult = await dbActions.updateMDRToDb(sql, connectionPool, req.body);
  res.status(200).send(writeResult);
});

app.use('/api/updateInvestigationToDb', async (req, res) => {
  const writeResult = await dbActions.updateInvestigationToDb(sql, connectionPool, req.body);
  res.status(200).send(writeResult);
});

app.use('/api/changePhase', async (req, res) => {
  const phaseChangeResult = await dbActions.changePhase(sql, connectionPool, req.body);
  res.status(200).send({ result: phaseChangeResult });
});

app.use('/api/writeFile', async (req, res) => {
  const {
    issueID,
    fileBuffer,
    fileName,
    fileType,
    dateUploaded,
    personUpload,
    phase,
  } = req.body;

  // Creates new buffer with base64 encoding
  const parsedBuffer = Buffer.alloc(Buffer.byteLength(fileBuffer), fileBuffer, 'base64');
  let writeFileResult = false;

  // Function to write a file to the file share
  const writeFile = async () => {
    const account = process.env.FILE_ACCOUNT_NAME || '';
    const accountKey = process.env.FILE_ACCOUNT_KEY || '';
    const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);

    const serviceClient = new ShareServiceClient(
      `https://${account}.file.core.windows.net`,
      sharedKeyCredential,
    );

    const shareName = 'vtc-data';
    const shareClient = serviceClient.getShareClient(shareName);

    const filePath = `IssueID-${issueID}`;

    // Navigate to 'Complaints' folder in vtc-data file share
    const directoryClient = shareClient.getDirectoryClient('VTC Quality');
    const directoryClient2 = directoryClient.getDirectoryClient('Complaints');

    // Create folder for specified IssueID if it does not already exist
    const directoryClient3 = directoryClient2.getDirectoryClient(filePath);
    await directoryClient3.createIfNotExists();

    // Create file by reserving space equal to size (in bytes) of Buffer
    const fileClient = directoryClient3.getFileClient(fileName);
    const fileSize = Buffer.byteLength(parsedBuffer);
    await fileClient.create(fileSize);

    // Begin writing file data to reserved file space
    // If file size is > 4 MB, break it into 4 MB chunks and write sequentially
    if (fileSize > 4000000) {
      let start = 0;
      let end = 4000000;
      const numSegments = Math.ceil(fileSize / 4);

      // For each chunk, write it to the reserved space
      for (let i = 0; i < numSegments; i++) {
        if (end === fileSize) {
          // If this is the last chunk, finish writing then check if file is successfullly created
          writeFileResult = await fileClient.uploadRange(parsedBuffer.slice(start, end), 0, end - start).then(async () => fileClient.exists());
        } else {
          await fileClient.uploadRange(parsedBuffer.slice(start, end), 0, end - start);
        }

        // Reassign params to cover next 4 MB chunk of data in buffer
        start = end;
        // If the remaining data is < 4 MB, set the endpoint to last value instead
        end = end + 4000000 > fileSize ? fileSize : end + 4000000;
      }
    // If file size < 4 MB write the file in one chunk
    } else {
      writeFileResult = await fileClient.uploadRange(parsedBuffer, 0, parsedBuffer.length).then(async () => fileClient.exists());
    }
  };

  // Write the miscellaneous file information to the DB
  const uploadFilesResult = await dbActions.uploadFiles(sql, connectionPool, {
    issueID, fileName, fileType, dateUploaded, personUpload, phase,
  });

  writeFile().catch((error) => {
    console.error(error);
    writeFileResult = false;
  });

  return res.status(200).send(writeFileResult && uploadFilesResult);
});

app.use('/api/getFile', async (req, res) => {
  const {
    issueID,
    fileName,
  } = req.body;

  // eslint-disable-next-line no-unused-vars
  async function streamToBuffer(readableStream) {
    return new Promise((resolve, reject) => {
      const chunks = [];
      readableStream.on('data', (data) => {
        chunks.push(data instanceof Buffer ? data : Buffer.from(data));
      });
      readableStream.on('end', () => {
        resolve(Buffer.concat(chunks));
      });
      readableStream.on('error', reject);
    });
  }

  const account = process.env.FILE_ACCOUNT_NAME || '';
  const accountKey = process.env.FILE_ACCOUNT_KEY || '';
  const sharedKeyCredential = new StorageSharedKeyCredential(account, accountKey);

  const serviceClient = new ShareServiceClient(
    `https://${account}.file.core.windows.net`,
    sharedKeyCredential,
  );

  const shareName = 'vtc-data';
  const shareClient = serviceClient.getShareClient(shareName);
  const filePath = `IssueID-${issueID}`;

  const directoryClient = shareClient.getDirectoryClient('VTC Quality');
  const directoryClient2 = directoryClient.getDirectoryClient('Complaints');
  const directoryClient3 = directoryClient2.getDirectoryClient(filePath);
  console.log('FILE NAME IN SERVER: ', fileName);
  console.log('FILENAME TYPE IN SERVER: ', typeof fileName);
  const fileClient = directoryClient3.getFileClient(fileName);
  const downloadFileResponse = await fileClient.download();
  const fileBuffer = await streamToBuffer(downloadFileResponse.readableStreamBody);
  await fs.writeFile(`./${fileName}`, fileBuffer.toString('base64'), (err) => {
    if (err) {
      console.error(err);
    }
  });
  // res.attachment('pdfname.pdf');
  // pdfstream.pipe(res);
  return res.download(`./${fileName}`, fileName);
});

app.use('/api/uploadFiles', async (req, res) => {
  console.log(req.body);
  const uploadFilesResult = await dbActions.uploadFiles(sql, connectionPool, req.body);
  res.status(200).send(uploadFilesResult);
});

app.use('/api/fetchAllFiles', async (req, res) => {
  const { issueID } = req.body;
  const filesResult = await dbActions.fetchAllFiles(sql, connectionPool, issueID)
    .catch((e) => {
      console.log('ALL FILES ERROR CATCH');
      console.error(e);
    });
  return res.status(200).json({ result: filesResult });
});

app.use('/api/updateCustFollowup', async (req, res) => {
  const custFollowupResult = await dbActions.updateCustFollowup(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('CUSTOMER FOLLOWUP ERROR CATCH');
      console.error(e);
    });
  return res.status(200).json({ result: custFollowupResult });
});

app.use('/api/updateNotesToDB', async (req, res) => {
  const updateNotesResult = await dbActions.updateNotesToDB(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('UPDATE NOTES ERROR CATCH');
      console.error(e);
    });
  return res.status(200).json(updateNotesResult);
});

app.use('/api/writeProducts', async (req, res) => {
  const writeProdsResult = await dbActions.writeProducts(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('PRODUCT UPLOAD ERROR CATCH');
      console.error(e);
    });
  return res.status(200).json(writeProdsResult);
});

app.use('/api/fetchUserProducts', async (req, res) => {
  const userProductsResult = await dbActions.fetchUserProducts(sql, connectionPool, req.body)
    .catch((e) => {
      console.log('GET USER PRODUCTS ERROR CATCH');
      console.log(e);
    });
  return res.status(200).json(userProductsResult);
});

const host = '0.0.0.0';
const port = process.env.PORT || 5000;

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'build', 'index.html'));
  });
}

app.listen(port, host, () => console.log('API is running on http://localhost:8080/'));
